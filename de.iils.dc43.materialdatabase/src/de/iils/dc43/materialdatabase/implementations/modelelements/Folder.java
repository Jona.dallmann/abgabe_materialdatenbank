package de.iils.dc43.materialdatabase.implementations.modelelements;

import java.beans.PropertyChangeListener;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;

public class Folder extends AbstractGraphElement{

	public Folder(String name) {
		super("folder", name);
	}
	
	@Override
	public String toString() {
		return String.format("Type: Folder, Name: %s, UUID: %s", this.getName(),this.getId().toString());
	}
}
