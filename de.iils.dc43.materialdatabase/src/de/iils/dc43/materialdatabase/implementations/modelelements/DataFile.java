package de.iils.dc43.materialdatabase.implementations.modelelements;

import java.nio.file.Path;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;

public class DataFile extends AbstractGraphElement{
	private Path filePath;
	private boolean protectionStatus = false;
	
	public DataFile(Path filePath) {
		// constructor extracts name from path object
		super("dataFile", filePath.toString());
		this.setName(filePath.getFileName().toString());
		this.filePath = filePath;
	}
	
	public DataFile(Path filePath, String name) {
		super("dataFile", name);
		this.filePath = filePath;
	}
	
	public boolean isProtected() {
		return this.protectionStatus;
	}
	
	public void setProtected() {
		this.protectionStatus = true;
	}
	
	public Path getFilePath() {
		return this.filePath;
	}
	
	@Override
	public String toString() {
		return String.format("Type: Datafile, Path: %s, UUID: %s", this.getFilePath().toString(), this.getId().toString());
	}
}
