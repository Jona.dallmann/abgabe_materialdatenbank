package de.iils.dc43.materialdatabase.implementations.modelelements;

import java.util.Random;
import java.util.UUID;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;

public class Material extends AbstractGraphElement{
	//private HashSet<E> properties = new HashSet<>();
	
	public Material(String name) {
		super("material", name);
	}
	
	public Material(String name, UUID id) {
		// custom UUID only allowed for materials to allow
		// tracking uuids when the program was closed and the
		// database was saved
		super("material", name, id);
	}
	
	@Override
	public String toString() {
		return String.format("Type: Material, Name: %s, UUID: %s", 
				this.getName(),this.getId().toString());
	}
}
