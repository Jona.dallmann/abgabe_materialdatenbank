package de.iils.dc43.materialdatabase.implementations.modelelements;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;

public class RootElement extends AbstractGraphElement{
	// using singleton pattern here since root element must not be instanced twice
	private static final RootElement instance = new RootElement();
	
	private RootElement() {
		super("root", "root");
	};

	public static RootElement getInstance() {
		return instance;
	}
}
