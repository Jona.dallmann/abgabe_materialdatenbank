package de.iils.dc43.materialdatabase.implementations.modelelements;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;

import java.util.TreeMap;
import java.util.UUID;

// class to store material property
// IMPORTANT: the property of the property unit/value etc can be extended but not with a same name element
// this will confuse the simple view

public class MaterialProperty extends AbstractGraphElement {
	private String unit = "";
	private double value;
	private Map<Double, Double> lookUpTable;
	private String lookUpTableUnit = "";

	//constructors
	public MaterialProperty(String name, double value, String unit) {
		super("property", name);
		this.setValueOrLookUpTable((double) value);
		this.setUnit(unit);
	}
	
	public MaterialProperty(String name, double value, String unit, UUID id) {
		super("property", name, id);
		this.setValueOrLookUpTable((double) value);
		this.setUnit(unit);
	}

	public MaterialProperty(String name, double value, String unit, String note) {
		super("property", name);
		this.setValueOrLookUpTable(value);
		this.setUnit(unit);
		this.setNote(note);
	}

	public MaterialProperty(String name, Map<Double, Double> propertyLookUpTable, String unit, String note) {
		this(name, propertyLookUpTable, unit);
		this.setNote(note);
	}

	public MaterialProperty(String name, Map<Double, Double> propertyLookUpTable, String unit) {
		super("property", name);
		this.setValueOrLookUpTable(propertyLookUpTable);
		this.setUnit(unit);
	}
	
	public MaterialProperty(String name, Map<Double, Double> propertyLookUpTable, String unit, UUID id) {
		super("property", name, id);
		this.setValueOrLookUpTable(propertyLookUpTable);
		this.setUnit(unit);
	}

	@Override
	public String toString() {
		return String.format("Type: MaterialProperty, Name: %s, UUID: %s", this.getName(), this.getId().toString());
	}

	// getter and setter
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		String oldUnit = this.getUnit();
		if(unit.contains("/")){
			return;
		}
		this.unit = unit;
		this.firePropertyChange("unit", oldUnit, unit);
	}
	
	public String getLookUpTableUnit() {
		return lookUpTableUnit;
	}

	public void setLookUpTableUnit(String lookUpTableUnit) {
		// there shall be no null value so it gets replaced wih ""
		if(lookUpTableUnit == null) {
			lookUpTableUnit = "";
		}
		String oldUnit = this.getLookUpTableUnit();
		this.lookUpTableUnit = lookUpTableUnit;
		this.firePropertyChange("lookUpTableUnit", oldUnit, lookUpTableUnit);
	}

	public double getValue() {
		// function if a static value is used, returns value at 0.0 of the lut if it is
		// not null
		if (this.lookUpTable == null) {
			return this.value;
		} else {
			return getValueAtPoint(0.0);
		}
	}

	public Object getValueObject() {
		if (this.lookUpTable == null) {
			return this.value;
		} else {
			return this.lookUpTable;
		}
	}

	public double getValueAtPoint(double point) {
		// function if a lut or function is used which returns the value at the point
		if (this.lookUpTable != null) {
			return this.interpolateLutValue(point);
		} else {
			return this.getValue();
		}
	}

	private void setValueOrLookUpTable(Object valueOrLut) {
		// get old lut or function
		Object oldValueOrLut = null;
		if (this.lookUpTable == null) {
			oldValueOrLut = this.value;
		} else {
			oldValueOrLut = this.lookUpTable;
		}

		// check what is the incoming object and set it corresponding to its class
		if (valueOrLut.getClass().equals(Double.class)) {
			this.value = (double) valueOrLut;
			this.lookUpTable = null;
		} else if (valueOrLut instanceof Map<?, ?>) {
			this.lookUpTable = (Map<Double, Double>) valueOrLut; // unchecked conversion but due to the private its ok
			this.value = Double.NaN;
		}
		this.firePropertyChange("value", oldValueOrLut, valueOrLut);
		
		// also change unit of look up table to ""
		this.setLookUpTableUnit("");
	}

	public void setValue(int value) {
		this.setValueOrLookUpTable((double) value);
	}

	public void setValue(double value) {
		this.setValueOrLookUpTable(value);
	}

	public void setValue(String value) {
		this.setValueOrLookUpTable(Double.parseDouble(value));
	}

	public void setValue(HashMap<Double, Double> valueMap) {
		this.setValueOrLookUpTable(valueMap);
	}

	// functions for use in the materialProperty class
	private Double interpolateLutValue(double interpolateX) throws IndexOutOfBoundsException{
		// TODO: ask what should happen with extrapolated values? -> currently error
		// TODO: ask what should happen with values below zero? -> works but axtrapolated error
		// TODO: insert zero when all values are below zero? -> currently jap
		// TODO: what should be done with devision by zero?

		// return value if the given interpolate x is in the look up table
		if (this.lookUpTable.containsKey(interpolateX)) {
			return this.lookUpTable.get(interpolateX);
		}

		// this function defines between which values should be
		// interpolated, only positive values are supported
		if (this.lookUpTable.size() > 1) {
			// copy hash map so operations performed on it dont change
			// the original hash map, also transform it to tree map so
			// pairs get sorted due to the key value
			Map<Double, Double> copiedLookUpTable = new TreeMap<Double, Double>(this.lookUpTable);

			// check if first key is above zero which indicates
			// that element [0,y] has to be inserted, same for last element < [0,y]
			Double firstLookUpTableXValue = (Double) copiedLookUpTable.keySet().toArray()[0];
			Double lastLookUpTableXValue = (Double) copiedLookUpTable.keySet().toArray()[copiedLookUpTable.size()-1];
			if (firstLookUpTableXValue > 0 || lastLookUpTableXValue < 0) {
				// add zero so interpolation from between
				// value and zero can be done
				copiedLookUpTable.put(0.0, 0.0);
			}

			// find points closest to the given interpolateX value and
			// interpolate between them
			Entry<Double, Double> lastPoint = null;
			Double lastDistanceAbsolute = Double.MAX_VALUE;
			for (Entry<Double, Double> currentPoint : copiedLookUpTable.entrySet()) {
				double currentDistanceAbsolute = Math.abs(currentPoint.getKey() - interpolateX);
				if (lastPoint != null) {
					if (lastDistanceAbsolute <= currentDistanceAbsolute) {
						double x1 = lastPoint.getKey();
						double y1 = lastPoint.getValue();
						double x2 = currentPoint.getValue();
						double y2 = currentPoint.getValue();
						return this.interpolateLin(x1, y1, x2, y2, interpolateX);
					}
				}
				lastPoint = currentPoint;
				lastDistanceAbsolute = currentDistanceAbsolute;
			}

		} else if (this.lookUpTable.size() == 1) {
			Double xPointLookUpTable = (Double) this.lookUpTable.keySet().toArray()[0];
			Double yPointLookUpTable = (Double) this.lookUpTable.values().toArray()[0];
			if (xPointLookUpTable > 0) {
				return this.interpolateLin(xPointLookUpTable, yPointLookUpTable, 0.0, 0.0, interpolateX);
			} else {
				return this.interpolateLin(0.0, 0.0, xPointLookUpTable, yPointLookUpTable, interpolateX);
			}
		}
		throw new IndexOutOfBoundsException("Input value for interpolation in Look up"
				+ " Table for Material UUID '"+this.getParentId()+"' in Property '"
				+ this.getName() +"' ID: "+this.getId().toString()+" is not between two table points");
	}

	private Double interpolateLin(double x1, double y1, double x2, double y2, double interpolateX) {
		// linear interpolation between the two given points
		Double result = y1 + (y2 - y1) / (x2 - x1) * (interpolateX - x1);
		return result;
	}
}
