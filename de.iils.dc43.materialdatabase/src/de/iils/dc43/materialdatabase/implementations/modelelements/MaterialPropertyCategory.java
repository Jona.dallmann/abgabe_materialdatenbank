package de.iils.dc43.materialdatabase.implementations.modelelements;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;

public class MaterialPropertyCategory extends AbstractGraphElement{

	public MaterialPropertyCategory(String name) {
		super("category", name);
		// TODO Auto-generated constructor stub
	}
	
	//Override functions which are not allowed to be used
	@Override
	public void setNote(String note) {
	}
	
	@Override
	public String getNote() {
		return null;
	}
	
	@Override
	public String toString() {
		return String.format("Type: MaterialPropertyCategory, Name: %s, UUID: %s", this.getName(),this.getId().toString());
	}
}
