package de.iils.dc43.materialdatabase.implementations.simplemodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import de.iils.dc43.materialdatabase.implementations.simplemodel.GraphUtility;
import de.iils.dc43.materialdatabase.implementations.simplemodel.UndirectedWeightedSimpleGraph;
import de.iils.dc43.materialdatabase.implementations.simplemodel.UndirectedWeightedSimpleGraph.Edge;

/**
 * Base implementation of algorithms that search a path between two vertices of a graph.
 *
 * @author Hannes Wellmann
 * @param <V> the type of vertices
 */
public abstract class PathSearchAlgorithm<V> {
	public final UndirectedWeightedSimpleGraph<V> graph;
	private int discoveredVertices = 0;
	private int verticesDiscoveredInLatestRun = 0;

	public PathSearchAlgorithm(UndirectedWeightedSimpleGraph<V> graph) {
		this.graph = graph;
	}

	public UndirectedWeightedSimpleGraph<V> getGraph() {
		return graph;
	}

	public abstract GraphPath<V> getPath(V sourceVertex, V targetVertex);

	// --- path creation ---

	public static class VertexEdge<V> {
		public final V vertex;
		public Edge<V> reachingEdge; // Kante vom parent-Knoten zum vertex dieses Objects

		public VertexEdge(V vertex, Edge<V> edge) {
			this.vertex = vertex;
			this.reachingEdge = edge;
		}
	}

	public GraphPath<V> backTracePath(V sourceVertex, V targetVertex, Map<V, ? extends VertexEdge<V>> vertexToEdge) {

		List<Edge<V>> edgeList = new ArrayList<>();

		VertexEdge<V> node = vertexToEdge.get(targetVertex);
		while (node.reachingEdge != null) { // Die reachingEdge des sourceVertex ist immer null
			edgeList.add(node.reachingEdge);

			V previousVertex = GraphUtility.getOppositeVertex(node.reachingEdge, node.vertex);
			node = vertexToEdge.get(previousVertex);
		}

		return createGraphPathFromReversedEdgeList(sourceVertex, edgeList);
	}

	public GraphPath<V> createGraphPathFromReversedEdgeList(V start, List<Edge<V>> edgesList) {
		verticesDiscoveredInLatestRun = discoveredVertices;
		discoveredVertices = 0;

		List<V> vertexList = new ArrayList<>();
		double weight = 0;

		Collections.reverse(edgesList);

		vertexList.add(start);
		V v = start;
		for (Edge<V> edge : edgesList) {
			v = GraphUtility.getOppositeVertex(edge, v);
			vertexList.add(v);
			weight += edge.getWeight();
		}
		return new GraphPath<>(vertexList, edgesList, weight, graph);
	}

	public static class GraphPath<V> {
		private final List<V> vertexList;
		private final List<Edge<V>> edgeList;

		private final double weight;
		private final UndirectedWeightedSimpleGraph<V> graph;

		private GraphPath(List<V> vertexList, List<Edge<V>> edgeList, double weight,
			UndirectedWeightedSimpleGraph<V> graph) {
			this.vertexList = vertexList;
			this.edgeList = edgeList;
			this.weight = weight;
			this.graph = graph;
		}

		public List<V> getVertexList() {
			return vertexList;
		}

		public List<Edge<V>> getEdgeList() {
			return edgeList;
		}

		public double getWeight() {
			return weight;
		}

		public UndirectedWeightedSimpleGraph<V> getGraph() {
			return graph;
		}

		@Override
		public String toString() {
			return vertexList.toString();
		}
	}

	// --- notifications ---

	public int getVerticesDiscoveredInLatestRun() {
		return verticesDiscoveredInLatestRun;
	}

	private List<BiConsumer<V, Edge<V>>> vertexDiscoveredNotifier = null;
	private List<Consumer<V>> vertexCompletedNotifier = null;

	public void addVertexDiscoveredNotifier(BiConsumer<V, Edge<V>> notifier) {
		if (vertexDiscoveredNotifier == null) {
			vertexDiscoveredNotifier = new ArrayList<>();
		}
		vertexDiscoveredNotifier.add(notifier);
	}

	public void addVertexCompletedNotifier(Consumer<V> notifier) {
		if (vertexCompletedNotifier == null) {
			vertexCompletedNotifier = new ArrayList<>();
		}
		vertexCompletedNotifier.add(notifier);
	}

	public void notifyVertexDiscovered(V vertex, Edge<V> edge) {
		discoveredVertices++;
		if (vertexDiscoveredNotifier != null) {
			for (BiConsumer<V, Edge<V>> notifier : vertexDiscoveredNotifier) {
				notifier.accept(vertex, edge);
			}
		}
	}

	public void notifyVertexCompleted(V vertex) {
		if (vertexCompletedNotifier != null) {
			for (Consumer<V> notifier : vertexCompletedNotifier) {
				notifier.accept(vertex);
			}
		}
	}
}
