package de.iils.dc43.materialdatabase.implementations.simplemodel;
import java.util.Objects;
import java.util.Set;

/**
 * An abstract undirected and weighted Simple-Graph.
 *
 * @author Hannes Wellmann
 * @param <V> the type of vertices
 */
public abstract class UndirectedWeightedSimpleGraph<V> {

	/**
	 * Adds the specified vertex to this graph, if not already present.
	 *
	 * @param vertex the vertex to add
	 * @return true if the vertex was added to this graph, false if this graph already contained the specified vertex
	 */
	public abstract boolean addVertex(V vertex);

	/**
	 * Removes the specified vertex from this graph including all its touching edges, if present.
	 *
	 * @param vertex the vertex to remove
	 * @return true if the vertex was removed from this graph, false if this graph did not contained the vertex
	 */
	public abstract boolean removeVertex(V vertex);

	/**
	 * Adds an undirected edge between the two specified nodes with the specified weight, if not already present.
	 *
	 * @param vertex1 the vertex at one end of the edge
	 * @param vertex2 the second at the other end of the edge
	 * @param weight the weight of the edge, must be greater zero
	 * @return true if the edge was added to this graph, false if this graph already contained the specified edge
	 */
	public abstract boolean addEdge(V vertex1, V vertex2, int weight);

	/**
	 * Removes the edge between the two specified nodes, if present
	 *
	 * @param vertex1 the vertex at one end of the edge
	 * @param vertex2 the second at the other end of the edge
	 * @return true if the edge was removed from this graph, false if this graph did not contained the edge
	 */
	public abstract boolean removeEdge(V vertex1, V vertex2);

	/**
	 * Returns true if this graph contains the specified vertex.
	 *
	 * @param vertex the vertex to test
	 * @return true if this graph contains the vertex, else false
	 */
	public abstract boolean containsVertex(V vertex);

	/**
	 * Returns true if the specified vertices are adjacent, so this graph contains a edge connecting/touching both
	 * vertices.
	 *
	 * @param vertex1 the first vertex
	 * @param vertex2 the second vertex
	 * @return if both edges are adjacent in this graph
	 */
	public abstract boolean containsEdge(V vertex1, V vertex2);

	/**
	 * Returns a {@link Set} of all {@link Edge edges} touching the specified vertex in this graph.
	 *
	 * @param vertex the vertex touched
	 * @return all edges touching the vertex
	 */
	public abstract Set<Edge<V>> getEdgesOf(V vertex);

	/**
	 * Returns the {@link Set} of all vertices contained in this graph.
	 *
	 * @return the set of all vertices in this graph
	 */
	public abstract Set<V> getVertices();

	/**
	 * Returns the {@link Set} of all edges contained in this graph.
	 *
	 * @return the set of all edges in this graph
	 */
	public abstract Set<Edge<V>> getEdges();

	// --- common utility methods ---

	protected void checkEdgeRequirements(V vertex1, V vertex2, int weight) { // O(1)
		GraphUtility.checkEdgeRequirements(vertex1, vertex2, weight);
	}

	/**
	 * An undirected and weighted edge.
	 *
	 * @author Hannes Wellmann
	 * @param <V> the the type of vertices this edge connects
	 */
	public static class Edge<V> {
		private final V vertex1;
		private final V vertex2;
		private final int weight;

		public Edge(V vertex1, V vertex2, int weight) {
			this.vertex1 = Objects.requireNonNull(vertex1);
			this.vertex2 = Objects.requireNonNull(vertex2);
			this.weight = weight;
		}

		/**
		 * The first vertex of this edge.
		 * <p>
		 * Note that the number does not imply any direction.
		 * </p>
		 *
		 * @return the first vertex of this edge
		 */
		public V getVertex1() {
			return vertex1;
		}

		/**
		 * The second vertex of this edge.
		 * <p>
		 * Note that the number does not imply any direction.
		 * </p>
		 *
		 * @return the second vertex of this edge
		 */
		public V getVertex2() {
			return vertex2;
		}

		/**
		 * Returns the weight of this edge. The weight is never zero or negative.
		 *
		 * @return the weight of this edge
		 */
		public int getWeight() {
			return weight;
		}

		/**
		 * Returns true if this edge touches the specified vertex.
		 * <p>
		 * An edge touches a vertex if either {@link #getVertex1() vertex1} or {@link #getVertex2() vertex2} is equal to
		 * the given vertex.
		 * </p>
		 *
		 * @param vertex the vertex to test
		 * @return true if this edge touches the vertex, else false
		 */
		public boolean touches(V vertex) { // O(1)
			return vertex1.equals(vertex) || vertex2.equals(vertex);
		}

		/**
		 * Returns true if this edge connects the specified two vertices.
		 * <p>
		 * An edge connects two vertices if it touches both and both vertices are not the same or equal.
		 * So one of the specified vertices has to be equal to {@code vertex1} of this edge and the other has to be
		 * equal to {@code vertex2}.
		 * </p>
		 *
		 * @param vertex1 the first vertex
		 * @param vertex2 the second vertex
		 * @return true if this edge connects both vertices, else false
		 */
		public boolean connects(V vertex1, V vertex2) { // O(1)
			return (this.vertex1.equals(vertex1) && this.vertex2.equals(vertex2))
				|| (this.vertex1.equals(vertex2) && this.vertex2.equals(vertex1));
		}

		@Override
		public int hashCode() {
			// symmetrisch -> gleicher Hashcode, falls vertex1 und vertex2 vertauscht
			int result = 0;
			result += ((vertex1 == null) ? 0 : vertex1.hashCode());
			result += ((vertex2 == null) ? 0 : vertex2.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (!(obj instanceof Edge)) {
				return false;
			}
			// gleich wenn vertex1 und vertex2 entweder im direkten Vergleich oder vertauscht gleich sind
			@SuppressWarnings("unchecked")
			Edge<V> other = (Edge<V>) obj;
			return connects(other.vertex1, other.vertex2);
		}
	}
}
