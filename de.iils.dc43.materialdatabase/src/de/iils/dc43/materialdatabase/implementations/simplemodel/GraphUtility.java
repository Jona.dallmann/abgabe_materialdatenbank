package de.iils.dc43.materialdatabase.implementations.simplemodel;

import de.iils.dc43.materialdatabase.implementations.simplemodel.UndirectedWeightedSimpleGraph.Edge;

/**
 * Utility class to simplfy handling of graphs.
 *
 * @author Hannes Wellmann
 */
public class GraphUtility {

	private GraphUtility() { // static use only
	}

	public static <V> void checkEdgeRequirements(V vertex1, V vertex2, int weight) { // O(1)
		if (weight <= 0) {
			throw new IllegalArgumentException("Edge weight must be greater zero");
		}
		if (vertex1.equals(vertex2)) {
			throw new IllegalArgumentException("Self loops are not permitted");
		}
	}

	public static <V> V getOppositeVertex(Edge<V> edge, V v) {
		V v1 = edge.getVertex1();
		V v2 = edge.getVertex2();
		if (v1.equals(v)) {
			return v2;
		} else if (v2.equals(v)) {
			return v1;
		} else {
			throw new IllegalStateException("Edge does not touch the given vertex");
		}
	}
}
