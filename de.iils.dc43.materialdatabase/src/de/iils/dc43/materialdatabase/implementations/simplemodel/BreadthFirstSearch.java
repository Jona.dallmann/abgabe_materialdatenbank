package de.iils.dc43.materialdatabase.implementations.simplemodel;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import de.iils.dc43.materialdatabase.implementations.simplemodel.GraphUtility;
import de.iils.dc43.materialdatabase.implementations.simplemodel.UndirectedWeightedSimpleGraph;
import de.iils.dc43.materialdatabase.implementations.simplemodel.UndirectedWeightedSimpleGraph.Edge;

/**
 * Implementation of Breadth-first search to find a path between two vertices of a graph.
 *
 * @author Hannes Wellmann
 * @param <V> the type of vertices
 */
public class BreadthFirstSearch<V> extends PathSearchAlgorithm<V> {

	public BreadthFirstSearch(UndirectedWeightedSimpleGraph<V> graph) {
		super(graph);
	}

	private final Queue<VertexEdge<V>> queue = new ArrayDeque<>(); // FiFo
	private final Map<V, VertexEdge<V>> discoveredVerticesToReachingEdge = new HashMap<>();

	/*
	 * Im worst-case läuft die äußere Schleife über alle vertices, wobei die innnere Schleife über alle berührenden
	 * Kanten des jeweiligen Knoten iteriert. Schlimmstenfalls wird also für jeden Knoten jeder mögliche Pfad untersucht
	 * -> O(|V|+|E|)
	 */
	@Override
	public GraphPath<V> getPath(V sourceVertex, V targetVertex) { // worst-case O(|V|+|E|)
		queue.clear(); // O(|remaining elements|)
		discoveredVerticesToReachingEdge.clear(); // O(|remaining elements|)

		enqueVertex(sourceVertex, null); // O(1)

		while (!queue.isEmpty()) { // O(|V|+|E|) Erläuterung, siehe oben

			VertexEdge<V> node = queue.remove(); // Entfernt das erste Elemente aus Schlange // O(1)

			V v = node.vertex;
			notifyVertexCompleted(v); // O(1)
			if (v.equals(targetVertex)) { // O(1)
				// targetVertex gefunden -> Suche beenden
				return backTracePath(sourceVertex, targetVertex, discoveredVerticesToReachingEdge); // O(|E_inPfad|)
			}

			// Expandiere den aktuellen Knoten
			for (Edge<V> edge : graph.getEdgesOf(v)) { // Schleife über alle touching edges -> O(|touchingEdges|)
				if (edge != node.reachingEdge) {
					V successor = GraphUtility.getOppositeVertex(edge, v); // O(1)

					enqueVertex(successor, edge); // O(1)
				}
			}
		}
		return null; // targetVertex nicht gefunden
	}

	private void enqueVertex(V vertex, Edge<V> reachingEdge) { // O(1)

		if (!discoveredVerticesToReachingEdge.containsKey(vertex)) { // O(1)

			VertexEdge<V> node = new VertexEdge<>(vertex, reachingEdge); // O(1)
			queue.add(node); // Fügt den Knoten als letztes Element in Schlange ein // O(1)

			markAsDiscovered(node);
		}
	}

	private void markAsDiscovered(VertexEdge<V> node) { // O(1)
		discoveredVerticesToReachingEdge.put(node.vertex, node); // O(1)
		notifyVertexDiscovered(node.vertex, node.reachingEdge); // O(1)
	}
}
