package de.iils.dc43.materialdatabase.implementations.simplemodel;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.management.InstanceNotFoundException;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractController;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractDataFileHandler;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractModel;
import de.iils.dc43.materialdatabase.implementations.modelelements.DataFile;
import de.iils.dc43.materialdatabase.implementations.modelelements.Folder;
import de.iils.dc43.materialdatabase.implementations.modelelements.Material;
import de.iils.dc43.materialdatabase.implementations.modelelements.MaterialProperty;
import de.iils.dc43.materialdatabase.implementations.modelelements.MaterialPropertyCategory;
import de.iils.dc43.materialdatabase.implementations.modelelements.RootElement;
import de.iils.dc43.materialdatabase.implementations.simplemodel.PathSearchAlgorithm.GraphPath;
import de.iils.dc43.materialdatabase.implementations.simplemodel.UndirectedWeightedSimpleGraph.Edge;

public class SimpleModel extends AbstractModel {
	private UUID rootElementId;
	private UndirectedWeightedSimpleGraph<AbstractGraphElement> modelGraph = new MapBasedGraph_Optimized<>();
	private PathSearchAlgorithm<AbstractGraphElement> searchAlgorithm;

	public void initialize(AbstractController controller) {
		super.initialize(controller);
		// clear model if there is somethin in the model
		if(!this.modelGraph.getVertices().isEmpty()) {
			this.clearModel();
		}
		 
		// adding the root element to the graph so databases can be added to it
		AbstractGraphElement graphRootElement = RootElement.getInstance();
		this.rootElementId = graphRootElement.getId();
		this.modelGraph.addVertex(graphRootElement);
		this.searchAlgorithm = new BreadthFirstSearch<>(this.modelGraph);

		// initialize file handler which means loading its data files into the model
		for (AbstractDataFileHandler fileHandler : addedFileHandler) {
			fileHandler.initialize();
		}
	}

	@Override
	public void saveModel() throws InstanceNotFoundException, Exception {
		for (AbstractDataFileHandler fileHandler : addedFileHandler) {
			fileHandler.saveAllDataFiles();
		}
	}

	@Override
	public AbstractGraphElement getElementById(UUID id) {
		Set<AbstractGraphElement> allVertices = this.modelGraph.getVertices();
		for (AbstractGraphElement element : allVertices) {
			if (element.getId().equals(id)) {
				return element;
			}
		}
		return null;
	}

	@Override
	public String getElementName(UUID id) {
		return this.getElementById(id).getName();
	}

	@Override
	public String getElementType(UUID id) {
		return this.getElementById(id).getType();
	}

	@Override
	public String getParentDataFilePath(UUID id) {
		return this.getDatafileContainingElement(this.getElementById(id)).getFilePath().toString();
	}

	@Override
	public UUID getElementParentId(UUID id) {
		return this.getElementById(id).getParentId();
	}

	@Override
	public void moveElement(UUID elementId, UUID newParentId) {
		System.out.println("Moving of elements is currently not supported");
	}

	@Override
	public String getElementNote(UUID id) {
		return this.getElementById(id).getNote();
	}

	@Override
	public Object getElementProperty(UUID elementId, String property) {
		AbstractGraphElement element = this.getElementById(elementId);
		try {
			Method method = element.getClass().getMethod("get" + property);
			return method.invoke(element);
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public UUID getRootElementId() {
		return this.rootElementId;
	}

	@Override
	public UUID getElementIdByPath(List<String> path) {
		// iterate over the elements by searching the
		// child name for the corresponding path part
		UUID elementId = this.rootElementId;
		for (String elementName : path) {
			UUID[] childElementIds = this.getElementChildIds(elementId);
			for (UUID childElementId : childElementIds) {
				if (this.getElementById(childElementId).getName().equals(elementName)) {
					elementId = childElementId;
				}
			}
		}
		// if element was found the name of the element with the element
		// id is the same as the last entry in the path
		// if this is not the case, the path is wrong
		if (this.getElementName(elementId).equals(path.get(path.size() - 1))) {
			return elementId;
		}
		return null;
	}

	@Override
	public List<String> getPathByElementId(UUID elementId) {
		// gets an id and returns the path in that way that it is compatible
		// to the abstract dataFileHandler path setting for an ElementDataExchangeClass
		// element
		// eg last path element is the parent of the element the path is searched to
		List<String> elementPath = new ArrayList<>();
		UUID parentId = this.getElementParentId(elementId);
		while (!this.getElementType(parentId).equals("root")) {
			elementPath.add(0, this.getElementName(parentId));
			elementId = parentId;
			parentId = this.getElementParentId(parentId);
		}
		return elementPath;
	}

	@Override
	public boolean isElementChildOf(UUID elementId, UUID supposedParentElementId) {
		// checks if the element behind the first element id is a child
		// of supposedParentElement

		// get element parent until root element is reached and if than the parent
		// element
		// is not found, the element is not child of the parent element
		while (!this.getElementType(elementId).equals("root") && supposedParentElementId != null) {
			if (this.getElementParentId(elementId).equals(supposedParentElementId)) {
				return true;
			}
			elementId = this.getElementParentId(elementId);
		}
		return false;
	}

	@Override
	public boolean setElementProperty(UUID elementId, String property, Object newValue) {
		if(this.getDatafileContainingElement(elementId).isProtected()) {
			return false;
		}
		AbstractGraphElement element = this.getElementById(elementId);
		try {
			Method method = element.getClass().getMethod("set" + property, newValue.getClass());
			method.invoke(element, newValue);
			return true;
		} catch (Exception ex) {
			System.out.println(ex.getCause());
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public DataFile getDatafileContainingElement(AbstractGraphElement element) {
		String elementType = element.getType();
		if (!elementType.equals("dataFile")) {
			GraphPath<AbstractGraphElement> pathToElement = this.searchAlgorithm
					.getPath(this.getElementById(rootElementId), element);
			for (AbstractGraphElement pathElement : pathToElement.getVertexList()) {
				if (pathElement.getType().equals("dataFile")) {
					return (DataFile) pathElement;
				}
			}
		} else if (element.getType().equals("dataFile")) {
			return (DataFile) element;
		}
		return null;
	}
	
	@Override
	public DataFile getDatafileContainingElement(UUID elementId) {
		AbstractGraphElement element = this.getElementById(elementId);
		return this.getDatafileContainingElement(element);
	}

	@Override
	public UUID[] getElementChildIds(UUID elementId) {
		// returns all the id's of the child elements of the GraphElement, without sub
		// child
		List<AbstractGraphElement> childElements = new ArrayList<>();
		List<UUID> childElementIds = new ArrayList<>();
		AbstractGraphElement element = this.getElementById(elementId);
		UUID parentId = this.getElementParentId(elementId);
		Set<Edge<AbstractGraphElement>> edges = this.modelGraph.getEdgesOf(element);
		for (Edge<AbstractGraphElement> edge : edges) {
			if (!edge.getVertex1().getId().equals(elementId) && !edge.getVertex1().getId().equals(parentId)) {
				childElements.add(edge.getVertex1());
			} else if (!edge.getVertex2().getId().equals(elementId) && !edge.getVertex2().getId().equals(parentId)) {
				childElements.add(edge.getVertex2());
			}
		}
		Collections.sort(childElements);
		for (AbstractGraphElement child : childElements) {
			childElementIds.add(child.getId());
		}

		return childElementIds.toArray(new UUID[0]);
	}
	
	@Override
	public void clearModel() {
		// get root element and delete all datafiles
		UUID[] dataFileIds = this.getElementChildIds(this.rootElementId);
		for(UUID dataFileId:dataFileIds) {
			AbstractGraphElement dataFileElement = this.getElementById(dataFileId);
			// since the model is saved changing the model does nothing
			// so the protections are not important anymore and
			// so force remove can be used
			this.forceRemoveChildrenAndElement(dataFileElement);
		}
	}

	// operations performed on graph
	@Override
	public void addElement(AbstractGraphElement element, UUID parent) {
		AbstractGraphElement parentElement = this.getElementById(parent);
		this.addElement(element, parentElement);
	}

	public void addElement(DataFile dataFile) {
		this.addElement(dataFile, RootElement.getInstance().getId());
	}

	public void addElement(AbstractGraphElement element, AbstractGraphElement parent) throws IllegalArgumentException {
		// check if the datafile is protected and forbid changes it its the case
		if(!element.getType().equals("dataFile") && this.getDatafileContainingElement(parent).isProtected()) {
			this.raiseError("Could not add element to protected datafile");
			return;
		}
		// check if the id of the element already exists in the model
		// this is important since the UUID is the only way to track the element in
		// the model and therefore is uniqueness has to be secured
		if(this.getElementById(element.getId()) != null) {
			this.raiseError("Adding Element named '"+element.getName()+"' to parent '"+parent.getName()+"'"
					+ " does not work since the UUID of the element is already in the model.\nSkipping");
			return;
		}
		
		
		// this functions checks that the structure of the model is kept when adding an element
		if (((parent.getType().equals("folder") || parent.getType().equals("dataFile"))
				&& (element.getType().equals("folder") || element.getType().equals("material")))
				|| parent.getType().equals("root") && element.getType().equals("dataFile")) {
			
			//this check the case that the maximum folder depth is 2
			if(element.getType().equals("folder") && parent.getType().equalsIgnoreCase("folder")) {
				List<String> pathToParentElement = this.getPathByElementId(parent.getId());
				if(pathToParentElement.size()>1) {
					// the maximum folder depth of 2 is exceeded and therefore the
					// folder will be added to the parent of the parent
					AbstractGraphElement parentParent = this.getElementById(parent.getParentId());
					this.addElementToModel(element, parentParent);
					return;
				}
			}
			this.addElementToModel(element, parent);
			return;

		} else if (parent.getType().equals("material") && !element.getType().equals("category")
				&& !element.getType().equals("property")) {
			// if there is something added to a material it gets added to its parent instead
			parent = this.getElementById(parent.getParentId());
			
			// this checks that the maximum folder depth of 2 is not exceeded
			List<String> pathToParentElement = this.getPathByElementId(parent.getId());
			if(pathToParentElement.size()>1) {
				// the maximum folder depth of 2 is exceeded and therefore the
				// folder will be added to the parent of the parent
				AbstractGraphElement parentParent = this.getElementById(parent.getParentId());
				this.addElementToModel(element, parentParent);
				return;
			}
			this.addElementToModel(element, parent);
			return;

		} else if (parent.getType().equals("material") && element.getType().equals("category")) {
			// adding category to material
			parent = this.getElementById(parent.getId());
			this.addElementToModel(element, parent);
			return;

		} else if (parent.getType().equals("category") && element.getType().equals("property")) {
			parent = this.getElementById(parent.getId());
			this.addElementToModel(element, parent);
			return;
		} else {
			throw new IllegalArgumentException(
					"Can not add element: '" + element.getName() + "' of type: '" + element.getType()
							+ "' to parent element: '" + parent.getName() + "' of type: '" + parent.getType() + "'");
		}
	}

	@Override
	public void removeElement(UUID id) {
		this.removeElement(this.getElementById(id));
	}

	@Override
	public void removeElement(AbstractGraphElement element) {
		// check if element parent datafile is protected and
		// refuse changes if that is the case
		if(this.getDatafileContainingElement(element).isProtected()) {
			this.raiseError("Could not remove element from protected datafile");
			return;
		}
		if (element instanceof DataFile) {
			this.removeDataFileFromHandler((DataFile) element);
		}
		this.removeChildrenAndElement(element);
		this.firePropertyChange(element.getParentId().toString(), element.getId().toString(), null);
	}

	///////////////////////////// functions only used by the graph
	private void removeChildrenAndElement(AbstractGraphElement element) {
		// solved with recursion due to the assumption that there will be not a lot of
		// recursion calls
		Set<Edge<AbstractGraphElement>> connections = this.modelGraph.getEdgesOf(element);
		List<AbstractGraphElement> children = new ArrayList<>();

		// iterate through all connections and add the opposite element of folder to the
		// children list
		// which contains the elements to be removed
		for (Edge<AbstractGraphElement> edgeToChild : connections) {
			if (!edgeToChild.getVertex1().getId().equals(element.getParentId())
					&& !edgeToChild.getVertex2().getId().equals(element.getParentId())) {
				if (!edgeToChild.getVertex1().getId().equals(element.getId())) {
					children.add(edgeToChild.getVertex1());
				} else {
					children.add(edgeToChild.getVertex2());
				}
			}
		}
		// remove operation called reverse for every children
		for (AbstractGraphElement childElement : children) {
			this.removeElement(childElement);
		}
		this.modelGraph.removeVertex(element);
	}

	private void removeDataFileFromHandler(DataFile dataFile) {
		Path dataFileToRemovePath = dataFile.getFilePath();
		List<AbstractDataFileHandler> dataFileHandlers = this.addedFileHandler;
		for (AbstractDataFileHandler dataFileHandler : dataFileHandlers) {
			if (dataFileHandler.getDataFilePaths().contains(dataFileToRemovePath)) {
				dataFileHandler.removeFilePath(dataFileToRemovePath);
				return;
			}
		}
	}
	
	public void forceRemoveElement(AbstractGraphElement element) {
		// this should only be used by experienced since it will not care
		// for protections
		if (element instanceof DataFile) {
			this.removeDataFileFromHandler((DataFile) element);
		}
		this.forceRemoveChildrenAndElement(element);
		this.firePropertyChange(element.getParentId().toString(), element.getId().toString(), null);
	}
	
	private void forceRemoveChildrenAndElement(AbstractGraphElement element) {
		// solved with recursion due to the assumption that there will be not a lot of
		// recursion calls
		Set<Edge<AbstractGraphElement>> connections = this.modelGraph.getEdgesOf(element);
		List<AbstractGraphElement> children = new ArrayList<>();

		// iterate through all connections and add the opposite element of folder to the
		// children list which contains the elements to be removed
		for (Edge<AbstractGraphElement> edgeToChild : connections) {
			if (!edgeToChild.getVertex1().getId().equals(element.getParentId())
					&& !edgeToChild.getVertex2().getId().equals(element.getParentId())) {
				if (!edgeToChild.getVertex1().getId().equals(element.getId())) {
					children.add(edgeToChild.getVertex1());
				} else {
					children.add(edgeToChild.getVertex2());
				}
			}
		}
		// remove operation called reverse for every children
		for (AbstractGraphElement childElement : children) {
			this.forceRemoveElement(childElement);
		}
		this.modelGraph.removeVertex(element);
	}

	private boolean addElementToModel(AbstractGraphElement element, AbstractGraphElement parent) {
		// ads an element to the assingned model and returns false if this did not work
		if (this.modelGraph.addVertex(element)) {
			if (this.modelGraph.addEdge(element, parent, 1)) {
				element.setParentId(parent.getId());
				element.addPropertyChangeListener(this.getPropertyChangeListeners()[0]);
				this.firePropertyChange(parent.getId().toString(), null, element.getId().toString());
				return true;
			}
		}
		return false;
	}
}
