package de.iils.dc43.materialdatabase.implementations.simplemodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ElementDataExchangeClass implements Comparable<ElementDataExchangeClass>{
	// this is the path to the parent element with its root being the corresponding datafile
	// example:
	// path in Model: "DataFile.abd"/"Parent1"/"parent2"/currentElementDataExchangeClass
	// pathToParentElement: ["DataFile.abd", "Parent1", "parent2"] with the next element being the
	// model element corresponding to the current ElementDataExchange class
	private List<String> pathToParentElement = new ArrayList<>();
	
	private String type;
	private Map<String,Object> dataValues = new HashMap<>();
	
	
	public ElementDataExchangeClass(String type, List<String> pathToParentElement) {
		this.pathToParentElement = pathToParentElement;
		this.type = type;
	}
	
	public ElementDataExchangeClass(String type) {
		this.type = type;
	}
	
	public void addDataValue(String attributeName, Object attributeValue) {
		this.dataValues.put(attributeName, attributeValue);
	}
	
	public Object getDataValue(String attributeName) {
		return this.dataValues.get(attributeName);
	}
	
	public Map<String,Object> getAllDataValues(){
		return this.dataValues;
	}

	public String getType() {
		return type;
	}

	public List<String> getPathToParentElement() {
		return pathToParentElement;
	}
	
	public int getPathToParentElementSize() {
		return pathToParentElement.size();
	}

	@Override
	public int compareTo(ElementDataExchangeClass element) {
		// this function is needed so the elements are sorted from elements more upt the tree
		// to elements which are leaves to ensure that there is no element added to the
		// model or dataFileModel whose parents are not added
		return this.getPathToParentElementSize() < element.getPathToParentElementSize() ? -1
				: this.getPathToParentElementSize() > element.getPathToParentElementSize() ? 1
				:0;
	}
	
	// used for the hash set if needed
    @Override
    public boolean equals(Object obj) {
            ElementDataExchangeClass otherObj = (ElementDataExchangeClass) obj;
            // compare path, toString, type and datavalues to be sure
            // it is the same element but on a different reference/place in memory
            return this.toString().equals(otherObj.toString()) &&
                     this.getType().equals(otherObj.getType()) &&
                     this.getAllDataValues().equals(otherObj.getAllDataValues()) &&
                     this.pathToParentElement.equals(otherObj.getPathToParentElement());
    }
    
    // used for hash set so no two complete identical objects can be in the
    // hash set
    @Override
    public int hashCode() {
    	// this is basically not needed
        return (43 + 777);
}
	
	@Override
	public String toString() {
		return this.pathToParentElement.toString() + this.dataValues.toString() + " Type: "+ this.getType();
	}
}
