package de.iils.dc43.materialdatabase.implementations.simplemodel;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of {@link UndirectedWeightedSimpleGraph} based on a vertex-to-touching-edges-Map and a edges-Set.
 *
 * @author Hannes Wellmann
 */
public class MapBasedGraph_Optimized<V> extends UndirectedWeightedSimpleGraph<V>{

	private final Map<V, Set<Edge<V>>> vertexToTouchingEdges;
	
	public MapBasedGraph_Optimized() {
		this.vertexToTouchingEdges = new HashMap<>();
	}

	@Override
	public boolean addVertex(V vertex) { // O(1)
		boolean containsVertex = containsVertex(vertex); // O(1)
		if (containsVertex) {
			return false;
		}
		vertexToTouchingEdges.put(vertex, new HashSet<>()); // O(1)
		return true;
	}

	@Override
	public boolean removeVertex(V vertex) { // O(|E_touching|)
		if (!containsVertex(vertex)) { // O(1)
			return false;
		}
		Set<Edge<V>> touchingEdges = vertexToTouchingEdges.remove(vertex); // O(1)

		// Entferne edges aus touching-edges Sets der gegenüberliegenden Knoten
		for (Edge<V> edge : touchingEdges) { // Schleife über alle touching edges -> O(|touchingEdges|)
			V opposite = GraphUtility.getOppositeVertex(edge, vertex); // O(1)
			Set<Edge<V>> oppositesTouchingEdges = vertexToTouchingEdges.get(opposite); // O(1)
			oppositesTouchingEdges.remove(edge); // O(1)
		}

		return true;
	}

	@Override
	public boolean addEdge(V vertex1, V vertex2, int weight) { // O(1)
		checkEdgeRequirements(vertex1, vertex2, weight);
		
		if (!containsVertex(vertex1) || !containsVertex(vertex2)) { // jeweils O(1)
			throw new IllegalStateException("Vertices touching the edge not in the graph");
		}

		Edge<V> edge = new Edge<>(vertex1, vertex2, weight); // O(1)
		
		Set<Edge<V>> edges1 = vertexToTouchingEdges.get(vertex1); // O(1)
		Set<Edge<V>> edges2 = vertexToTouchingEdges.get(vertex2); // O(1)

		// Zuerst pruefen, ob es nicht schon eine solche Kante gibt
		if (edges1.contains(edge) || edges2.contains(edge)) { // O(1)
			return false; // Edge ist bereits vorhanden
		}

		// Neue edge jeweils zum Set der touching-edges beider vertices hinzufügen
		edges1.add(edge); // O(1)
		edges2.add(edge); // O(1)
		return true;
	}

	@Override
	public boolean removeEdge(V vertex1, V vertex2) { // O(1)
		// Siehe SetBasedGraph für Erläuterung zur Test-Edge
		Edge<V> testEdge = new Edge<>(vertex1, vertex2, 1); // Gewicht in hashcode()/equals() nicht berücksichtigt
		
		Set<Edge<V>> edges1 = vertexToTouchingEdges.get(vertex1); // O(1)
		Set<Edge<V>> edges2 = vertexToTouchingEdges.get(vertex2); // O(1)

		if (edges1.contains(testEdge) && edges2.contains(testEdge)) {
			edges1.remove(testEdge); // O(1)
			edges2.remove(testEdge); // O(1)
			return true;
		}
		return false;
	}

	@Override
	public boolean containsVertex(V vertex) { // O(1)
		return vertexToTouchingEdges.containsKey(vertex); // O(1)
	}

	@Override
	public boolean containsEdge(V vertex1, V vertex2) { // O(1)
		// Siehe SetBasedGraph für Erläuterung zur Test-Edge

		Edge<V> testEdge = new Edge<>(vertex1, vertex2, 1); // Gewicht in hashcode()/equals() nicht berücksichtigt
		return vertexToTouchingEdges.get(vertex1).contains(testEdge); // O(1)
	}

	@Override
	public Set<Edge<V>> getEdgesOf(V vertex) { // O(1)
		if (vertexToTouchingEdges.containsKey(vertex)) {
			return vertexToTouchingEdges.get(vertex); // O(1)
		} else { // O(1)
			return Collections.emptySet();
		}
	}

	@Override
	public Set<V> getVertices() { // O(1)
		return vertexToTouchingEdges.keySet();
	}

	@Override
	public Set<Edge<V>> getEdges() { // O(|Anzahl Vertex|*|Anzahl Knoten|)
		Set<Edge<V>> resultEdges = new HashSet<>(); // O(1)
		for (Set<Edge<V>> edgeHashSet : vertexToTouchingEdges.values()) { // O(|Anzahl Vertex|)
			for(Edge<V> edge : edgeHashSet) {  // O(|Anzahl edges des Vertex|)
				resultEdges.add(edge);  // O(1)
			}
		}
		return resultEdges;
	}
}
