package de.iils.dc43.materialdatabase.implementations.simplecontroller;

import java.beans.PropertyChangeEvent;
import java.util.UUID;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractAction;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractController;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractModel;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractView;
import de.iils.dc43.materialdatabase.implementations.actions.DebugTask;
import de.iils.dc43.materialdatabase.implementations.actions.DebugTask2;
import de.iils.dc43.materialdatabase.implementations.datafilehandler.MatMlFileHandler;
import de.iils.dc43.materialdatabase.implementations.datafilehandler.SimpleFileHandler;
import de.iils.dc43.materialdatabase.implementations.modelelements.Folder;
import de.iils.dc43.materialdatabase.implementations.modelelements.Material;
import de.iils.dc43.materialdatabase.implementations.modelelements.MaterialProperty;
import de.iils.dc43.materialdatabase.implementations.modelelements.MaterialPropertyCategory;
import de.iils.dc43.materialdatabase.implementations.simplemodel.SimpleModel;
import de.iils.dc43.materialdatabase.implementations.simpleview.SimpleView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class SimpleController extends AbstractController {

	@Override
	public void setModelElementProperty(UUID elementId, String propertyName, Object newValue) {
		if (!this.registeredModel.setElementProperty(elementId, propertyName, newValue)) {
			String name = (String) this.getModelElementProperty(elementId, "Name");
			this.raiseError("'" + propertyName + "' of element '" + name + "' could" + " not be set.");
		}
	}

	@Override
	public Object getModelElementProperty(UUID elementId, String propertyName) {
		Object result = this.registeredModel.getElementProperty(elementId, propertyName);
		if (result == null) {
			this.raiseError("Could not get property '" + propertyName + "' of Element "
					+ this.registeredModel.getElementName(elementId) + ". ");
			return "";
		}
		return result;
	}

	@Override
	public AbstractGraphElement getDatafileContainingElement(AbstractGraphElement element) {
		return this.registeredModel.getDatafileContainingElement(element);
	}

	@Override
	public void addMaterial(UUID parentId) {
		this.registeredModel.addElement(new Material("New Material"), parentId);
	}

	@Override
	public void addMaterialWithGeneralCategory(UUID parentId) {
		// add material to the model
		Material newMaterial = new Material("new Material");
		UUID newMaterialId = newMaterial.getId();
		this.registeredModel.addElement(newMaterial, parentId);
		// check if the element was correctly added
		if (this.registeredModel.getElementById(newMaterialId) != null) {
			// add general property category to the material
			MaterialPropertyCategory generalCategory = new MaterialPropertyCategory("General");
			this.registeredModel.addElement(generalCategory, newMaterialId);
		}
	}

	@Override
	public void addFolder(UUID parentId) {
		this.registeredModel.addElement(new Folder("New Folder"), parentId);
	}

	@Override
	public void addMaterialPropertyCategory(UUID parentId) {
		this.registeredModel.addElement(new MaterialPropertyCategory("New Category"), parentId);
	}

	@Override
	public void addMaterialProperty(UUID parentId) {
		this.registeredModel.addElement(new MaterialProperty("New Property", 0, ""), parentId);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		this.registeredView.modelPropertyChange(evt);
	}

	@Override
	public void runTask(String name) {
		for (AbstractAction task : this.registeredTasks) {
			if (task.getName().equals(name)) {
				task.performTask();
				return;
			}
		}
	}

	@Override
	public void raiseError(String description) {
		// public functions to raise errors in the view
		this.registeredView.raiseError(description);
	}

	@Override
	public int showDecisionWindow(String description, String[] options) {
		// triggers the decision window in the view
		return this.registeredView.showDecisionWindow(description, options);
	}

	@Override
	public void removeElement(UUID id) {
		this.registeredModel.removeElement(id);
	}

	@Override
	public List<UUID> getMaterialPropertyIds(UUID materialId) {
		List<UUID> materialPropertiesIds = new ArrayList<>();
		// get all category ids of the material categories
		UUID[] categoryIds = this.registeredModel.getElementChildIds(materialId);
		// get the properties in the categories
		for (UUID categoryId : categoryIds) {
			UUID[] categoryPropertyIds = this.getModelElementChildIds(categoryId);
			for (UUID propertyId : categoryPropertyIds) {
				materialPropertiesIds.add(propertyId);
			}
		}
		return materialPropertiesIds;
	}

	@Override
	public String getFirstSubCategoryNameOfTheMaterial(UUID materialId) {
		// get the parents of an material until the top sub category of the material is
		// found
		// and return its name
		AbstractGraphElement givenMaterial = this.registeredModel.getElementById(materialId);
		if (givenMaterial instanceof Material) {
			AbstractGraphElement parentElement = this.registeredModel.getElementById(givenMaterial.getParentId());
			AbstractGraphElement lastParentElement = null;
			while (!parentElement.getType().equals("dataFile")) {
				if (parentElement.getType().equals("root")) {
					return null;
				}
				lastParentElement = parentElement;
				parentElement = this.registeredModel.getElementById(parentElement.getParentId());
			}
			return lastParentElement.getName();
		}
		return null;
	}

	@Override
	public String getCategoryNameOfMaterialProperty(UUID materialPropertyId) throws IllegalArgumentException {
		AbstractGraphElement property = this.registeredModel.getElementById(materialPropertyId);
		// check if the ID exists and if the id is corresponding to a materialProperty
		if (property != null && property instanceof MaterialProperty) {
			UUID propertyCategoryId = property.getParentId();
			return this.registeredModel.getElementName(propertyCategoryId);
		}
		throw new IllegalArgumentException(
				"No corresponding property for ID '" + materialPropertyId.toString() + "' found.");
	}

	@Override
	public String getUnitOfMaterialProperty(UUID materialPropertyId) throws IllegalArgumentException {
		AbstractGraphElement property = this.registeredModel.getElementById(materialPropertyId);
		// check if the ID exists and if the id is corresponding to a materialProperty
		if (property != null && property instanceof MaterialProperty) {
			MaterialProperty materialProperty = (MaterialProperty) property;
			return materialProperty.getUnit();
		}
		throw new IllegalArgumentException(
				"No corresponding property for ID '" + materialPropertyId.toString() + "' found.");
	}
	
	@Override
	public void clearModel() {
		this.registeredModel.clearModel();
	}
}
