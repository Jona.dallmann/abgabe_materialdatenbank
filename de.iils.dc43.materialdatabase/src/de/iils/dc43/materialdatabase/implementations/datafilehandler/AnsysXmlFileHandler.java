package de.iils.dc43.materialdatabase.implementations.datafilehandler;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;

import de.iils.dc43.materialdatabase.implementations.simplemodel.ElementDataExchangeClass;

public class AnsysXmlFileHandler extends MatMlFileHandler {

	@Override
	protected void transformPropertyNodeToElementDataExchangeClass(Node propertyDataNode, Map<String,Node> metaDataIdToNodeMap,
			ArrayList<String> pathToGeneralPropertyCategory) {
		// override transforming property to make this due to the Ansys mat ml style
		// create placeholder for the property element exchange class
		ElementDataExchangeClass transformedPropertyNode = null;

		// get path to the material
		ArrayList<String> pathToMaterial =
				(ArrayList<String>) pathToGeneralPropertyCategory.clone();
		pathToMaterial.remove(pathToMaterial.size() - 1);
		
		// get id of the property
		String propertyId = propertyDataNode.getAttributes().getNamedItem("property").getTextContent();
		
		// get property node from metadata
		Node propertyDetailsNodeFromMetadata = (Node) metaDataIdToNodeMap.get(propertyId);

		// check if there is a category for the material property given.
		// if so, create the category and add it to the path
		if (propertyDetailsNodeFromMetadata != null) {

			// get name of the category if it exists
			Node propertyCategoryNameNode =
					propertyDetailsNodeFromMetadata.getAttributes().getNamedItem("type");

			// create property category if the propertyCategory of the property exists
			if (propertyCategoryNameNode != null) {
				String propertyCategoryName = propertyCategoryNameNode.getTextContent();

				// create element for the property category
				ElementDataExchangeClass propertyCategory =
						new ElementDataExchangeClass("category", pathToMaterial);
				propertyCategory.addDataValue("name", propertyCategoryName);

				// add element to transformed elements list
				this.transformedElements.add(propertyCategory);

				// create path to this new property category
				ArrayList<String> pathPropertyCategory =
						(ArrayList<String>) pathToMaterial.clone();

				// add material name so the parent is set
				pathPropertyCategory.add(propertyCategoryName);

				// create ElementDataExchange Class object with the path to the property
				// category
				transformedPropertyNode =
						new ElementDataExchangeClass("property", pathPropertyCategory);

			} else {
				// create ElementDataExchange Class object with the path to the property
				// category
				transformedPropertyNode =
						new ElementDataExchangeClass("property", pathToGeneralPropertyCategory);
			}

			// get the name of the property and set it
			String propertyName = this.getFirstChildNodeWithName(propertyDetailsNodeFromMetadata, "Name")
					.getTextContent().strip();
			transformedPropertyNode.addDataValue("name", propertyName);

			// get and set note for the material property
			Node materialPropertyNoteNode = this.getFirstChildNodeWithName(propertyDetailsNodeFromMetadata, "Notes");
			if (materialPropertyNoteNode != null) {
				transformedPropertyNode.addDataValue("note", materialPropertyNoteNode.getTextContent());
			}

			// get and set unit to the transformed property node
			String propertyUnit = this.getUnitOfPopertyNode(propertyDetailsNodeFromMetadata);
			transformedPropertyNode.addDataValue("unit", propertyUnit);

		} else {
			// create ElementDataExchange Class object with the path to the property
			// category
			transformedPropertyNode = new ElementDataExchangeClass("property", pathToGeneralPropertyCategory);
			transformedPropertyNode.addDataValue("name", "No Name found");
		}

//		// get value of the property and decide between lut and constant value
//		if (this.getFirstChildNodeWithName(propertyDataNode, "ParameterValue") != null) {
//			// case that there is a lut in the property
//			// get data and parameter value nodes and create the hashmap for the lut out of
//			// this
//			Node dataNode = this.getFirstChildNodeWithName(propertyDataNode, "Data");
//			Node parameterValueNode = this.getFirstChildNodeWithName(propertyDataNode, "ParameterValue");
//
//			String[] dataNodeStringValues = dataNode.getTextContent().split(",");
//			String[] parameterValueStringValues = parameterValueNode.getTextContent().split(",");
//
//			Map<Double, Double> lutMap = new HashMap<>();
//			for (int valueIndex = 0; valueIndex < dataNodeStringValues.length; valueIndex++) {
//				Double parameterValue = Double.parseDouble(parameterValueStringValues[valueIndex]);
//				Double dataValue = Double.parseDouble(dataNodeStringValues[valueIndex]);
//				lutMap.put(parameterValue, dataValue);
//			}
//			transformedPropertyNode.addDataValue("value", lutMap);
//
//		} else {
//			// case that there is no lut in the Property
//			Node propertyValueNode = this.getFirstChildNodeWithName(propertyDataNode, "Data");
//			double propertyValue = Double.parseDouble(propertyValueNode.getTextContent());
//			transformedPropertyNode.addDataValue("value", propertyValue);
//		}
		transformedPropertyNode.addDataValue("value", 0.0);
		this.transformedElements.add(transformedPropertyNode);
	}
	
	@Override
	protected String getMaterialNotes(Node bulkDetailsNode) {
		Node materialNotesNode = this.getFirstChildNodeWithName(bulkDetailsNode, "Description");
		if (materialNotesNode != null) {
			return materialNotesNode.getTextContent();
		}
		return null;
	}

	@Override
	public void elementListToFile(Path dataFilePath, List<ElementDataExchangeClass> elements) {
		// TODO Auto-generated method stub
		this.raiseError("Saving Option not supported for AsysXMLFileManager.\nSaving '" + dataFilePath.toString()
				+ "' Skipped");
	}

}
