package de.iils.dc43.materialdatabase.implementations.datafilehandler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import javax.management.InstanceNotFoundException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.jface.resource.DataFormatException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractXmlDataFileHandler;
import de.iils.dc43.materialdatabase.implementations.simplemodel.ElementDataExchangeClass;

// it is important for this file manager to have an XSD
// file loaded. otherwise the correct structure of the fileHandler
// secured load xsd file with
// *FilemanagerInstancename*.addXsdFile("Path/To/MatML/Xsd/File")
public class MatMlFileHandler extends AbstractXmlDataFileHandler {
	// list of transformed elements which can be transformed to the model/graph
	// elements
	protected Set<ElementDataExchangeClass> transformedElements = new HashSet<>();

	// dataFileElement to have the root so the paths can be set
	protected ElementDataExchangeClass dataFileElement;

	@Override
	protected List<ElementDataExchangeClass> fileToElementList(Path dataFilePath)
			throws DataFormatException, ParserConfigurationException, SAXException, IOException {
		
		// check if an .xsd scheme exists, otherwise this filehandler cant be used
		if(this.getXsdSchemeFilePath() == null) {
			throw new FileNotFoundException("XSD scheme file for matMl File handler needed.\nPlease insert with "+
		"*FilemanagerInstancename*.addXsdFile(\"Path/To/MatML/Xsd/File\").\nSkipped Loading File '"
					+dataFilePath.toString()+"'");
		}
		
		// clear transformed elements set, which prevents elements from a previous data
		// file from appearing in the current transformed data file
		this.transformedElements = new HashSet<>();

		// create data file element for the document as root for the model and
		// add it to the list of transformed elements
		this.dataFileElement = new ElementDataExchangeClass("dataFile");
		this.dataFileElement.addDataValue("name", dataFilePath.getFileName().toString());
		this.transformedElements.add(dataFileElement);

		// get root element
		Node rootNode = this.getXmlRootNode(dataFilePath);

		// search in the xml document for the matml nodes named "MatML_Doc"
		List<Node> matMlNodes = this.searchForMatMlDocNodesIn(rootNode);
		
		// return if no matml nodes where found
		if(matMlNodes.size() == 0) {
			throw new IOException("No XML nodes named \"MatML_Doc\" found in '"
					+dataFilePath.toString()+"'.");
		}
		
		// get notes for the datafile out of the commentary at the first mat ml node
		String notes = this.getFirstCommentInMatMlNode(matMlNodes.get(0));
		if(notes != null) {
			this.dataFileElement.addDataValue("note", notes);
		}

		// validate the mat ml nodes and process them
		for (int matmlNodeIndex = 0; matmlNodeIndex < matMlNodes.size(); matmlNodeIndex++) {
			Node currentMatMlNode = matMlNodes.get(matmlNodeIndex);
			// validate node due to its correctness and if it is
			// not correct (e.g. the validation function throws an error)
			// continue with the next node
			try {
				this.validateSubtreeNodeAgainstXsd(currentMatMlNode);
			} catch (SAXException | IOException ex) {
				this.raiseError("Error Loading a MatMlNode in '" + dataFilePath.toString()
						+ "'\nXmlFile does not match given Scheme.\nMat Ml Node index is " + matmlNodeIndex
						+ "\n\nParsed Error message:\n" + ex.toString());
				continue;
			}

			// get the Material nodes in the matml doc node
			// (one mat ml can contain a unbounded number of them due to matml xsd
			// definition)
			List<Node> materialNodes = this.getAllChildNodesWithName(currentMatMlNode, "Material");

			// get the meta data node if it exists, it is a child of the matMl Node
			Node metaDataNode = this.getFirstChildNodeWithName(currentMatMlNode, "Metadata");

			// create a map mapping the id of the metadata node to its node if the metadata
			// node exists
			Map<String, Node> metaDataIdToNodeMap = null;
			if (metaDataNode != null) {
				metaDataIdToNodeMap = this.createIdToNodeMapFromMetaDataNode(metaDataNode);
			}

			// get the bulk details of every material node and transform it to
			// ElementDataExchangeClass
			for (int materialNodeIndex = 0; materialNodeIndex < materialNodes.size(); materialNodeIndex++) {
				Node currentMaterialNode = materialNodes.get(materialNodeIndex);

				if (metaDataNode != null) {
					// catch any errors so next material could be processed
					try {
						// transfrom the material note to the corresponding
						// element data exchange class
						this.materialNodeToElementDataExchangeClass(currentMaterialNode, metaDataIdToNodeMap);
					} catch (Exception ex) {
						this.raiseError("Error transforming Material with index '" + materialNodeIndex
								+ "' to DataElementExchangeClass in file " + dataFilePath.toString() + "\n"
								+ ex.getMessage());
						continue;
					}
				}
			}

		}
		return new ArrayList<ElementDataExchangeClass>(this.transformedElements);
	}

	private void materialNodeToElementDataExchangeClass(Node materialNode, Map<String, Node> metaDataIdToNodeMap) {
		// this is needed since most the needed information for the material are in the
		// bulk details node of the material node
		Node bulkDetailsNode = this.getFirstChildNodeWithName(materialNode, "BulkDetails");

		// get path to the element and create the elements for the path
		ArrayList<String> pathToMaterialParent = new ArrayList<>();
		pathToMaterialParent.add((String) this.dataFileElement.getDataValue("name"));
		Node classNode = this.getFirstChildNodeWithName(bulkDetailsNode, "Class");
		Node subclassNode = this.getFirstChildNodeWithName(bulkDetailsNode, "Subclass");

		// since class is optional check if it exists
		if (classNode != null) {
			// get relevant elements to transform the class to the model and transform
			List<String> pathToClassNode = new ArrayList<>();
			pathToClassNode.add((String) this.dataFileElement.getDataValue("name"));
			String classNodeName = classNode.getTextContent().strip();
			ElementDataExchangeClass transformedClassNode = new ElementDataExchangeClass("folder", pathToClassNode);
			transformedClassNode.addDataValue("name", classNodeName);

			// set path of the material to class and add element to transformed material
			// nodes
			this.transformedElements.add(transformedClassNode);
			pathToMaterialParent.add(classNodeName);
		}

		// since subclass is optional check if it exists
		if (subclassNode != null) {
			// get relevant elements to transform the subclass to the model and transform
			List<String> pathToSubClassNode = new ArrayList<>();
			pathToSubClassNode.add((String) this.dataFileElement.getDataValue("name"));
			// if there is a class set, the path of the subclass has to have the class as
			// parent
			if (classNode != null) {
				pathToSubClassNode.add(classNode.getTextContent().strip());
			}
			String subClassNodeName = subclassNode.getTextContent().strip();
			ElementDataExchangeClass transformedSubClassNode = new ElementDataExchangeClass("folder",
					pathToSubClassNode);
			transformedSubClassNode.addDataValue("name", subClassNodeName);

			this.transformedElements.add(transformedSubClassNode);
			pathToMaterialParent.add(subclassNode.getTextContent().strip());
		}
		
		// create the elementDataExchange Class element
		ElementDataExchangeClass transformedMaterialNode = new ElementDataExchangeClass("material",
				pathToMaterialParent);

		// check if the Material has an id in the matMl file, extract it
		// and replace the created one, so the id for a material is the
		// same after every restart of the material Db which helps
		// tracking the material in the dc43
		UUID materialUuid = null;
		Node materialIdNode = materialNode.getAttributes().getNamedItem("id");
		if (materialIdNode != null) {
			String materialId = materialIdNode.getTextContent();
			materialUuid = UUID.fromString(materialId);
		}

		// add the id to the materialExchangeElement if the id exists
		if (materialUuid != null) {
			transformedMaterialNode.addDataValue("id", materialUuid);
		}

		// get the name of the material
		Node materialNameNode = this.getFirstChildNodeWithName(bulkDetailsNode, "Name");
		String materialName = materialNameNode.getTextContent().strip();

		// add name to the element
		transformedMaterialNode.addDataValue("name", materialName);

		// get the notes for the Material if they exist
		String materialNotes = this.getMaterialNotes(bulkDetailsNode);
		if (materialNotes != null) {
			transformedMaterialNode.addDataValue("note", materialNotes);
		}

		// get the path to the property parent by cloning the pathToMaterialParent to
		// have no problems with the references
		ArrayList<String> pathToPropertyParent = (ArrayList<String>) pathToMaterialParent.clone();

		// add material name so the parent is set
		pathToPropertyParent.add(materialName);

		// create an general category so the properties with no category can be inserted
		// here
		ElementDataExchangeClass generalPropertyCategory = new ElementDataExchangeClass("category",
				pathToPropertyParent);
		String generalPropertyCategoryName = "General";
		generalPropertyCategory.addDataValue("name", generalPropertyCategoryName);
		this.transformedElements.add(generalPropertyCategory);

		// get the path to the property parent by cloning the pathToMaterialParent to
		// have no problems with the references
		ArrayList<String> pathToGeneralPropertyCategory = (ArrayList<String>) pathToPropertyParent.clone();

		// add material name so the parent is set
		pathToGeneralPropertyCategory.add(generalPropertyCategoryName);

		// get all property child nodes of the material
		List<Node> propertyChildNodes = this.getAllChildNodesWithName(bulkDetailsNode, "PropertyData");

		// transform the propertyDataNodes to ElementDataExchange class elements
		for (Node propertyDataNode : propertyChildNodes) {
			try {
				this.transformPropertyNodeToElementDataExchangeClass(propertyDataNode, metaDataIdToNodeMap, pathToGeneralPropertyCategory);
			} catch (Exception ex) {
				// get property id for debugging
				String propertyId = propertyDataNode.getAttributes().getNamedItem("property").getTextContent();
				this.raiseError("Could not import property with id '" + propertyId + "' of material '" + materialName
						+ "' from file '" + this.dataFileElement.getDataValue("name") + "'");
			}
		}
		this.transformedElements.add(transformedMaterialNode);
	}

	// general functions for converting the file to the element list
	private String getFirstCommentInMatMlNode(Node matMlNode) {
		NodeList matMlNodeChildNodes = matMlNode.getChildNodes();
		for(int nodeIndex=0;nodeIndex<matMlNodeChildNodes.getLength();nodeIndex++) {
			Node currentNode = matMlNodeChildNodes.item(nodeIndex);
			if(currentNode.getNodeType() == Node.COMMENT_NODE) {
				String notes = currentNode.getTextContent();
				return notes;
			}
		}
		return null;
	}
	
	protected String getUnitOfPopertyNode(Node propertyDetailNode) {
		String unitAsString = "";
		// check if there is a unit class given
		Node unitsNode = this.getFirstChildNodeWithName(propertyDetailNode, "Units");
		if (unitsNode != null) {
			// get factor
			Node propertyUnitFactorNode = this.getFirstChildNodeWithName(unitsNode, "Factor");
			if (propertyUnitFactorNode != null) {
				unitAsString = unitAsString + propertyUnitFactorNode.getTextContent().strip() + "*";
			}
			NodeList propertyUnitsNodeChilds = unitsNode.getChildNodes();
			for (int index = 0; index < propertyUnitsNodeChilds.getLength(); index++) {
				Node propertyDetailChildNode = propertyUnitsNodeChilds.item(index);
				// only get unit nodes and concatenate them to a unit
				if (propertyDetailChildNode.getNodeName().equals("Unit")) {
					String unitPartString = propertyDetailChildNode.getTextContent().strip();
					unitAsString = unitAsString + unitPartString;
					// add power if given
					Node unitFactorNode = propertyDetailChildNode.getAttributes().getNamedItem("power");
					if (unitFactorNode != null) {
						String factor = unitFactorNode.getTextContent().strip();
						unitAsString = unitAsString + "^(" + factor + ")";
					}
					unitAsString = unitAsString + "*";
				}
			}
			// delete last "*"
			if (unitAsString.length() > 0 && unitAsString.charAt(unitAsString.length() - 1) == '*') {
				unitAsString = unitAsString.substring(0, unitAsString.length() - 1);
			}
			return unitAsString;

		} else if (this.getFirstChildNodeWithName(propertyDetailNode, "Unitless") != null) {
			// if element is unit-less the unit is empty
			return unitAsString;
		} else {
			// if unit does not exist
			return unitAsString;
		}
	}

	private List<Node> searchForMatMlDocNodesIn(Node documentRootNode) {
		// search whole document for nodes named MatML_Doc and return them as a list
		// this is done with a breadth first search pattern
		List<Node> foundMatMlDocNodes = new ArrayList<>();
		List<Node> nodesToCheck = new ArrayList<>();
		nodesToCheck.add(documentRootNode);

		while (nodesToCheck.size() != 0) {
			Node currentNodeToCheck = nodesToCheck.remove(0);

			if (currentNodeToCheck.getNodeName().equals("MatML_Doc")) {
				// if the current node is named "MatML_Doc" a material is found
				// and there will be no material in the material so the children of
				// this node don't need to be added to the nodesToSarch list
				foundMatMlDocNodes.add(currentNodeToCheck);
			} else {
				// if the currentNodeToSearch is no material one of its children
				// could be a material so all of its children are added
				// to be searched for a material
				List<Node> currentNodeChildNodes = this.getOnlyChildNodes(currentNodeToCheck);
				nodesToCheck.addAll(currentNodeChildNodes);
			}
		}
		return foundMatMlDocNodes;
	}

	protected Map<String, Node> createIdToNodeMapFromMetaDataNode(Node metaDataNode) {
		// creates a map for the metadata part so every
		// id is mapped to its corresponding Node
		List<Node> metaDataChildNodes = this.getOnlyChildNodes(metaDataNode);
		Map<String, Node> parameterMap = new HashMap<>();
		for (Node childNode : metaDataChildNodes) {
			String id = childNode.getAttributes().getNamedItem("id").getTextContent();
			parameterMap.put(id, childNode);
		}
		return parameterMap;
	}

	protected void transformPropertyNodeToElementDataExchangeClass(Node propertyDataNode, Map<String, Node> metaDataIdToNodeMap,
			ArrayList<String> pathToGeneralPropertyCategory) {
		// create placeholder for the property element exchange class
		ElementDataExchangeClass transformedPropertyNode = null;

		// set path to propertyMaterial
		ArrayList<String> pathToMaterial = (ArrayList<String>) pathToGeneralPropertyCategory.clone();
		pathToMaterial.remove(pathToMaterial.size() - 1);
		
		// get id of the property and check if it is of UUID Type
		String propertyId = propertyDataNode.getAttributes().getNamedItem("property").getTextContent();

		// get property node from metadata
		Node propertyDetailsNodeFromMetadata = (Node) metaDataIdToNodeMap.get(propertyId);

		// check if there is a category for the material property given.
		// if so, create the category and add it to the path
		if (propertyDetailsNodeFromMetadata != null) {

			// get name of the category if it exists
			Node propertyCategoryNameNode = propertyDetailsNodeFromMetadata.getAttributes().getNamedItem("type");

			// create property category if the propertyCategory of the property exists
			if (propertyCategoryNameNode != null) {
				String propertyCategoryName = propertyCategoryNameNode.getTextContent();

				// create element for the property category
				ElementDataExchangeClass propertyCategory = new ElementDataExchangeClass("category", pathToMaterial);
				propertyCategory.addDataValue("name", propertyCategoryName);

				// add element to transformed elements list
				this.transformedElements.add(propertyCategory);

				// create path to this new property category
				ArrayList<String> pathPropertyCategory = (ArrayList<String>) pathToMaterial.clone();

				// add material name so the parent is set
				pathPropertyCategory.add(propertyCategoryName);

				// create ElementDataExchange Class object with the path to the property
				// category
				transformedPropertyNode = new ElementDataExchangeClass("property", pathPropertyCategory);

			} else {
				// create ElementDataExchange Class object with the path to the property
				// category if no property category exists
				transformedPropertyNode = new ElementDataExchangeClass("property", pathToGeneralPropertyCategory);
			}
			
			// add the property id to the propertyNode when it is a uuid
			try {
				UUID propertyUuid = UUID.fromString(propertyId);
				transformedPropertyNode.addDataValue("id", propertyUuid);
			} catch (IllegalArgumentException ex) {
				// catch case that Id is no uuid and simply pass
			}

			// get the name of the property and set it
			String propertyName = this.getFirstChildNodeWithName(propertyDetailsNodeFromMetadata, "Name")
					.getTextContent().strip();
			transformedPropertyNode.addDataValue("name", propertyName);

			// get and set note for the material property
			Node materialPropertyNoteNode = this.getFirstChildNodeWithName(propertyDetailsNodeFromMetadata, "Notes");
			if (materialPropertyNoteNode != null) {
				transformedPropertyNode.addDataValue("note", materialPropertyNoteNode.getTextContent());
			}

			// get and set unit of the property
			String propertyUnit = this.getUnitOfPopertyNode(propertyDetailsNodeFromMetadata);
			transformedPropertyNode.addDataValue("unit", propertyUnit);

		} else {
			// create ElementDataExchange Class object with the path to the property
			// category
			transformedPropertyNode = new ElementDataExchangeClass("property", pathToGeneralPropertyCategory);
			transformedPropertyNode.addDataValue("name", "No Name found");
		}

		// get value of the property and decide between lut and constant value
		if (this.getFirstChildNodeWithName(propertyDataNode, "ParameterValue") != null) {
			// case that there is a lut in the property
			// get data and parameter value nodes and create the hashmap for the lut out of
			// this
			Node dataNode = this.getFirstChildNodeWithName(propertyDataNode, "Data");
			Node parameterValueNode = this.getFirstChildNodeWithName(propertyDataNode, "ParameterValue");

			// get the delimiter if its set and split the elements accordingly
			Node delimiterAttributeNode = propertyDataNode.getAttributes().getNamedItem("delimiter");
			String delimiterString;
			if (delimiterAttributeNode != null) {
				delimiterString = delimiterAttributeNode.getTextContent();
			} else {
				delimiterString = ",";
			}

			String[] dataNodeStringValues = dataNode.getTextContent().split(delimiterString);
			String[] parameterValueStringValues = parameterValueNode.getTextContent().split(delimiterString);

			Map<Double, Double> lutMap = new HashMap<>();
			for (int valueIndex = 0; valueIndex < dataNodeStringValues.length; valueIndex++) {
				Double parameterValue = Double.parseDouble(parameterValueStringValues[valueIndex]);
				Double dataValue = Double.parseDouble(dataNodeStringValues[valueIndex]);
				lutMap.put(parameterValue, dataValue);
			}

			// get the unit of the parameter value
			String parameterValueNodeId = parameterValueNode.getAttributes().getNamedItem("parameter").getTextContent();
			Node parameterValueMetadataNode = (Node) metaDataIdToNodeMap.get(parameterValueNodeId);

			String lookUpTableUnit = this.getUnitOfPopertyNode(parameterValueMetadataNode);
			transformedPropertyNode.addDataValue("lookUpTableUnit", lookUpTableUnit);

			// add the look up table/map as a data value to the transformed propertyNode (eg
			// the
			// corresponding ElementData exchange class element)
			transformedPropertyNode.addDataValue("value", lutMap);

		} else {
			// case that there is no lut in the Property
			Node propertyValueNode = this.getFirstChildNodeWithName(propertyDataNode, "Data");
			double propertyValue = Double.parseDouble(propertyValueNode.getTextContent());
			transformedPropertyNode.addDataValue("value", propertyValue);
		}
		this.transformedElements.add(transformedPropertyNode);
	}

	protected String getMaterialNotes(Node bulkDetailsNode) {
		Node materialNotesNode = this.getFirstChildNodeWithName(bulkDetailsNode, "Notes");
		if (materialNotesNode != null) {
			return materialNotesNode.getTextContent();
		}
		return null;
	}

	// part with functions to transform elementDataEschange Class elements to the
	// xml file for saving data
	@Override
	public void elementListToFile(Path dataFilePath, List<ElementDataExchangeClass> elementsFromModel)
			throws Exception {
		// create new document builder to create the file
		Document resultDocument = this.getNewDocument();

		// get the first node containing the information about the data file
		// extract the note
		ElementDataExchangeClass dataFileElement = elementsFromModel.remove(0);

		// get the new Datafile name and file ending
		String newDataFileName = dataFileElement.getDataValue("name").toString().strip();
		String newDataFileEnding = newDataFileName.substring(newDataFileName.length() - 4, newDataFileName.length());
		String oldDataFileEnding = ".xml";

		// add correct file Ending if the newDataFileName does not have it
		// this is needed since later the current file in dataFilePath will be renamed
		// to
		// newDataFileName an therefore the correct fileEnding has to be ensured
		if (!newDataFileEnding.equals(oldDataFileEnding)) {
			newDataFileName = newDataFileName + oldDataFileEnding;
		}

		// define structure of the matml Document
		Element xmlRootElement = resultDocument.createElement("MatML_Doc");
		resultDocument.appendChild(xmlRootElement);
		
		// create the note comment at the beginning which holds the note
		// made for the datafile and add it to the xmlRootElement
		String note = (String) dataFileElement.getDataValue("note");
		if(note != null) {
			Node dataFileNoteNode = resultDocument.createComment(note);
			xmlRootElement.appendChild(dataFileNoteNode);
		}
		// create metadata node but it will not get added to the document
		// because its defined to be at the end of the document
		Node metadataNode = resultDocument.createElement("Metadata");

		// iterate over the elements in the
		for (ElementDataExchangeClass currentModelElement : elementsFromModel) {
			String currentModelElementType = currentModelElement.getType();

			// transform the elements due to their type to .xml nodes
			if (currentModelElementType.equals("folder")) {
				// since this file format does not have separate folder
				// it is checked if the folder has no children
				// which means it will get deleted during saving
				String folderName = (String) currentModelElement.getDataValue("name");
				Object folderIdObject = currentModelElement.getDataValue("id");
				if(folderIdObject != null) {
					UUID folderId = (UUID) folderIdObject;
					if(!this.hasElementChildInModel(folderId)) {
						// ask the user if saving should be continued
						int decisionResult = this.showDecisionWindow("The folder '"+folderName+"' in "+
								currentModelElement.getPathToParentElement()+" has no children"+
								" in the material database.\nEmpty folders can not be saved in the "
								+ "MatMl File structure, therefore it will be deleted.\n\nContinue?", new String[] {"Yes","No"});
						if(decisionResult == 1) {
							throw new Exception("Stop saving because of folder deletion");
						}
					}
				}
				
				continue;
			} else if (currentModelElementType.equals("category")) {
				// the category is set in the property itselve therefore
				// the category element can be skipped
				continue;
			} else if (currentModelElementType.equals("material")) {
				Node materialNode = this.transformMaterialToMaterialNode(currentModelElement, resultDocument);
				xmlRootElement.appendChild(materialNode);
			} else if (currentModelElementType.equals("property")) {
				Node propertyDataNode = this.transformPropertyToPropertyDataNode(currentModelElement, resultDocument,
						metadataNode);
				this.addPropertyNodeToCorrespondingMaterialNode(propertyDataNode,
						currentModelElement.getPathToParentElement(), xmlRootElement);
			}
		}
		// add the metadata node as last child node of the root element
		xmlRootElement.appendChild(metadataNode);

		// validate the created document against the given .xsd file
		try {
			this.validateSubtreeNodeAgainstXsd(xmlRootElement);
		} catch (Exception ex) {
			// catch error and ask if file should be still saved
			int decision = this.showDecisionWindow("An error occured during saving due to an invalid"
					+ "matml Format during check'"
			+dataFilePath.toString()+"\n\nMessage:\n"+ex.getMessage()+
			"'\n\nContinue?)", new String[] {"Yes","No"});
			
			// abort saving when selected
			if(decision == 1) {
				throw new Exception("Saving File '"+dataFilePath.toString()+"' aborted.");
			}
		}

		// DEBUG printout of the finished file
//		try {
//			this.printDocument(resultDocument);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		this.saveXmlFile(resultDocument, dataFilePath);
	}

	private Node transformMaterialToMaterialNode(ElementDataExchangeClass materialElementDataExchangeElement,
			Document resultDocument) {
		// create material and bulk details node due to the .xsd specification of the
		// matml XML
		Node materialNode = resultDocument.createElement("Material");
		Node materialBulkDetailsNode = resultDocument.createElement("BulkDetails");
		materialNode.appendChild(materialBulkDetailsNode);

		// get key iterator for the data values and iterate over them for processing
		Map<String, Object> elementDataMap = materialElementDataExchangeElement.getAllDataValues();
		Iterator<Entry<String, Object>> elementDataIterator = elementDataMap.entrySet().iterator();

		// create node hash map to store the nodes and later set them in the correct
		// order
		Map<String, Node> dataNodeMap = new HashMap<>();

		while (elementDataIterator.hasNext()) {
			// get the corresponding entry Set
			Entry<String, Object> currentDataValue = elementDataIterator.next();

			// split entry set in value and object for further processing
			String valueKey = currentDataValue.getKey();
			Object valueObject = currentDataValue.getValue();

			// process the data Values due to their keys (names)
			if (valueKey.equals("name")) {
				// create a material name node and append it to the
				// bulk details node
				String materialName = valueObject.toString();
				String nodeName = "Name";
				Node nameNode = resultDocument.createElement(nodeName);
				nameNode.setTextContent(materialName);
				// add to data node map for later adding node to bulk
				// details node
				dataNodeMap.put(nodeName, nameNode);
				continue;
			} else if (valueKey.equals("note")) {
				// create Notes element and append it to the
				// datanode map if notes exist
				String notes = valueObject.toString().strip();
				if (notes.length() != 0) {
					String nodeName = "Notes";
					Node notesNode = resultDocument.createElement(nodeName);
					notesNode.setTextContent(notes);
					dataNodeMap.put(nodeName, notesNode);
				}
				continue;
			} else if (valueKey.equals("id")) {
				// get the UUID and set it in the material as attribute so
				// the material is traceable when the matml file is loaded again
				// since this is an attribute it is not needed to add this to the
				String attributeName = "id";
				String id = valueObject.toString();
				Element materialNodeElement = (Element) materialNode;
				materialNodeElement.setAttribute(attributeName, id);
				continue;
			}
		}

		// add the path to the material e.g. add class and subclass
		List<String> materialPathToParent = materialElementDataExchangeElement.getPathToParentElement();

		// remove first entry which is the dataFile
		materialPathToParent.remove(0);

		// preinitialize the the materialclass and subclass and fill
		// them with the corresponding class and subclass names

		if (materialPathToParent.size() > 0) {
			// create the class node and a name node as child of the class node
			// and add the name as text to the name node of the class node
			String materialClassName = materialPathToParent.remove(0);
			String nodeName = "Class";
			Node classNode = resultDocument.createElement(nodeName);
			Node classNameNode = resultDocument.createElement("Name");
			classNameNode.setTextContent(materialClassName);
			classNode.appendChild(classNameNode);
			dataNodeMap.put("Class", classNode);
		}

		if (materialPathToParent.size() > 0) {
			// create the subclass node and a name node as child of the subclass node
			// and add the name as text to the name node of the subclass node
			String materialSubclassName = materialPathToParent.remove(0);
			String nodeName = "Subclass";
			Node SubclassNode = resultDocument.createElement(nodeName);
			Node SubclassNameNode = resultDocument.createElement("Name");
			SubclassNameNode.setTextContent(materialSubclassName);
			SubclassNode.appendChild(SubclassNameNode);
			dataNodeMap.put(nodeName, SubclassNode);
		}

		// take elements from dataNodeMap and add them to the bulk details node in
		// the correct order (given in the xsd of matml)
		for (String nodeName : new String[] { "Name", "Class", "Subclass", "Notes" }) {
			Node dataNode = dataNodeMap.get(nodeName);
			if (dataNode != null) {
				materialBulkDetailsNode.appendChild(dataNode);
			}
		}
		return materialNode;
	}

	private Node transformPropertyToPropertyDataNode(ElementDataExchangeClass propertyElementDataExchange,
			Document resultDocument, Node metadataNode) throws InstanceNotFoundException {
		// this takes the property elemend and returns the corresponsign property data node
		// with the values etc added. In addition it creates an metadata node for the property
		// data node

		// get the path to the parent element
		List<String> pathToPropertyParent = propertyElementDataExchange.getPathToParentElement();

		// create the property node
		Element propertyDataElement = resultDocument.createElement("PropertyData");

		// the property needs an id so it gets linked to the metadata propertyDetaisl
		// node
		// it is NOT checked if the property already exists. This causes a creation of a
		// propertyDetails
		// node for every element. This ensures that the notes for every property can
		// be saved individually and the note for one property is not shared with the
		// one for
		// a property with the same name and unit
		String propertyId = propertyElementDataExchange.getDataValue("id").toString();

		// add id-attribute corresponding to the one of the PropertyDetails
		// node in the metadata section of the document
		propertyDataElement.setAttribute("property", propertyId);

		// get the name of the property
		String propertyDataName = propertyElementDataExchange.getDataValue("name").toString();

		// type corresponds to the property category in the material database
		String propertyCategory = pathToPropertyParent
				.get(propertyElementDataExchange.getPathToParentElementSize() - 1);

		// get the unit of the property
		Object propertyUnit = propertyElementDataExchange.getDataValue("unit");

		// get the notes for the property
		Object propertyNotesObject = propertyElementDataExchange.getDataValue("note");

		// create the property details node for the property and add it to the metadata
		// node
		Element propertyDetialsNode = this.createPropertyDetailsNode(propertyId, propertyUnit, propertyDataName,
				propertyNotesObject, propertyCategory, resultDocument);
		metadataNode.appendChild(propertyDetialsNode);

		// get the property value
		Object propertyDataValue = propertyElementDataExchange.getDataValue("value");

		// create Data node for propertyData to contain the dataValue
		Element propertyDataDataNode = resultDocument.createElement("Data");

		// add format node to the propertyDataDataNode
		propertyDataDataNode.setAttribute("format", "float");

		// check if the value is a look up table or constant
		if (propertyDataValue instanceof HashMap) {
			// get values and sort the pairs by putting them into a tree map
			TreeMap<Double, Double> propertyDataValueMap = new TreeMap<>((Map) propertyDataValue);

			// raise error if there are no values in the look up table
			// this should avoid saving empty values and causing problems when reopening
			// the datafile with the Material database
			if (propertyDataValueMap.size() == 0) {
				throw new InstanceNotFoundException("Look up Table of material Property '" + propertyDataName + "' in "
						+ pathToPropertyParent + " is empty.");
			}
			// set delemiter for values of the look up table
			String delemiter = ",";

			// add delimiter to the property data node as attribute
			propertyDataElement.setAttribute("delimiter", delemiter);

			// get the entry set of the propertyDatavalue Map (the look up table)
			Collection<Entry<Double, Double>> propertyValueCollection = propertyDataValueMap.entrySet();

			// look up table values (x and y) get transformed to strings to save them
			String valueString = "";
			String keyString = "";
			for (Entry<Double, Double> lookUpTableEntry : propertyValueCollection) {
				valueString = valueString + delemiter + lookUpTableEntry.getValue().toString();
				keyString = keyString + delemiter + lookUpTableEntry.getKey().toString();
			}
			// cut off first delimiter which is due to adding
			// the first value to an empty substring
			valueString = valueString.substring(1, valueString.length());
			keyString = keyString.substring(1, keyString.length());

			// add the values to the property data data node
			propertyDataDataNode.setTextContent(valueString);

			// create parameter value node for the key of the map (e.g. the x values of the
			// look up table)
			Element parameterValueNode = resultDocument.createElement("ParameterValue");

			// the parameter value needs an id (called parameter ) to correspond to
			// the metadata node. therefore an UUID is chosen like for the other
			// elements (this UUID does have no meaning in the material database)
			// since is is not used outside the xml document
			String parameterValueId = this.createUuidWithLetterAtEnd().toString();

			// add id to the parameter value node
			parameterValueNode.setAttribute("parameter", parameterValueId);

			// add format attribute which is always float
			parameterValueNode.setAttribute("format", "float");

			// create the datanode and add it to the parameter value node
			Node parameterValueDataNode = resultDocument.createElement("Data");

			// set the keys /x values of the map/look up table as the data
			parameterValueDataNode.setTextContent(keyString);

			// add the data node to the parameter value node
			// so the value contains its data
			parameterValueNode.appendChild(parameterValueDataNode);

			// add the parameter value node to the property data node
			// so the look up tabel y-axis/values is corellated with its x-axis/keys
			propertyDataElement.appendChild(parameterValueNode);

			// get the unit for the look up table x axis /the map keys
			Object parameterValueUnit = propertyElementDataExchange.getDataValue("lookUpTableUnit");
			if (parameterValueUnit == null) {
				parameterValueUnit = "";
			}

			// create the corresponding propertyDetails node for the parameter value (the
			// x-axis of the look
			// up table) there are no notes and type for this so this values are empty in
			// the function
			// in addition the name is never used so it is just filles with the id again as
			// placeholder
			Element parameterValuePropertyDetailsNode = this.createPropertyDetailsNode(parameterValueId,
					parameterValueUnit, parameterValueId, "", "", resultDocument);

			// the parmeter details node gets stored in the metadata section of the document
			metadataNode.appendChild(parameterValuePropertyDetailsNode);

		} else if (propertyDataValue instanceof Double) {
			Double value = (double) propertyDataValue;
			propertyDataDataNode.setTextContent(value.toString());
		}

		// add the propertyDataElement (which is also of type node) at the beginning of
		// the child nodes
		Node firstChildNode = propertyDataElement.getFirstChild();
		if (firstChildNode != null) {
			propertyDataElement.insertBefore(propertyDataDataNode, firstChildNode);
		} else {

			// add the propertyDataDataNode to the propertyDataNode
			propertyDataElement.appendChild(propertyDataDataNode);
		}

		return propertyDataElement;
	}

	private Element createPropertyDetailsNode(String id, Object unitObject, String name, Object notesObject,
			String proprtyCategory, Document resultDocument) {
		// create the corresponding property details node
		Element propertyDetailsNode = resultDocument.createElement("PropertyDetails");

		// check if the proprtyCategory is not null or "" and set it
		if (proprtyCategory != null && !proprtyCategory.toString().equals("")) {
			propertyDetailsNode.setAttribute("type", proprtyCategory);
		}

		// set id attribute which is unique in the xml document
		propertyDetailsNode.setAttribute("id", id);

		// create the name for the property
		Node correspondingMetadataPropertyNameNode = resultDocument.createElement("Name");
		correspondingMetadataPropertyNameNode.setTextContent(name);

		// add name to the propertyDetails node
		propertyDetailsNode.appendChild(correspondingMetadataPropertyNameNode);

		// set the unit of the property into the propertyDetails node
		Element propertyUnitNode = this.convertUnitToUnitNode(unitObject, resultDocument);
		propertyDetailsNode.appendChild(propertyUnitNode);

		// only add property notes of there is a notes String and the string is not
		// empty
		if (notesObject != null && !notesObject.toString().strip().equals("")) {
			String propertyNotes = notesObject.toString();
			Node propertyNotesNode = resultDocument.createElement("Notes");
			propertyNotesNode.setTextContent(propertyNotes);
			propertyDetailsNode.appendChild(propertyNotesNode);
		}
		return propertyDetailsNode;
	}

	private void addPropertyNodeToCorrespondingMaterialNode(Node propertyNode, List<String> propertyNodeParentPath,
			Element xmlRootNode) throws InstanceNotFoundException {
		// get the material name out of the propertyNodeParentPath
		String materialName = propertyNodeParentPath.get(propertyNodeParentPath.size() - 2);

		// get all materials form the xmlRootNode
		NodeList materials = xmlRootNode.getElementsByTagName("Material");

		for (int materialIndex = 0; materialIndex < materials.getLength(); materialIndex++) {
			// get name of the current material
			Node currentMaterial = materials.item(materialIndex);
			Element bulkDetailsNode = (Element) currentMaterial.getFirstChild();
			String currentMaterialName = bulkDetailsNode.getElementsByTagName("Name").item(0).getTextContent();
			
			// check if the name is the same as the material name the property is belonging
			// to
			if (materialName.equals(currentMaterialName)) {
				// the element "Notes" is searched, and the propertyNode is
				// inserted before, as children of the bulk details node of the material
				// since this is convention by the .xsd file
				NodeList materialDetails = bulkDetailsNode.getChildNodes();

				for (int elementIndex = 0; elementIndex < materialDetails.getLength(); elementIndex++) {
					Node currentMaterialDetailNode = materialDetails.item(elementIndex);

					if (currentMaterialDetailNode.getNodeName().equals("Notes")) {
						bulkDetailsNode.insertBefore(propertyNode, currentMaterialDetailNode);
						return;
					}
				}
				// if there is no Notes-node simply add the property at the end of the bulk details
				bulkDetailsNode.appendChild(propertyNode);
				return;
			}
		}
		// get id of the node if adding the property to the node did not wirk and throw exception
		String propertyNodeId = propertyNode.getAttributes().getNamedItem("propertyid").getTextContent();
		throw new InstanceNotFoundException("Could not add Property with id '" + propertyNodeId + "' to material '"
				+ materialName+"'");

	}

	private Element convertUnitToUnitNode(Object propertyUnit, Document resultDocument) {
		// unit should consist of the following sings [1-9*.^-()]
		// actual support is for dataformat SI_unit^(factor)*SI_unit^(factor)...
		// factor can be positive or negative real number
		// TODO: this could be done better but is shortened because of time
		// TODO: better recognition of unit ot of the given scheme
		Element unitNode = null;
		if (propertyUnit != null) {
			if (propertyUnit.toString().strip().equals("")) {
				// catch case that property is an empty string and therefore unitless
				unitNode = resultDocument.createElement("Unitless");
			} else {
				// create the unit node
				unitNode = resultDocument.createElement("Units");

				// convert unit string to units
				String propertyUnitString = propertyUnit.toString();
				String[] unitStringParts = propertyUnitString.split("\\*");

				// trannsform unit parts to unit nodes
				for (String unitPart : unitStringParts) {
					// create the unit node for the part
					Element unitPartNode = resultDocument.createElement("Unit");

					// create the name note for the unitPart
					Element unitPartNameNode = resultDocument.createElement("Name");

					// get index of power-of sign "^" to check if the Unit has a power of
					int powerSignIndex = unitPart.indexOf("^");
					// get the factor of the unit
					if (powerSignIndex != -1) {
						// get the factor
						int factorBeginIndex = unitPart.indexOf("(");

						// factor has to be between brackets so they get eliminated
						unitPartNode.setAttribute("power",
								unitPart.substring(factorBeginIndex + 1, unitPart.length() - 1));

						// get and set the unit(name)
						String unitName = unitPart.substring(0, powerSignIndex);
						unitPartNameNode.setTextContent(unitName);
					} else {
						unitPartNameNode.setTextContent(unitPart);
					}

					// add the name to the unit part
					unitPartNode.appendChild(unitPartNameNode);

					// ad UnitPart to the unit node
					unitNode.appendChild(unitPartNode);
				}
			}
		} else {
			// if the unit is not there, the element is unitless
			unitNode = resultDocument.createElement("Unitless");
		}
		return unitNode;
	}

	private UUID createUuidWithLetterAtEnd() {
		// set an id with the condition that the first char has to be a letter
		// this is needed s othe matml data file handler
		// can set the id wich is defined as starting with a letter
		// in the mat ml xsd
		String materialUuidString = UUID.randomUUID().toString();

		// create random char form a-f (since uuid is hexadecimal)
		// and replace first UUID char
		Random rnd = new Random();
		char c = (char) ('a' + rnd.nextInt(6));
		materialUuidString = (String) materialUuidString.subSequence(1, materialUuidString.length() - 1);
		materialUuidString = c + materialUuidString;

		// transform the UUID from String back to UUID class object
		UUID materialUuid = UUID.fromString(materialUuidString);
		return materialUuid;
	}

	// debug functions
	private void printDocument(Document doc) throws TransformerFactoryConfigurationError, TransformerException {
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		// initialize StreamResult with File object to save to file
		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);
		String xmlString = result.getWriter().toString();
		System.out.println(xmlString);
	}
}
