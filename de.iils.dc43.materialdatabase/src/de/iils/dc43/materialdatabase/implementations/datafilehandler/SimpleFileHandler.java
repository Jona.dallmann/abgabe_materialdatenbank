package de.iils.dc43.materialdatabase.implementations.datafilehandler;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractDataFileHandler;
import de.iils.dc43.materialdatabase.implementations.simplemodel.ElementDataExchangeClass;

import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;

public class SimpleFileHandler extends AbstractDataFileHandler {
	@Override
	protected List<ElementDataExchangeClass> fileToElementList(Path dataFilePath) {
		List<ElementDataExchangeClass> returnList = new ArrayList<>();
		try {
			// open xml File
			File inputFile = dataFilePath.toFile();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			// get root element
			Node dataFileElementXml = doc.getDocumentElement();

			// create list which contains unprocessed nodes, iterate through the list,
			// get child elements and process them until the list is empty
			// the starting point for the list is the dataFileElementXml
			List<Node> nodesToProcess = new ArrayList<>();
			nodesToProcess.add(dataFileElementXml);

			while (nodesToProcess.size() != 0) {
				// get node out of the list and get its child nodes
				Node currentNode = nodesToProcess.remove(0);
				NodeList childNodes = currentNode.getChildNodes();

				for (int counter = 0; counter < childNodes.getLength(); counter++) {
					// select only child elements which are node and not text or other things
					if (childNodes.item(counter).getNodeType() == Node.ELEMENT_NODE) {
						nodesToProcess.add(childNodes.item(counter));
					}
				}
				
				// process the node to a ExchangeElementClass element and add it to the return list
				returnList.add(this.createExchangeElementFromXmlNode(currentNode));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnList;
	}

	@Override
	public void elementListToFile(Path dataFilePath, List<ElementDataExchangeClass> elements) {
		try {
			// create document Builder instance to create XML file
			File inputFile = dataFilePath.toFile();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			
			// get the xmlnodes from the DataExchange element
			// and add them to the xml document
			ElementDataExchangeClass dataFileRoot = elements.remove(0);
			this.createXmlNodeFromExchangeElement(dataFileRoot, doc);
			for(ElementDataExchangeClass element:elements) {
				this.createXmlNodeFromExchangeElement(element, doc);
			}
			
			// use transform engine to create XML file for the dataFile
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(inputFile);
            transformer.transform(domSource, streamResult);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void createXmlNodeFromExchangeElement(ElementDataExchangeClass element,
												  Document doc) {
		Element xmlElement = doc.createElement(element.getType());
		Map<String,Object> dataValueMap = element.getAllDataValues();
		for(Map.Entry<String, Object> dataValue: dataValueMap.entrySet()) {
			xmlElement.setAttribute(dataValue.getKey(), dataValue.getValue().toString());
		}
		
		// get the parent element according to path
		Node currentElement = doc.getFirstChild();
		if(currentElement == null) {
			doc.appendChild(xmlElement);
			return;
		}else if(currentElement.getAttributes().getNamedItem("name").getNodeValue().equals(element.getPathToParentElement().get(0))
				 && element.getPathToParentElement().size() == 0) {
			currentElement.appendChild(xmlElement);
			return;
		}
		
		for(String pathPart:element.getPathToParentElement()) {
			NodeList childElements = currentElement.getChildNodes();
			for(int counter = 0; counter<childElements.getLength();counter++) {
				if(childElements.item(counter).getAttributes().getNamedItem("name").getNodeValue().equals(pathPart)) {
					currentElement = childElements.item(counter);
				}
			}
		}
		if(currentElement.getAttributes().getNamedItem("name").getNodeValue().equals(element.getPathToParentElement().get(element.getPathToParentElementSize()-1))) {
			currentElement.appendChild(xmlElement);
		}
	}

	private ElementDataExchangeClass createExchangeElementFromXmlNode(Node node) {
		ElementDataExchangeClass element = new ElementDataExchangeClass(node.getNodeName(),
				this.createPathForNode(node));
		NamedNodeMap attributes = node.getAttributes();
		for (int counter = 0; counter < attributes.getLength(); counter++) {
			if(attributes.item(counter).getNodeName() == "value") {
				// decide between look up table and simple value by checking if the
				// string can be converted to a double
				String nodeValue = attributes.item(counter).getNodeValue();
				try {
					double value = Double.valueOf(nodeValue);
					element.addDataValue(attributes.item(counter).getNodeName(), value);
					continue;
				}catch(NumberFormatException ex) {
					// make lut (look up table) to hash map
					String[] lutValues = nodeValue.split(", ");
					Map<Double,Double> propertyLookUpTable = new HashMap<>();
					for(String valuePair:lutValues) {
						// delete characters at start and beginning
						valuePair = valuePair.replace("{", "");
						valuePair = valuePair.replace("}", "");
						// separate the numbers and put them into the PropertyLookUpTable
						String[] seperatedValuePair = valuePair.split("=");
						propertyLookUpTable.put(Double.valueOf(seperatedValuePair[0]), Double.valueOf(seperatedValuePair[1]));
					}
					// if the property look up table has been added next attribute can be processed
					element.addDataValue(attributes.item(counter).getNodeName(), propertyLookUpTable);
					continue;
				}
			}
			element.addDataValue(attributes.item(counter).getNodeName(), attributes.item(counter).getNodeValue());
		}
		return element;
	}

	private List<String> createPathForNode(Node node) {
		// this function creates the needed path list for the node so it can be
		// used with the createExchangeElementFromXmlNode method
		List<String> path = new ArrayList<>();
		while (!node.getParentNode().equals(node.getOwnerDocument())) {
			path.add(0, node.getParentNode().getAttributes().getNamedItem("name").getNodeValue());
			node = node.getParentNode();
		}
		return path;
	}
}
