package de.iils.dc43.materialdatabase.implementations.simpleview;

import java.util.UUID;

import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

public class MaterialTabItem extends TabItem{
	private UUID id;

	public MaterialTabItem(TabFolder parent, int style, UUID id) {
		super(parent, style);
		this.id = id;
	}
	
	public MaterialTabItem(TabFolder parent, int style, int style2, UUID id) {
		super(parent, style, style2);
		this.id = id;
	}

	public UUID getId() {
		return id;
	}
	
	@Override
	protected void checkSubclass() {
	    //  this overrides the check subclass method so subclassing of treeTtem element of java.swt is allowed
	}

}
