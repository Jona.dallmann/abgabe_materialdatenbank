package de.iils.dc43.materialdatabase.implementations.simpleview;

import java.util.ArrayList;
import java.util.UUID;

import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import de.iils.dc43.materialdatabase.implementations.modelelements.Material;

// this class extends the TreeItem with a UUID so this UUID can be used to detect the element in the
// view

public class MaterialTreeItem extends TreeItem{
	private final UUID id;

	public MaterialTreeItem(Tree parent, int style, UUID elementId, int index) {
		super(parent, style, index);
		this.id = elementId;
	}
	
	public MaterialTreeItem(TreeItem parent, int style, UUID elementId, int index) {
		super(parent, style, index);
		this.id = elementId;
	}
	
	public MaterialTreeItem(TreeItem parent, int style, UUID elementId) {
		super(parent, style);
		this.id = elementId;
	}
	
	public UUID getId() {
		return id;
	}
	
	public MaterialTreeItem[] getAllChildren() {
		// gets all the children and subchildren etc. of the current MaterialTreeItem and returns them
		// as array list
		ArrayList<MaterialTreeItem> finishedItemList = new ArrayList<>();
		ArrayList<MaterialTreeItem> searchItemList = new ArrayList<>();
		for(TreeItem item : this.getItems()) {
			searchItemList.add((MaterialTreeItem) item);
		}
		while(searchItemList.size() != 0) {
			MaterialTreeItem currentItem = searchItemList.remove(0);
			if(currentItem.getItemCount() != 0) {
				for(TreeItem item : currentItem.getItems()) {
					searchItemList.add((MaterialTreeItem)item);
				}
			}
			finishedItemList.add(currentItem);
		}
		return finishedItemList.toArray(new MaterialTreeItem[0]);
	}
	
	@Override
	protected void checkSubclass() {
	    //  this overrides the check subclass method so subclassing of treeTtem element of java.swt is allowed
	}

}
