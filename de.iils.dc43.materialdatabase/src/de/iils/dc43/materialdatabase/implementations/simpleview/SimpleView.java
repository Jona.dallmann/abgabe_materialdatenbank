package de.iils.dc43.materialdatabase.implementations.simpleview;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractController;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractGraphElement;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractModel;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractView;

public class SimpleView extends AbstractView implements SelectionListener, Listener {
	protected Shell shlEditor;
	private Text txtSuchen; // this is unused since the corresponding functionality is not implemented
	private Text nameField;
	private Text databaseField;
	private Text noteField;
	private Display display = new Display();
	private Device device;
	private Composite propertiesArea;
	private Tree materialTree;
	private ToolItem taskDropDown;
	private Menu treeSubMenu;
	private TabFolder materialPropertyCategoryTabFolder;
	private Menu propertySubMenu;
	private Menu categorySubMenu;
	private TableEditor propertyTableEditor;
	private Menu taskMenu;
	private UUID selectedMaterialID = null;
	private UUID selectedMaterialTreeElmentId = null;

	/**
	 * Open the window.
	 */
	public void open() {
		this.display.syncExec(new Runnable() {

			@Override
			public void run() {
				createContents();
				shlEditor.open();
				shlEditor.layout();
				// initialize controller here in main loop so changes in initialization of the
				// model directly affect the view
				controller.initialize();
				while (!shlEditor.isDisposed()) {
					if (!display.readAndDispatch()) {
						display.sleep();
					}
				}
			}
		});

	}

	/**
	 * Create contents of the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	protected void createContents() {
		shlEditor = new Shell(SWT.MIN | SWT.MAX | SWT.TITLE | SWT.RESIZE);
		shlEditor.setBackground(new Color(this.device, new RGB(173, 216, 230)));
		shlEditor.setSize(900, 500);
		shlEditor.setText("Material Database Editor");
		shlEditor.setLayout(new FillLayout(SWT.HORIZONTAL));
		// handle event on closing the shell
		shlEditor.addListener(SWT.Close, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				Shell shell = shlEditor;
				MessageDialog dialog = new MessageDialog(shell, "Warning", null,
						"Changes have not been saved. Should the changes be saved?", MessageDialog.INFORMATION,
						new String[] { "Yes", "No", "Cancel" }, 0);
				int result = dialog.open();
				if (result == 2) {
					// simply leave dialogue if 
					arg0.doit = false;
					dialog.close();
					return;
				} else if (result == 0) {
					dialog.close();
					try {
						controller.saveModel();
					} catch (Exception ex) {
						// stop saving when Exception occured
						controller.raiseError("Saving Model aborted due to '" + ex.getMessage() + "'");
						return;
					}
				}
			}	
		});

		// get device for creating and setting colors
		this.device = this.display;

		SashForm sashForm = new SashForm(shlEditor, SWT.SMOOTH);
		sashForm.setSashWidth(2);

		Composite treeSide = new Composite(sashForm, SWT.NO_RADIO_GROUP);
		treeSide.setBackground(new Color(this.device, new RGB(240, 240, 240)));
		treeSide.setForeground(new Color(this.device, new RGB(220, 220, 220)));
		GridLayout gl_treeSide = new GridLayout(1, false);
		gl_treeSide.marginBottom = 5;
		gl_treeSide.marginLeft = 3;
		gl_treeSide.verticalSpacing = 0;
		gl_treeSide.marginWidth = 0;
		gl_treeSide.marginHeight = 0;
		treeSide.setLayout(gl_treeSide);

		// This is commented out search filed because it could not be used currently
		// TODO:treeside search field has to be worked on
//		txtSuchen = new Text(treeSide, SWT.NONE);
////		txtSuchen.setFont(new Font(device, new FontData("Segoe UI", 10, SWT.NORMAL)));
//		GridData gd_txtSuchen = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
//		gd_txtSuchen.heightHint = 23;
//		txtSuchen.setLayoutData(gd_txtSuchen);
//		txtSuchen.addFocusListener(new FocusAdapter() {
//			@Override
//			public void focusLost(FocusEvent e) {
//				txtSuchen.setText("Search");
//				txtSuchen.setForeground(new Color(device,new RGB(180, 180, 180)));
//			}
//			
//			@Override
//			public void focusGained(FocusEvent e) {
//				txtSuchen.setText("");
//			}
//		});
//		txtSuchen.addKeyListener(new KeyAdapter() {
//			@Override
//			public void keyPressed(KeyEvent e) {
//				String text = txtSuchen.getText() + e.character;
//				System.out.println(text);
//				if (96 < e.keyCode & e.keyCode < 123) {
//					//FIXME: searching for materials/folder goes in here
//					Composite treeSide = txtSuchen.getParent();
//					Control[] test = treeSide.getChildren();
//					for (int i = 0; i < 2; i++) {
//						if (test[i].getClass().getName() == "org.eclipse.swt.widgets.Tree") {
//							Tree tree = (Tree) test[i];
//							TreeItem[] treeChildren = tree.getItems();
//						}
//					}
//
//				} else if (e.keyCode == 8 & text == "") {
//					txtSuchen.setText("Search");
//				}
//			}
//		});
//		txtSuchen.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseDown(MouseEvent e) {
//				if (txtSuchen.getText().equals("Search")) {
//					txtSuchen.setText("");
//					txtSuchen.setForeground(new Color(device,new RGB(0, 0, 0)));
//				}
//			}
//		});
//		txtSuchen.setForeground(new Color(this.device,new RGB(180, 180, 180)));
//		txtSuchen.setBackground(new Color(this.device,new RGB(222, 222, 222)));
//		txtSuchen.setText("Search");

		this.materialTree = new Tree(treeSide, SWT.NONE);
		this.materialTree.addSelectionListener(this);
		this.materialTree.addListener(SWT.MouseDown, this);

		this.materialTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		this.materialTree.setBackground(new Color(this.device, new RGB(230, 230, 230)));

		// create right click menu for the material tree
		treeSubMenu = new Menu(materialTree);
		MenuItem newFolderItem = new MenuItem(treeSubMenu, SWT.POP_UP);
		newFolderItem.setText("new Folder");
		newFolderItem.addSelectionListener(this);
		MenuItem newMaterialItem = new MenuItem(treeSubMenu, SWT.POP_UP);
		newMaterialItem.setText("new Material");
		newMaterialItem.addSelectionListener(this);
		MenuItem removeItem = new MenuItem(treeSubMenu, SWT.POP_UP);
		removeItem.setText("remove");
		removeItem.addSelectionListener(this);

		Composite propertySide = new Composite(sashForm, SWT.NONE);
		propertySide.setBackground(new Color(this.device, new RGB(240, 240, 240)));
		GridLayout gl_propertySide = new GridLayout(1, false);
		gl_propertySide.horizontalSpacing = 0;
		gl_propertySide.marginBottom = 5;
		gl_propertySide.marginHeight = 0;
		gl_propertySide.marginWidth = 0;
		propertySide.setLayout(gl_propertySide);

		Composite propertiesArea = new Composite(propertySide, SWT.NONE);
		propertiesArea.setLayout(new FillLayout(SWT.VERTICAL));
		propertiesArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		propertiesArea.setVisible(false);
		this.propertiesArea = propertiesArea;

		Group grpGeneral = new Group(propertiesArea, SWT.NONE);
		grpGeneral.setText("General");
		grpGeneral.setLayout(new GridLayout(2, false));

		Label lblName = new Label(grpGeneral, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name");

		// set textfield to input the name of the element
		nameField = new Text(grpGeneral, SWT.NONE);
		GridData gd_nameField = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_nameField.widthHint = 172;
		nameField.setLayoutData(gd_nameField);
		nameField.setBackground(new Color(this.device, new RGB(222, 222, 222)));
		// when the field lost its focus set the name typed into it
		// ad the corresponding element in the model
		// this does not save the name persistent in a datafile
		nameField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				controller.setModelElementProperty(selectedMaterialTreeElmentId, "Name", nameField.getText());
			}
		});

		Label lblDatenbank = new Label(grpGeneral, SWT.NONE);
		lblDatenbank.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDatenbank.setText("Datafile");

		databaseField = new Text(grpGeneral, SWT.NONE);
		databaseField.setBackground(new Color(this.device, new RGB(222, 222, 222)));
		GridData gd_databaseField = new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1);
		gd_databaseField.widthHint = 172;
		databaseField.setLayoutData(gd_databaseField);
		databaseField.setEditable(false);

		Label lblNotizen = new Label(grpGeneral, SWT.NONE);
		lblNotizen.setText("Note");
		new Label(grpGeneral, SWT.NONE);

		// add text field on the general tab which contains the notes to the
		// material/datafile/Folder etc
		noteField = new Text(grpGeneral, SWT.MULTI);
		noteField.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		// the below listener changes the note text of the element
		// in the model when the text field lost the focus
		noteField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				controller.setModelElementProperty(selectedMaterialTreeElmentId, "Note", noteField.getText());
			}
		});

		// properties view creation
		Composite propertiesComposite = new Composite(propertiesArea, SWT.NONE);
		propertiesComposite.setLayout(new FillLayout(SWT.HORIZONTAL));

		materialPropertyCategoryTabFolder = new TabFolder(propertiesComposite, SWT.NONE);

		// create toolbar area below properties tabs
		Composite toolbarArea = new Composite(propertySide, SWT.NONE);
		GridData gd_toolbarArea = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_toolbarArea.heightHint = 35;
		gd_toolbarArea.widthHint = 167;
		toolbarArea.setLayoutData(gd_toolbarArea);

		ToolBar toolBar = new ToolBar(toolbarArea, SWT.FLAT);
		toolBar.setBackground(new Color(this.device, new RGB(220, 220, 220)));
		toolBar.setLocation(0, 0);
		toolBar.setSize(500, 35);

// this is commented out since it is not implemented yet
//		ToolItem tltmDatenbankverwaltung = new ToolItem(toolBar, SWT.NONE);
//		tltmDatenbankverwaltung.setText("add DataFile");
		
		// create the drop down button for the custom actions
		taskDropDown = new ToolItem(toolBar, SWT.ARROW);
		taskDropDown.setText("Actions");
		taskDropDown.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// for implementation of the options to run custom actions
				super.widgetSelected(e);
				// get position of the button to place the menu correctly
				// under the button
				Rectangle rect = taskDropDown.getBounds();
				Point pt = new Point(rect.x, rect.y + rect.height);
				pt = toolBar.toDisplay(pt);
				taskMenu.setLocation(pt.x, pt.y);
				taskMenu.setVisible(true);
			}
		});

		// set task menu to select different tasks
		this.taskMenu = new Menu(shlEditor, SWT.CASCADE);

		// further toolbar items
		// save all button
		ToolItem tltmSaveAll = new ToolItem(toolBar, SWT.NONE);
		tltmSaveAll.setText("Save all");
		tltmSaveAll.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					controller.saveModel();
				} catch (Exception ex) {
					// stop saving when Exception occured
					controller.raiseError("Saving Model aborted due to '" + ex.getMessage() + "'");
				}
			}
		});

		// add select button to parse selection to the dc43
		ToolItem tltmSelect = new ToolItem(toolBar, SWT.NONE);
		tltmSelect.setText("Select");
		tltmSelect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// if there is a material selected in the material tree
				// set the uuid as selectedMaterial Id to parse this id to the
				// dc43
				if (materialTree.getSelectionCount() > 0) {
					MaterialTreeItem selectedMaterial = (MaterialTreeItem) materialTree.getSelection()[0];
					selectedMaterialID = selectedMaterial.getId();
				} else {
					selectedMaterialID = null;
				}
				// close the Material Database
				tltmSelect.getParent().getShell().close();
			}
		});

		// close button
		ToolItem tltmClose = new ToolItem(toolBar, SWT.NONE);
		tltmClose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				tltmClose.getParent().getShell().close();
			}
		});
		tltmClose.setText("Close");
		toolBar.pack();

		// get size of elements of toolbar and set toolbar size
		int xSize = 0;
		for (ToolItem child : toolBar.getItems()) {
			xSize = xSize + child.getBounds().width;
		}
		toolBar.setSize(xSize, toolBar.getBounds().height);

		// set size of material database window
		sashForm.setWeights(new int[] { 175, 550 });
	}

	@Override
	public void initialize() {
		// this is not needed for this implementation of the abstract view
	}

	@Override
	public void show() {
		try {
			this.open();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public UUID selectAndReturnMaterialIdFromGUI() {
		// show the GUI to select the material
		this.show();
		// return the selected material id and set the value to null so next time
		// there is no problem when no material was selected
		UUID selectedId = this.selectedMaterialID;
		this.selectedMaterialID = null;
		return selectedId;
	}

	@Override
	public void setController(AbstractController controller) {
		this.controller = controller;
	}

	@Override
	public void addTask(String name) {
		// controller calls this and adds a task to the task menu
		MenuItem taskItem = new MenuItem(this.taskMenu, SWT.PUSH);
		taskItem.setText(name);
		taskItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controller.runTask(name);
			}
		});
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// track changes made to the model which means deleting/creating/moving elements
		if (evt.getSource() instanceof AbstractModel) {
			UUID elementId;
			String elementType;

			// check if a graph element was deleted from the Model
			if (evt.getNewValue() == null) {
				// quick and dirty catch of null pointer exception for the presentation
				try {
					elementId = UUID.fromString((String) evt.getOldValue());
	
					// since the type of the deleted element does not exist anymore, the parent
					// element dataType
					// gives information about what the element was
					UUID parentElementId = UUID.fromString(evt.getPropertyName());
					String parentElementType = (String) this.controller.getModelElementProperty(parentElementId, "Type");
	
					if (parentElementType.equals("root") || parentElementType.equals("dataFile")
							|| parentElementType.equals("folder")) {
						// if the element is of type folder, datafile or material simply remove it
						this.removeMaterialTreeElement(elementId);
	
					} else if (this.getSelectedMaterialTreeItemId() != null
							&& this.controller.getModelElementProperty(this.getSelectedMaterialTreeItemId(), "Type")
									.equals("material")
							&& (this.controller.isElementChildOf(parentElementId, this.getSelectedMaterialTreeItemId()))
							|| parentElementId.equals(this.getSelectedMaterialTreeItemId())) {
						// changes to material property and material property category only have to be
						// processed, if the parent material is currently displayed
						// this is the cause if:
						// the parent is the currently selected item in the material tree ->category was
						// deleted
						// the parent is a child of the current selected item in the material tree ->
						// property was deleted
	
						if (parentElementType.equals("material")) {
							// simply dispose materialPropertycategory belonging to the deleted element
							TabItem[] visibleMaterialPropertyCategories = this.materialPropertyCategoryTabFolder.getItems();
							for (TabItem tabItem : visibleMaterialPropertyCategories) {
								MaterialTabItem materialPropertyCategory = (MaterialTabItem) tabItem;
								if (materialPropertyCategory.getId().equals(elementId)) {
									materialPropertyCategory.dispose();
								}
							}
	
						} else if (parentElementType.equals("category")) {
							// get the corresponding material Table item for the elementId and dispose it
							MaterialTabItem propertyParentCategoryTab = this
									.getMaterialPropertyCategoryTabById(parentElementId);
							Table tabControl = (Table) propertyParentCategoryTab.getControl();
							TableItem[] materialTableItems = tabControl.getItems();
							for (TableItem tableItem : materialTableItems) {
								MaterialTableItem materialTableItem = (MaterialTableItem) tableItem;
								if (materialTableItem.getId().equals(elementId)) {
									materialTableItem.dispose();
								}
							}
						}
					}
				}catch (NullPointerException ex) {
					// catch deletion errors on startup because the listener pattern
					// makes it necessary to reload the graph, so the elements are shown
					// correctly. This is done during startup which leads to nullpointer
					// exceptions
					if(!this.startupFlag) {
						this.raiseError("Could not delete element with UUID "+(String)evt.getOldValue()+
								".\nSince there is no corresponding element existing in the view.");
					}
				}

				// check if a graph element was added to the model
			} else if (evt.getOldValue() == null) {
				elementId = UUID.fromString((String) evt.getNewValue());
				elementType = (String) this.controller.getModelElementProperty(elementId, "Type");
				String elementName = (String) this.controller.getModelElementProperty(elementId, "Name");
				UUID parentElementId = UUID.fromString(evt.getPropertyName());

				// if element is not property or materialCategroy simply add it to the material
				// Tree
				if (elementType.equals("folder") || elementType.equals("dataFile") || elementType.equals("material")) {
					this.addMaterialTreeElement(elementName, elementId, parentElementId);

					// if element is of type material category or property the change is only
					// relevant
					// if its parent element is currently displayed
				} else if (this.getSelectedMaterialTreeItemId() != null
						&& this.controller.getModelElementProperty(this.getSelectedMaterialTreeItemId(), "Type")
								.equals("material")
						&& (this.controller.isElementChildOf(parentElementId, this.getSelectedMaterialTreeItemId()))
						|| parentElementId.equals(this.getSelectedMaterialTreeItemId())) {

					// simply reload the material in the view and set the selected tab to the one
					// which was
					// previous selected
					int selectedMaterialPropertyCategoryTab = this.materialPropertyCategoryTabFolder
							.getSelectionIndex();
					this.showMaterial(this.getSelectedMaterialTreeItemId());
					this.materialPropertyCategoryTabFolder.setSelection(selectedMaterialPropertyCategoryTab);
				}
			}

			// track changes made to the elements in the model e.g. changing properties of
			// abstract graph elements
		} else if (evt.getSource() instanceof AbstractGraphElement) {
			// specific changes of the element itself which are inherited from
			// abstractGraphElement and therefore for all graph elements important

			UUID elementId = (UUID) ((AbstractGraphElement) evt.getSource()).getId();
			String elementType = (String) controller.getModelElementProperty(elementId, "Type");
			UUID currentlyVisibleMaterialCategoryTab = this.getVisibleMaterialPropertyCategoryTabId();

			if (elementType.equals("folder") || elementType.equals("dataFile") || elementType.equals("material")) {
				MaterialTreeItem treeItem = this.getMaterialTreeItemById(elementId);
				MaterialTreeItem[] selectedItem = Arrays.copyOf(this.materialTree.getSelection(),
						this.materialTree.getSelectionCount(), MaterialTreeItem[].class);

				// treat case that element changed its name
				if (evt.getPropertyName().equals("name") && treeItem != null && evt.getNewValue() instanceof String
						&& selectedItem.length != 0) {
					treeItem.setText((String) evt.getNewValue());
					if (selectedItem[0] != null && selectedItem[0].getId().equals(elementId)) {
						this.nameField.setText((String) evt.getNewValue());
					}

				} else if (evt.getPropertyName().equals("note") && treeItem != null
						&& evt.getNewValue() instanceof String && selectedItem.length != 0) {
					// refresh change of the note
					if (selectedItem[0] != null && selectedItem[0].getId().equals(elementId)) {
						this.noteField.setText((String) evt.getNewValue());
					}
				}

				// this treats changes made "live" to the Material Property Properties
				// so it is checked of the currently displayed materialCategory tab contains the
				// property
			} else if (elementType.equals("property")
					&& this.controller.isElementChildOf(elementId, currentlyVisibleMaterialCategoryTab)) {

				// get elements which are currently displayed in the view
				Table visibleMaterialPropertyTable = this.getCurrentlyVisibleMaterialPropertyTable();
				TableColumn[] propertyTableColumns = visibleMaterialPropertyTable.getColumns();
				MaterialTableItem materialProperty = this.getMaterialPropertyInActiveTableById(elementId);

				// initialize variables needed to display the material properties values
				String oldValue = null;
				String newValue = null;

				// check what the incoming change value is and convert values to their belonging
				// strings
				if (ClassUtils.isPrimitiveOrWrapper(evt.getNewValue().getClass())) {
					newValue = String.valueOf(evt.getNewValue());
				} else {
					newValue = evt.getNewValue().toString();
				}

				if (ClassUtils.isPrimitiveOrWrapper(evt.getOldValue().getClass())) {
					oldValue = String.valueOf(evt.getOldValue());
				} else {
					oldValue = evt.getOldValue().toString();
				}

				// using a counter to determine the position of the column in the array instead
				// of indexOf() operation to save time and calculation power
				int columnNumber = 0;

				// get corresponding column in the material property table
				for (TableColumn column : propertyTableColumns) {
					// check of column name is the same as the name of the set property and
					// check if the value before replacement is the same as the old value
					// given in the event
					if (column.getText().equalsIgnoreCase(evt.getPropertyName())
							&& materialProperty.getText(columnNumber).equals(oldValue)) {
						materialProperty.setText(columnNumber, newValue);
						return;
					}
					columnNumber++;
				}

				// this treats with changes made "live" to the Material Property category tabs
				// therefore it is checked if the material property category belongs to the
				// currently selected element of the tree
			} else if (elementType.equals("category")
					&& this.controller.isElementChildOf(elementId, this.getSelectedMaterialTreeItemId())) {
				// convert new of event to string for view, valueOf is needed to
				// ensure that all values are converted to string
				String newValue = String.valueOf(evt.getNewValue());

				// get all visible material tab items and check if the id matches the id
				// given in the event
				TabItem[] visibleMaterialPropertyCategoryTabs = this.materialPropertyCategoryTabFolder.getItems();
				if (visibleMaterialPropertyCategoryTabs.length != 0) {
					for (TabItem tabItem : visibleMaterialPropertyCategoryTabs) {
						MaterialTabItem materialTabItem = (MaterialTabItem) tabItem;
						if (materialTabItem.getId().equals(elementId)) {
							materialTabItem.setText(newValue);
							return;
						}
					}
				}
				return;
			}
		}
	}

	@Override
	public void raiseError(String description) {
		MessageDialog.openWarning(this.shlEditor, "Warning", description);
	}

	@Override
	public int showDecisionWindow(String description, String[] options) {
		MessageDialog dialog = new MessageDialog(this.shlEditor, "Warnung", null, description, MessageDialog.WARNING,
				options, 0);
		int result = dialog.open();
		return result;
	}

	////////////// private functions for in view use only

	////// functions regarding material tree
	private void addMaterialTreeElement(String elementName, UUID elementId, UUID parentId) {
		String elementType = (String) this.controller.getModelElementProperty(elementId, "Type");
		TreeItem element = null;
		// dataFile type has no parent in the view and therefore will be added directly
		// to the material tree
		if (elementType.equals("dataFile")) {
			// sort data file alphabetically into tree
			int indexInMaterialTree = this.getMaterialTreeIndexAlphabetical(elementName, this.materialTree.getItems());
			element = new MaterialTreeItem(this.materialTree, SWT.NONE, elementId, indexInMaterialTree);
		} else {
			MaterialTreeItem parentTreeItem = this.getMaterialTreeItemById(parentId);
			int indexInMaterialTree = this.getMaterialTreeIndexAlphabetical(elementName, parentTreeItem.getItems());
			if (indexInMaterialTree == parentTreeItem.getItems().length) {
				element = new MaterialTreeItem(parentTreeItem, SWT.NONE, elementId);
			} else {
				element = new MaterialTreeItem(parentTreeItem, SWT.NONE, elementId, indexInMaterialTree);
			}
		}
		element.setText(elementName);
	}

	private void removeMaterialTreeElement(UUID elementId) {
		// dispose element and all of its child elements
		MaterialTreeItem itemToRemove = this.getMaterialTreeItemById(elementId);

		// if selected item is displayed, close the view part
		if (this.getSelectedMaterialTreeItemId() != null && this.getSelectedMaterialTreeItemId().equals(elementId)) {
			this.propertiesArea.setVisible(false);
			if (this.getSelectedMaterialTreeItemId() != null) {
				this.materialTree.deselectAll();
			}
		}
		itemToRemove.dispose();
	}

	private UUID getSelectedMaterialTreeItemId() {
		TreeItem[] selectedTreeElement = this.materialTree.getSelection();
		if (selectedTreeElement.length != 0) {
			MaterialTreeItem selectedMaterialTreeElement = (MaterialTreeItem) selectedTreeElement[0];
			return selectedMaterialTreeElement.getId();
		}
		return null;
	}

	private MaterialTreeItem getMaterialTreeItemById(UUID id) {
		// this iterates through all the items of the material tree in the view
		// and returns the item which corresponds to the given ID or null
		// if the id is not contained by any tree item
		ArrayList<MaterialTreeItem> treeItemList = new ArrayList<>();
		for (TreeItem treeItem : this.materialTree.getItems()) {
			MaterialTreeItem item = (MaterialTreeItem) treeItem;
			treeItemList.add(item);
		}
		while (!treeItemList.isEmpty()) {
			MaterialTreeItem currentItem = treeItemList.remove(0);
			if (currentItem.getId().equals(id)) {
				return currentItem;
			} else {
				for (TreeItem subTreeItem : currentItem.getItems()) {
					treeItemList.add((MaterialTreeItem) subTreeItem);
				}
			}
		}
		return null;
	}

	private int getMaterialTreeIndexAlphabetical(String elementName, TreeItem[] otherElements) {
		// this takes a list of treeItems and returns the place as index where to put
		// the element with
		// the name

		// make name case lower so alphabetical "Z" is not before "a"
		elementName = elementName.toLowerCase();
		int otherElementslength = otherElements.length;
		if (otherElementslength == 0) {
			return 0;
		} else if (otherElementslength == 1) {
			int compareValue = elementName.compareTo(otherElements[0].getText().toLowerCase());
			if (compareValue <= -1 || compareValue == 0) {
				return 0;
			} else if (compareValue >= 1) {
				return 1;
			}
		} else {
			for (int index = 0; index < otherElementslength - 1; index++) {
				String elementNameAtIndex = otherElements[index].getText().toLowerCase();
				String elementNameAtIndexPlus1 = otherElements[index + 1].getText().toLowerCase();

				int elementComparedToIndexElement = elementName.compareTo(elementNameAtIndex);
				int elementComparedToIndexPlus1Element = elementName.compareTo(elementNameAtIndexPlus1);
				if (elementComparedToIndexElement > 0 && elementComparedToIndexPlus1Element < 0) {
					return index + 1;
				} else if (elementComparedToIndexElement < 0 && elementComparedToIndexPlus1Element < 0) {
					return 0;
				}
			}
			return otherElementslength;
		}
		return 0;
	}

	//////////// functions affecting the detailed view on the right side of the
	//////////// window

	private void showMaterial(UUID elementId) {
		this.clearMaterialPropertyCategoryTab();
		this.showGeneralProperties(elementId);
		this.addMaterialPropertiesAndCategoriesToView(elementId);
		this.materialPropertyCategoryTabFolder.setVisible(true);
	}

	private void showGeneralProperties(UUID elementId) {
		// displays the element name, note and database path of its parent database in
		// the right general view
		String elementName = (String) this.controller.getModelElementProperty(elementId, "Name");
		this.nameField.setText(elementName);
		String elementDataFilePath = this.controller.getParentDataFilePath(elementId);
		this.databaseField.setText(elementDataFilePath);
		String note = (String) this.controller.getModelElementProperty(elementId, "Note");
		this.noteField.setText(note);

		// set element as selected so next time the last selected element can be
		// determined
		this.selectedMaterialTreeElmentId = this.getSelectedMaterialTreeItemId();
	}

	// functions corresponding to the property view of the material on the right
	// side
	private void addMaterialPropertyCategoryToView(UUID categoryId) {
		MaterialTabItem categoryTab = new MaterialTabItem(materialPropertyCategoryTabFolder, SWT.NONE, categoryId);
		String name = (String) this.controller.getModelElementProperty(categoryId, "Name");
		if (name != null) {
			categoryTab.setText(name);
			this.addMaterialPropertyTable(categoryTab);
		}
	}

	private void addMaterialPropertyToView(UUID propertyId) {
		UUID propertyParentCategory = (UUID) this.controller.getModelElementProperty(propertyId, "ParentId");
		MaterialTabItem propertyParentCategoryTab = this.getMaterialPropertyCategoryTabById(propertyParentCategory);
		Table tabControl = (Table) propertyParentCategoryTab.getControl();
		MaterialTableItem property = new MaterialTableItem(tabControl, SWT.NONE, propertyId);

		// get values of the property and fill it into the table item
		String propertyName = (String) this.controller.getModelElementProperty(propertyId, "Name");
		String propertyValue = (String) this.controller.getModelElementProperty(propertyId, "ValueObject").toString();
		String propertyUnit = (String) this.controller.getModelElementProperty(propertyId, "Unit");
		String propertyNote = (String) this.controller.getModelElementProperty(propertyId, "Note");
		String propertyLutUnit = (String) this.controller.getModelElementProperty(propertyId, "LookUpTableUnit");
		if (propertyNote == null) {
			propertyNote = "";
		}
		// set text for the columns of the Material property table
		property.setText(new String[] { propertyName, propertyValue, propertyUnit, propertyLutUnit, propertyNote });
	}

	private void addMaterialPropertiesAndCategoriesToView(UUID materialId) {
		UUID[] propertyCategoryIds = this.controller.getModelElementChildIds(materialId);
		for (UUID category : propertyCategoryIds) {
			this.addMaterialPropertyCategoryToView(category);

			// get all material properties of this category and add them to the
			// materialProperty table
			// of the corresponding category tab
			UUID[] materialProperties = this.controller.getModelElementChildIds(category);
			for (UUID materialProperty : materialProperties) {
				this.addMaterialPropertyToView(materialProperty);
			}
		}
	}

	private MaterialTabItem getMaterialPropertyCategoryTabById(UUID categoryId) {
		// iterate through the material category tabs of the view and returning the one
		// with the corresponding id
		TabItem[] materialPropertyCategoryTabs = this.materialPropertyCategoryTabFolder.getItems();
		for (TabItem tab : materialPropertyCategoryTabs) {
			MaterialTabItem convertedTab = (MaterialTabItem) tab;
			if (convertedTab.getId().equals(categoryId)) {
				return convertedTab;
			}
		}
		return null;
	}

	private MaterialTableItem getMaterialPropertyInActiveTableById(UUID propertyId) {
		TableItem[] tableItems = this.getCurrentlyVisibleMaterialPropertyTable().getItems();
		for (TableItem tableItem : tableItems) {
			MaterialTableItem materialTableItem = (MaterialTableItem) tableItem;
			if (materialTableItem.getId().equals(propertyId)) {
				return materialTableItem;
			}
		}
		return null;

	}

	private UUID getVisibleMaterialPropertyCategoryTabId() {
		TabItem[] selectedPropertyCategories = this.materialPropertyCategoryTabFolder.getSelection();
		if (selectedPropertyCategories.length != 0) {
			MaterialTabItem materialTab = (MaterialTabItem) selectedPropertyCategories[0];
			return materialTab.getId();
		}
		return null;
	}

	private void clearMaterialPropertyCategoryTab() {
		TabItem[] categories = this.materialPropertyCategoryTabFolder.getItems();
		for (TabItem tab : categories) {
			tab.dispose();
		}
	}

	private void addMaterialPropertyTable(MaterialTabItem tab) {
		// this function adds a table to the Material Category tab to display the
		// properties which are included in the category
		Table materialPropertyTable = new Table(tab.getParent(), SWT.SINGLE | SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
		tab.setControl(materialPropertyTable);
		materialPropertyTable.setHeaderVisible(true);
		materialPropertyTable.setLinesVisible(true);
		materialPropertyTable.addListener(SWT.MouseDown, this);

		// add listener so the material properties can be selected to be changed
		// by doubleclicking on it
		materialPropertyTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				Point pt = new Point(e.x, e.y);
				TableItem item = materialPropertyTable.getItem(pt);
				if (item == null)
					return;
				for (int i = 0; i < materialPropertyTable.getColumnCount(); i++) {
					Rectangle rect = item.getBounds(i);
					if (rect.contains(pt)) {
						// edit for material property menu
						showMaterialPropertyEditor(materialPropertyTable.getColumn(i).getText());
					}
				}
			}
		});

		// create table header line
		TableColumn tblclmnName = new TableColumn(materialPropertyTable, SWT.CENTER);
		tblclmnName.setWidth(120);
		tblclmnName.setText("Name");

		TableColumn tblclmnVaue = new TableColumn(materialPropertyTable, SWT.CENTER);
		tblclmnVaue.setWidth(100);
		tblclmnVaue.setText("Value");

		TableColumn tblclmnUnit = new TableColumn(materialPropertyTable, SWT.CENTER);
		tblclmnUnit.setWidth(100);
		tblclmnUnit.setText("Unit");

		TableColumn tblclmnLutUnit = new TableColumn(materialPropertyTable, SWT.CENTER);
		tblclmnLutUnit.setWidth(130);
		tblclmnLutUnit.setText("LookUpTableUnit");

		TableColumn tblclmnNote = new TableColumn(materialPropertyTable, SWT.CENTER);
		tblclmnNote.setWidth(100);
		tblclmnNote.setText("Note");

		// add sub-menu entries
		propertySubMenu = new Menu(materialPropertyTable);

		// add menu entry for creating new material property
		MenuItem addPropertyItem = new MenuItem(propertySubMenu, SWT.POP_UP);
		addPropertyItem.setText("add Property");
		addPropertyItem.addSelectionListener(this);

		// define remove property
		MenuItem removePropertyItem = new MenuItem(this.propertySubMenu, SWT.POP_UP);
		removePropertyItem.setText("remove Property");
		removePropertyItem.addSelectionListener(this);

		MenuItem addCategoryItem = new MenuItem(this.propertySubMenu, SWT.POP_UP);
		addCategoryItem.setText("add Category");
		addCategoryItem.addSelectionListener(this);

		MenuItem renameCategoryItem = new MenuItem(this.propertySubMenu, SWT.POP_UP);
		renameCategoryItem.setText("rename Category");
		renameCategoryItem.addSelectionListener(this);
		
		MenuItem removeCategoryItem = new MenuItem(this.propertySubMenu, SWT.POP_UP);
		removeCategoryItem.setText("remove Category");
		removeCategoryItem.addSelectionListener(this);

		// define sub menu for the material property category tab when no property is
		// selected
		this.categorySubMenu = new Menu(materialPropertyTable);
		MenuItem addPropertyItem2 = new MenuItem(this.categorySubMenu, SWT.POP_UP);
		addPropertyItem2.setText("add Property");
		addPropertyItem2.addSelectionListener(this);

		addCategoryItem = new MenuItem(this.categorySubMenu, SWT.POP_UP);
		addCategoryItem.setText("add Category");
		addCategoryItem.addSelectionListener(this);

		renameCategoryItem = new MenuItem(this.categorySubMenu, SWT.POP_UP);
		renameCategoryItem.setText("rename Category");
		renameCategoryItem.addSelectionListener(this);
		
		removeCategoryItem = new MenuItem(this.categorySubMenu, SWT.POP_UP);
		removeCategoryItem.setText("remove Category");
		removeCategoryItem.addSelectionListener(this);
	}

	private Table getCurrentlyVisibleMaterialPropertyTable() {
		TabItem[] activeTabs = materialPropertyCategoryTabFolder.getSelection();
		if (activeTabs.length != 0) {
			Control materialProperty = activeTabs[0].getControl();
			return (Table) materialProperty;
		} else {
			return null;
		}
	}

	private void showMaterialPropertyEditor(String itemText) {
		// delete the "Edit " so the corresponding table column for the attribute can be
		// found
		String attributeName = itemText.replace("Edit ", "");

		// get the material Property table of the currently visible Property tab
		Table materialPropertyTable = this.getCurrentlyVisibleMaterialPropertyTable();

		// get the cell in the materialpropertyTable which corresponds to the selected
		// attribute in the right-click menu and open an editor for it
		TableColumn[] tableColumns = materialPropertyTable.getColumns();
		TableItem[] selectedMaterialProperties = materialPropertyTable.getSelection();
		int correspondingColumnNumber;
		for (TableColumn column : tableColumns) {
			if (column.getText().equals(attributeName)) {
				correspondingColumnNumber = ArrayUtils.indexOf(tableColumns, column);

				// create new editor to make the cell editable
				propertyTableEditor = new TableEditor(materialPropertyTable);
				propertyTableEditor.horizontalAlignment = SWT.LEFT;
				propertyTableEditor.grabHorizontal = true;
				propertyTableEditor.minimumWidth = 80;

				// case that the button in the right-click menu is selected twice
				Control oldEditor = propertyTableEditor.getEditor();
				if (oldEditor != null)
					oldEditor.dispose();

				// set text Field over the cell which will be used as editor interface
				Text newEditorTextField = new Text(materialPropertyTable, SWT.NONE);
				newEditorTextField.setText(selectedMaterialProperties[0].getText(correspondingColumnNumber));
				newEditorTextField.setForeground(selectedMaterialProperties[0].getForeground());
				newEditorTextField.selectAll();
				newEditorTextField.setFocus();

				// add a focus listener so the value of the editor is used when the focus is
				// lost
				newEditorTextField.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent arg0) {
						Text editorTextField = (Text) arg0.widget;
						applyMaterialPropertyChangeFromEditor(editorTextField.getText());
						propertyTableEditor.getEditor().dispose();

					}
				});

				// when pressing enter the changed property should be set
				newEditorTextField.addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						int keyCode = e.keyCode;
						if ((keyCode == (int) SWT.CR) || (keyCode == SWT.KEYPAD_CR) || (keyCode == SWT.LF)) {
							Text editorTextField = (Text) e.widget;
							applyMaterialPropertyChangeFromEditor(editorTextField.getText());
							editorTextField.dispose();
						}
					}
				});

				// open editor for next property field if traversed
				newEditorTextField.addTraverseListener(new TraverseListener() {
					@Override
					public void keyTraversed(TraverseEvent arg0) {
						// return if the event is not caused by a tab traverse
						if (arg0.detail == SWT.TRAVERSE_TAB_NEXT) {
							// apply changes made to the text field
							Text editorTextField = (Text) arg0.widget;
							applyMaterialPropertyChangeFromEditor(editorTextField.getText());
							// get next text field and open editor
							TableItem[] selectedProperties = materialPropertyTable.getSelection();
							TableItem selectedProperty = selectedProperties[0];
							if (correspondingColumnNumber == materialPropertyTable.getColumnCount() - 1) {
								// set editor to next item, first table cell, if it exists
								int propertyIndex = ArrayUtils.indexOf(materialPropertyTable.getItems(),
										selectedProperty);
								if (propertyIndex < materialPropertyTable.getItemCount() - 1) {
									MaterialTableItem nextItem = (MaterialTableItem) materialPropertyTable
											.getItem(propertyIndex + 1);
									materialPropertyTable.setSelection(
											ArrayUtils.indexOf(materialPropertyTable.getItems(), nextItem));
									showMaterialPropertyEditor(materialPropertyTable.getColumn(0).getText());
								} else {
									// if there is no item below the current item start again from the beginning
									// with traversing
									materialPropertyTable.setSelection(0);
									showMaterialPropertyEditor(materialPropertyTable.getColumn(0).getText());
								}
							} else {
								// set editor to next column
								TableColumn nextColumn = materialPropertyTable.getColumn(correspondingColumnNumber + 1);
								showMaterialPropertyEditor(nextColumn.getText());
							}
							arg0.widget.dispose();
						}else if(arg0.detail == SWT.TRAVERSE_ESCAPE) {
							arg0.widget.dispose();
						}
					}
				});
				// set the editor to the created text field and link with the table cell
				propertyTableEditor.setEditor(newEditorTextField, selectedMaterialProperties[0],
						correspondingColumnNumber);
				return;
			}
		}
		this.controller
				.raiseError("Cold not find corresponding Material property for selection '" + attributeName + "'");
	}

	private void showPropertyCategoryNameEditor() {
		UUID currentlyVisiblePropertyCategoryId = this.getVisibleMaterialPropertyCategoryTabId();
		MaterialTabItem propertyParentCategoryTab = this
				.getMaterialPropertyCategoryTabById(currentlyVisiblePropertyCategoryId);
		Table tabControl = (Table) propertyParentCategoryTab.getControl();
		MaterialTableItem property = new MaterialTableItem(tabControl, SWT.NONE, UUID.randomUUID());
		property.setText(new String[] {});

		// create new editor to make the cell editable
		propertyTableEditor = new TableEditor(this.getCurrentlyVisibleMaterialPropertyTable());
		propertyTableEditor.horizontalAlignment = SWT.LEFT;
		propertyTableEditor.grabHorizontal = true;
		propertyTableEditor.minimumWidth = 500;

		// case that the button in the right-click menu is selected twice
		Control oldEditor = propertyTableEditor.getEditor();
		if (oldEditor != null)
			oldEditor.dispose();

		// set text Field over the cell which will be used as editor interface
		Text newEditorTextField = new Text(this.getCurrentlyVisibleMaterialPropertyTable(), SWT.BORDER);
		newEditorTextField.setText(propertyParentCategoryTab.getText());
		newEditorTextField.setForeground(property.getForeground());
		// the lines below sets an listener to the editor of the cell so
		// when pressing enter, the change will be sent to the model
		newEditorTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(final KeyEvent e) {
				if ((e.keyCode == (int) SWT.CR) || (e.keyCode == SWT.KEYPAD_CR) || (e.keyCode == SWT.LF)) {
					Text editorTextField = (Text) e.widget;
					controller.setModelElementProperty(currentlyVisiblePropertyCategoryId, "Name",
							editorTextField.getText());
					propertyTableEditor.getEditor().dispose();
					// this is needed since it created a fake property which will be delted here
					property.dispose();
				}
			}
		});

		newEditorTextField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				Text editorTextField = (Text) e.widget;
				controller.setModelElementProperty(currentlyVisiblePropertyCategoryId, "Name",
						editorTextField.getText());
				propertyTableEditor.getEditor().dispose();
				// this is needed since it created a fake property which will be delted here
				property.dispose();
			}
		});

		newEditorTextField.selectAll();
		newEditorTextField.setFocus();
		// add listener to close editor on focus lost, but not save changes which where
		// made in the editor
		newEditorTextField.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				// close the editor
				propertyTableEditor.getEditor().dispose();

				// delete added material
				property.dispose();
			}
		});
		// set the editor to the created text field and link with the table cell
		propertyTableEditor.setEditor(newEditorTextField, property, 0);

	}

	private void applyMaterialPropertyChangeFromEditor(String newValue) {
		// get the ID of the changed material property and the property of the property,
		// which has been changed and forward the changes to the model
		TabItem[] selectedItems = this.materialPropertyCategoryTabFolder.getSelection();
		if (selectedItems.length != 0) {
			MaterialTabItem selectedItem = (MaterialTabItem) selectedItems[0];
			Table control = (Table) selectedItem.getControl();
			UUID selectedMaterialPropertyTableItemId = this.getSelectedMaterialPropertyId();
			String propertyName = control.getColumn(this.propertyTableEditor.getColumn()).getText();

			// check if a look up table was entered
			if (newValue.strip().endsWith("}") && newValue.strip().startsWith("{")) {
				// convert the string to hash map
				Map<Double, Double> propertyLookUpTable = new HashMap<>();
				try {
					// get the values out of the String
					newValue = newValue.substring(1, newValue.length() - 1);
					newValue = newValue.replace(" ", "");
					String[] valuePairs = newValue.split(",");
					for (String valuePair : valuePairs) {
						String[] splittedPair = valuePair.split("=");
						Double value1 = Double.valueOf(splittedPair[0].strip());
						Double value2 = Double.valueOf(splittedPair[1].strip());
						// fill look up table/hashmap with the values
						propertyLookUpTable.put(value1, value2);
					}
				} catch (Exception ex) {
					this.controller.raiseError("Could not save value due to unknown data format.\n"
							+ "Insert number for value or look up table with format '{xPoint1=yPoint,xPoint2=yPoint2,...}'");
					// close editor and exit function
					this.propertyTableEditor.getEditor().dispose();
					return;
				}
				// send changes to controller/model
				this.controller.setModelElementProperty(selectedMaterialPropertyTableItemId, propertyName,
						propertyLookUpTable);
				// close the editor
				this.propertyTableEditor.getEditor().dispose();
				return;

			}
			this.controller.setModelElementProperty(selectedMaterialPropertyTableItemId, propertyName, newValue);
		}
		// close the editor
		this.propertyTableEditor.getEditor().dispose();
	}

	private UUID getSelectedMaterialPropertyId() {
		TabItem[] selectedItems = this.materialPropertyCategoryTabFolder.getSelection();
		if (selectedItems.length != 0) {
			MaterialTabItem selectedItem = (MaterialTabItem) selectedItems[0];
			Table control = (Table) selectedItem.getControl();
			MaterialTableItem selectedMaterialPropertyTableItem = (MaterialTableItem) control.getSelection()[0];
			return selectedMaterialPropertyTableItem.getId();
		}
		return null;
	}

	// methods corresponding to the selection listener implementation of the view
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// this is unused
	}

	@Override
	public void widgetSelected(SelectionEvent arg0) {
		if (arg0.getSource() instanceof Tree) {
			// when an item is selected in the material tree on the left, the corresponding
			// element is fetched from the model via the controller and displayed
			MaterialTreeItem selectedItem = (MaterialTreeItem) arg0.item;
			UUID elementId = selectedItem.getId();
			String elementType = (String) this.controller.getModelElementProperty(elementId, "Type");
			if (elementType.equals("material")) {
				this.showMaterial(elementId);
			} else if (elementType.equals("dataFile")) {
				this.showGeneralProperties(elementId);
				this.materialPropertyCategoryTabFolder.setVisible(false);
			} else if (elementType.equals("folder")) {
				this.showGeneralProperties(elementId);
				this.materialPropertyCategoryTabFolder.setVisible(false);
			} else {
				this.controller.raiseError(
						"Selected element has unknown property <type>: '" + elementType + "'.\n Could not display");
				return;
			}
			this.propertiesArea.setVisible(true);

		} else if (arg0.getSource() instanceof MenuItem) {
			// this deals with the behavior of the pup up menu when right-clicking in the
			// Material tree
			MenuItem selectedMenuItem = (MenuItem) arg0.getSource();
			String itemText = selectedMenuItem.getText();
			if (itemText.equals("new Material")) {
				UUID selectedElementId = this.getSelectedMaterialTreeItemId();
				this.controller.addMaterialWithGeneralCategory(selectedElementId);
			} else if (itemText.equals("new Folder")) {
				UUID selectedElementId = this.getSelectedMaterialTreeItemId();
				this.controller.addFolder(selectedElementId);
			} else if (itemText.equals("remove")) {
				UUID selectedElementId = this.getSelectedMaterialTreeItemId();
				this.controller.removeElement(selectedElementId);
			} else if (itemText.contains("add Property")) {
				UUID selectedPropertyCategory = this.getVisibleMaterialPropertyCategoryTabId();
				this.controller.addMaterialProperty(selectedPropertyCategory);
			} else if (itemText.contains("add Category")) {
				UUID correspondingMaterialId = this.getSelectedMaterialTreeItemId();
				this.controller.addMaterialPropertyCategory(correspondingMaterialId);
			} else if (itemText.contains("remove Property")) {
				UUID correspondingMaterialPropertyid = this.getSelectedMaterialPropertyId();
				this.controller.removeElement(correspondingMaterialPropertyid);
			} else if (itemText.contains("rename Category")) {
				this.showPropertyCategoryNameEditor();
			} else if (itemText.contains("remove Category")) {
				UUID currentlyVisibleCategoryId = this.getVisibleMaterialPropertyCategoryTabId();
				this.controller.removeElement(currentlyVisibleCategoryId);
			}
		} else if (arg0.getSource() instanceof ToolItem) {
			// for implementation of the options to run custom actions
			ToolItem item = (ToolItem) arg0.getSource();
			this.controller.runTask(item.getText());

		} else {
			this.controller.raiseError("Selection listener got a element of type "
					+ arg0.getSource().getClass().toString() + " which is unknown. No Action Performed.");
		}
	}

	// method corresponding to the listener implementation of the view
	@Override
	public void handleEvent(Event arg0) {
		if (arg0.widget instanceof Tree && arg0.button == 3) {
			// check if right click menu for the tree can be shown
			Point point = new Point(arg0.x, arg0.y);
			TreeItem item = this.materialTree.getItem(point);
			if (item != null) {
				// show right click menu of the tree
				this.treeSubMenu.setVisible(true);
			}
		} else if (arg0.widget instanceof Table && arg0.button == 3) {
			// check if right click menu of the material property tab can be shown
			Table thisTable = (Table) arg0.widget;
			Point point = new Point(arg0.x, arg0.y);
			TableItem item = thisTable.getItem(point);
			if (item != null) {
				this.propertySubMenu.setVisible(true);
			} else {
				this.categorySubMenu.setVisible(true);
			}
		}
	}
}
