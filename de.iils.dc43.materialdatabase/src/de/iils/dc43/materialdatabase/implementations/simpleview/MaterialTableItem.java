package de.iils.dc43.materialdatabase.implementations.simpleview;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class MaterialTableItem extends TableItem{
	private final UUID id;

	public MaterialTableItem(Table parent, int style, UUID id) {
		super(parent, style);
		this.id = id;
	}
	
	public MaterialTableItem(Table parent, int style, int index, UUID id) {
		super(parent, style, index);
		this.id = id;
	}

	public UUID getId() {
		return id;
	}
	
	@Override
	protected void checkSubclass() {
	    //  this overrides the check subclass method so subclassing of treeTtem element of java.swt is allowed
	}
	
	public boolean replaceElement(String oldValue, String newValue, int columnNumber){
		// takes an old value and replaces it in the table entry with the new value
		List<String> entries = new ArrayList<>();
		int counter = 0;
		while(counter<columnNumber) {
			entries.add(this.getText(counter));
			counter++;
		}
		this.setText(entries.indexOf(oldValue), newValue);
		return true;
	}

}
