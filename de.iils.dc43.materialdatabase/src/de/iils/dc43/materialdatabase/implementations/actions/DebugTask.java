package de.iils.dc43.materialdatabase.implementations.actions;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractAction;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractController;

public class DebugTask extends AbstractAction{

	public DebugTask(AbstractController controller) {
		super("Decision Window Test", controller);
	}

	@Override
	public void performTask() {
		int result = this.controller.showDecisionWindow
				("Super Important message", new String[] {"Yes","No","Cancel"});
		System.out.println(result);
	}

}
