package de.iils.dc43.materialdatabase.implementations.actions;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractAction;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractController;

public class DebugTask2 extends AbstractAction{

	public DebugTask2(AbstractController controller) {
		super("Debug task 2", controller);
	}

	@Override
	public void performTask() {
		System.out.println("Dies ist der zweite Task");
	}

}
