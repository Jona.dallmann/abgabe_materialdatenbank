package de.iils.dc43.materialdatabase.materialdatabasestarter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;

import de.iils.dc43.materialdatabase.abstractclasses.AbstractController;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractModel;
import de.iils.dc43.materialdatabase.abstractclasses.AbstractView;
import de.iils.dc43.materialdatabase.implementations.actions.DebugTask;
import de.iils.dc43.materialdatabase.implementations.actions.DebugTask2;
import de.iils.dc43.materialdatabase.implementations.datafilehandler.MatMlFileHandler;
import de.iils.dc43.materialdatabase.implementations.simplecontroller.SimpleController;
import de.iils.dc43.materialdatabase.implementations.simplemodel.SimpleModel;
import de.iils.dc43.materialdatabase.implementations.simpleview.SimpleView;

public class MaterialDataBaseStarter {
	// set up as singleton so the database can not be accessed parallel
	private static MaterialDataBaseStarter MaterialDataBaseStarterInstance;

	// instanciate database elements
	private static AbstractModel materialDatabaseModel;
	private static AbstractView materialDatabaseView;
	private static AbstractController materialDatabaseController;

	private MaterialDataBaseStarter() {
	}

	public synchronized static MaterialDataBaseStarter getInstance() {
		if (MaterialDataBaseStarterInstance == null) {
			// create material database starter and add the model view control of the
			// database and
			// configure them

			// create dataFileHandler and add Data files
			MatMlFileHandler protectedMatMlFileHandler = new MatMlFileHandler();
			MatMlFileHandler userDataFileMatMlHandler = new MatMlFileHandler();
			// set protected because this contains the standard data file
			protectedMatMlFileHandler.setProtectet();

			// get the path to the material database project
			IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			IProject currentProject = workspaceRoot.getProject("de.iils.dc43.materialdatabase");
			String pathToCurrentProject = currentProject.getLocation().toOSString();
			// FIXME: change to 43 system plugins folder

			// set xsd file path so the mat ml File manager can check the given xmls and
			// add the standard database
			protectedMatMlFileHandler.setXsdFile(pathToCurrentProject + "\\xmlFiles\\matmlDefinition.xsd");
			userDataFileMatMlHandler.setXsdFile(pathToCurrentProject + "\\xmlFiles\\matmlDefinition.xsd");
			protectedMatMlFileHandler.addFilePath(pathToCurrentProject + "\\dataFiles\\StandardDataFile.xml");
			userDataFileMatMlHandler.addFilePath(pathToCurrentProject + "\\dataFiles\\UserDataFile.xml");

			// creating model and adding data file handler to
			// to load and save date into the model
			AbstractModel model = new SimpleModel();
			model.addDataFileHandler(protectedMatMlFileHandler);
			model.addDataFileHandler(userDataFileMatMlHandler);

			// creating view and configuring
			AbstractView view = new SimpleView();

			// create controller and configure
			AbstractController controller = new SimpleController();
			controller.addModel(model);
			controller.addView(view);

			// add the controller to the view so the user inputs in the view can control the
			// model
			view.setController(controller);

			// set up tasks
			DebugTask debugTask = new DebugTask(controller);
			controller.addTask(debugTask);

			DebugTask2 debugTask2 = new DebugTask2(controller);
			controller.addTask(debugTask2);

			// add the created elements to the starter
			materialDatabaseModel = model;
			materialDatabaseView = view;
			materialDatabaseController = controller;

			MaterialDataBaseStarterInstance = new MaterialDataBaseStarter();
		}
		return MaterialDataBaseStarterInstance;
	}

	// methods to access the data of the material database
	public UUID selectAndGetMaterialInDatabase() {
		return materialDatabaseView.selectAndReturnMaterialIdFromGUI();
	}

	public List<UUID> selectAndGetMaterialAndProperties() {
		// this returns the material id as first id and the rest of the ids are the
		// corresponding properties of
		// the material, if no material was selected return an empty list
		List<UUID> materialIdAndItsPropertyIds = new ArrayList<>();

		// get the material id by opening the material database view and getting the
		// selected element
		UUID materialId = this.selectAndGetMaterialInDatabase();

		// check if material in material database was selected
		if (materialId != null) {
			// add material id to the return list first
			materialIdAndItsPropertyIds.add(materialId);

			List<UUID> materialPropertyIds = materialDatabaseController.getMaterialPropertyIds(materialId);
			materialIdAndItsPropertyIds.addAll(materialPropertyIds);
			return materialIdAndItsPropertyIds;
		}
		return materialIdAndItsPropertyIds;
	}

	public List<UUID> getPropertyIdsOfMaterial(UUID materialId) {
		// check if material in material database was selected
		if (materialId != null) {
			// get all the material properties id
			List<UUID> materialPropertyIds = materialDatabaseController.getMaterialPropertyIds(materialId);
			return materialPropertyIds;
		}else {
			return null;
		}
	}

	public String getNameOfModelElement(UUID modelElementId) {
		// gets the name of the element corresponding to the id
		// the name is "" if there is no name set for the element
		String name = (String) materialDatabaseController.getModelElementProperty(modelElementId, "Name");
		return name;
	}

	public Double getValueOfMaterialProperty(UUID materialPropertyId) {
		Object value = materialDatabaseController.getModelElementProperty(materialPropertyId, "Value");
		if (!value.equals("") && value != null) {
			return Double.valueOf(value.toString());
		}
		return null;
	}

	public String getCategoryNameOfMaterialProperty(UUID materialPropertyId) {
		return materialDatabaseController.getCategoryNameOfMaterialProperty(materialPropertyId);
	}

	public String getUnitofMaterialProperty(UUID materialPropertyId) {
		// this function takes the unit of the material property and parses it so the DC43 can read it
		// and use it properly
		String propertyUnit = materialDatabaseController.getUnitOfMaterialProperty(materialPropertyId);
		// convert unit string so it is readable for the dc43 since it does not
		// understand the format the database is delivering the
		// property unit
		String[] unitParts = propertyUnit.split("\\*");
		String convertedUnit = null;
		boolean bracket = false;
		for (int index = 0; index < unitParts.length; index++) {
			String unitPart = unitParts[index];
			// delete brackets
			unitPart = unitPart.replace("(", "");
			unitPart = unitPart.replace(")", "");
			// first iteration only needs to be set as the convertedUnitString
			if (index == 0) {
				convertedUnit = unitPart;
				continue;
			}

			// replace - sign by adding the element with / to the string and add brackets if
			// there is more than
			// one element with negative index
			if (unitPart.contains("-")) {
				// if the bracket is set and the current element is the last element
				// of the unitparts close the bracket
				if (bracket && index == unitParts.length - 1) {
					if(unitPart.contains("^-1")) {
						unitPart = unitPart.replace("^-1", "");
					}else {
						unitPart = unitPart.replace("-", "");
					}
					convertedUnit = convertedUnit + "*" + unitPart + ")";
					return convertedUnit;
				}
				// check if bracket is set
				if (bracket) {
					// check if the next element needs also an open bracket
					if (unitParts[index + 1].contains("-")) {
						if(unitPart.contains("^-1")) {
							unitPart = unitPart.replace("^-1", "");
						}else {
							unitPart = unitPart.replace("-", "");
						}
						convertedUnit = convertedUnit + "*" + unitPart;
					} else {
						// close bracket
						if(unitPart.contains("^-1")) {
							unitPart = unitPart.replace("^-1", "");
						}else {
							unitPart = unitPart.replace("-", "");
						}
						convertedUnit = convertedUnit + "*" + unitPart + ")";
						bracket = false;
					}
				} else {
					// check if next element exponent is also negative and therefore
					// a bracket is needed
					if (index < unitParts.length - 1 && unitParts[index + 1].contains("-")) {
						if(unitPart.contains("^-1")) {
							unitPart = unitPart.replace("^-1", "");
						}else {
							unitPart = unitPart.replace("-", "");
						}
						convertedUnit = convertedUnit + "/(" + unitPart;
						bracket = true;
					} else {
						if(unitPart.contains("^-1")) {
							unitPart = unitPart.replace("^-1", "");
						}else {
							unitPart = unitPart.replace("-", "");
						}
						convertedUnit = convertedUnit + "/" + unitPart;
					}
				}
			} else {
				// if there is no negative exponent
				// check if the exponent is one and replace it
				if(unitPart.contains("^1")) {
					unitPart.replace("^1", "");
				}
				convertedUnit = convertedUnit + "*" + unitPart;
			}

		}
		

		// case that the unit is unit-less
		if (convertedUnit.equals("")) {
			convertedUnit = "1";
		}
		return convertedUnit;
	}

	public String getFirstSubCategoryOfTheMaterial(UUID materialId) {
		// returns the name of the folder which the material is sorted into below the
		// dataFile
		return materialDatabaseController.getFirstSubCategoryNameOfTheMaterial(materialId);
	}
}
