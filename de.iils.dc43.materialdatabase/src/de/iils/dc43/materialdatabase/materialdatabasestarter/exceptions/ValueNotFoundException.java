package de.iils.dc43.materialdatabase.materialdatabasestarter.exceptions;

public class ValueNotFoundException extends Exception {
	public ValueNotFoundException(String ErrorMessage) {
		super(ErrorMessage);
	}
	
	public ValueNotFoundException(String ErrorMessage, Throwable err) {
		super(ErrorMessage,err);
	}
}
