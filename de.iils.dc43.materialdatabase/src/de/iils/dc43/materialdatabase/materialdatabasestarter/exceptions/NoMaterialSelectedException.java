package de.iils.dc43.materialdatabase.materialdatabasestarter.exceptions;

public class NoMaterialSelectedException extends Exception {

	public NoMaterialSelectedException(String ErrorMessage) {
		super(ErrorMessage);
	}
	
	public NoMaterialSelectedException(String ErrorMessage, Throwable err) {
		super(ErrorMessage,err);
	}
}
