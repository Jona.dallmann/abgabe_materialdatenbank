package de.iils.dc43.materialdatabase.materialdatabasestarter.exceptions;

public class PropertyNotFoundException extends Exception {
	public PropertyNotFoundException(String ErrorMessage) {
		super(ErrorMessage);
	}
	
	public PropertyNotFoundException(String ErrorMessage, Throwable err) {
		super(ErrorMessage,err);
	}
}
