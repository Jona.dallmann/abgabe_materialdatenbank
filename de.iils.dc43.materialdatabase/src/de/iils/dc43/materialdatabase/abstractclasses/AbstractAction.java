package de.iils.dc43.materialdatabase.abstractclasses;

public abstract class AbstractAction {
	protected AbstractController controller;
	private String name;
	
	public AbstractAction(String name, AbstractController controller) {
		this.controller = controller;
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public abstract void performTask();
}
