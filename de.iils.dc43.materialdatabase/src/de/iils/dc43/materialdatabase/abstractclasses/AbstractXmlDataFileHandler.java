package de.iils.dc43.materialdatabase.abstractclasses;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public abstract class AbstractXmlDataFileHandler extends AbstractDataFileHandler{
	// variables needed to open and read the xml file
	private DocumentBuilderFactory dbFactory;
	private DocumentBuilder dBuilder;

	// variables needed to validate the xml file against its scheme
	private SchemaFactory sFactory;
	private Schema schema;
	private Validator validator = null;
	private File xsdSchemeFile = null;
	
	@Override
	public void initialize() {
		// create the needed elements to read the xml file
		this.dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		try {
			this.dBuilder = this.dbFactory.newDocumentBuilder();
		} catch (Exception ex) {
			this.raiseError("Could not load MatML FileManager due to: " + ex.getMessage());
		}
		
		// check if a .xsd-file is given, which defines the structure of the xml file
		if(this.xsdSchemeFile != null) {
			// if a .xsd file is given, it is checked whether the file exists or not
			if(this.xsdSchemeFile.exists()) {
				// create elements to validate the XML against its xsd
				this.sFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
				try {
					this.schema = sFactory.newSchema(this.xsdSchemeFile);
					this.validator = schema.newValidator();
				} catch (Exception ex) {
					this.raiseError(
							"Could not load XML DataFileManager because of error"
							+ " in xsd definition/file: " + ex.getMessage()+
							"\nLoading corresponding files skipped.");
					return;
				}
			}else {
				this.raiseError("Given .xsd File in path '"+this.xsdSchemeFile.getAbsolutePath().toString()
						+"' does not exist.\nXmlFile handler of type '"+this.getClass().toString()
						+"' can not be used.\nLoading corresponding files skipped.");
				return;
			}
		}

		// load the data files in data file path
		super.initialize();
	}
	
	// functions needed when using an .xsd file to validate the xml
	public void setXsdFile(String pathToXsdFile) {
		this.xsdSchemeFile = new File(pathToXsdFile);
	}
	
	protected void validateSubtreeNodeAgainstXsd(Node subtreeRootNode) throws SAXException, IOException {
		if(this.validator != null) {
			// Create document to validate against
			Document xmlDoc = this.dBuilder.newDocument();
			Source source = new DOMSource(xmlDoc);
			Node newNode = xmlDoc.importNode(subtreeRootNode, true);
			xmlDoc.appendChild(newNode);
		
			// validate matmlNode
			this.validator.validate(source);
		}else {
			this.raiseError("Could not validate "+subtreeRootNode.getNodeName()+" against xsd file because no xsd file is given");
		}
	}
	
	// xml related useful Methods which can be used during the xml reading/writing
	
	protected Document getNewDocument() {
		return this.dBuilder.newDocument();
	}
	
	protected Node getXmlRootNode(Path dataFilePath) throws SAXException, IOException {
		// open XML File
		File inputFile = dataFilePath.toFile();
		Document doc = this.dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		
		// get root element
		Node rootElement = doc.getDocumentElement();
		
		return rootElement;
	}
	
	protected List<Node> getOnlyChildNodes(Node node) {
		// iterates through the children of an xml node (not its sub children)
		// and returns a list of the children which are of type ELEMENT_NODE
		NodeList childNodes = node.getChildNodes();
		List<Node> childNodesToReturn = new ArrayList<>();
		for (int index = 0; index < childNodes.getLength(); index++) {
			Node childNode = childNodes.item(index);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				childNodesToReturn.add(childNode);
			}
		}
		return childNodesToReturn;
	}
	
	protected List<Node> getAllChildNodesWithName(Node parentNode, String childNodeName) {
		// takes a node and iterates through all the child nodes (not sub child nodes)
		// and takes the nodes with the node name equals childNodeName
		// and returns them as list
		NodeList childNodeList = parentNode.getChildNodes();
		List<Node> foundNodes = new ArrayList<>();
		for (int index = 0; index < childNodeList.getLength(); index++) {
			Node currentChildNode = childNodeList.item(index);
			if (currentChildNode.getNodeName().equals(childNodeName)
					&& currentChildNode.getNodeType() == Node.ELEMENT_NODE) {
				foundNodes.add(currentChildNode);
			}
		}
		return foundNodes;
	}
	
	protected Node getFirstChildNodeWithName(Node parentNode, String childNodeName) {
		// takes a node and iterates through all the child nodes until a
		// node with the given name is found and returns this node
		// else returns null
		NodeList childNodeList = parentNode.getChildNodes();
		for (int index = 0; index < childNodeList.getLength(); index++) {
			Node currentChildNode = childNodeList.item(index);
			if (currentChildNode.getNodeName().equals(childNodeName)) {
				return currentChildNode;
			}
		}
		return null;
	}
	
	protected void saveXmlFile(Document document, Path dataFilePath) throws TransformerException {
		// use transform engine to create XML file for the dataFile
		File inputFile = dataFilePath.toFile();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(inputFile);
        transformer.transform(domSource, streamResult);
	}
	
	protected String getXsdSchemeFilePath() {
		// returns the scheme file path to check
		// if there is an .xsd file set
		if(this.xsdSchemeFile != null) {
			return this.xsdSchemeFile.toPath().toString();
		}else {
			return null;
		}
	}
}
