package de.iils.dc43.materialdatabase.abstractclasses;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Random;
import java.util.UUID;

public abstract class AbstractGraphElement implements Comparable<AbstractGraphElement> {
	protected PropertyChangeSupport propertyChangeSupport;
	private final String type;
	private final UUID id;
	private String name;
	private UUID parentId;
	private String note = "";
	private boolean dirty = false;

	public AbstractGraphElement(String type, String name) {
		this.id = this.createUuidWithLetterAtEnd();
		this.type = type;
		this.setName(name);
		propertyChangeSupport = new PropertyChangeSupport(this);
	}

	public AbstractGraphElement(String type, String name, UUID id) {
		// this functions helps to keep the UUID static
		// during saving the database so every element can be accessed with
		// this id in the saved Database
		this.id = id;
		this.type = type;
		this.setName(name);
		propertyChangeSupport = new PropertyChangeSupport(this);
	}

	public UUID getId() {
		return this.id;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		String oldName = this.getName();
		this.name = name;
		this.firePropertyChange("name", oldName, name);
	}

	public UUID getParentId() {
		return parentId;
	}

	public void setParentId(UUID parentId) {
		this.parentId = parentId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		String oldNote = this.getNote();
		this.note = note;
		this.firePropertyChange("note", oldNote, note);
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		// this prevents firing event, when the element is not in the graph and
		// therefore
		// no property change tracking is needed
		if (this.propertyChangeSupport != null) {
			propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
			// set element dirty so it can be better chosen which element to save
			this.setDirty(true);
		}
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.propertyChangeSupport.addPropertyChangeListener(listener);
	}

	@Override
	// add compare to to use java collections to sort the objects due to their names
	// alphabetically
	public int compareTo(AbstractGraphElement element) {
		return this.getName().compareTo(element.getName());
	}

	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}
	
	private UUID createUuidWithLetterAtEnd() {
		// set an id with the condition that the first char has to be a letter
		// this is needed s othe matml data file handler
		// can set the id wich is defined as starting with a letter
		// in the mat ml xsd
		String materialUuidString = UUID.randomUUID().toString();

		// create random char form a-f (since uuid is hexadecimal)
		// and replace first UUID char
		Random rnd = new Random();
		char c = (char) ('a' + rnd.nextInt(6));
		materialUuidString = (String) materialUuidString.subSequence(1, materialUuidString.length() - 1);
		materialUuidString = c + materialUuidString;

		// transform the UUID from String back to UUID class object
		UUID materialUuid = UUID.fromString(materialUuidString);
		return materialUuid;
	}
}
