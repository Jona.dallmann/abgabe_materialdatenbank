package de.iils.dc43.materialdatabase.abstractclasses;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.management.InstanceNotFoundException;

import de.iils.dc43.materialdatabase.implementations.modelelements.DataFile;

public abstract class AbstractModel{
	protected PropertyChangeSupport propertyChangeSupport;
	protected List<AbstractDataFileHandler> addedFileHandler = new ArrayList<>();
	private AbstractController controller;
	
	abstract public void saveModel() throws InstanceNotFoundException, Exception;
	abstract public void addElement(AbstractGraphElement element, UUID parentId);
	abstract public void removeElement(UUID id);
	abstract public void removeElement(AbstractGraphElement element);
	abstract public boolean setElementProperty(UUID elementId, String propertyName, Object newValue);
	abstract public Object getElementProperty(UUID elementId, String property);
	abstract public UUID getElementParentId(UUID id);
	abstract public void moveElement(UUID elementId, UUID newParentId);
	abstract public String getElementName(UUID id);
	abstract public String getElementType(UUID id);
	abstract public String getElementNote(UUID id);
	abstract public UUID[] getElementChildIds(UUID id);	
	abstract public String getParentDataFilePath(UUID id);
	abstract public UUID getRootElementId();
	abstract public UUID getElementIdByPath(List<String> path);
	abstract public List<String> getPathByElementId(UUID elementId);
	abstract public AbstractGraphElement getElementById(UUID id);
	abstract public DataFile getDatafileContainingElement(AbstractGraphElement element);
	abstract public DataFile getDatafileContainingElement(UUID elementId);
	abstract public boolean isElementChildOf(UUID elementId, UUID supposedParentElementId);
	abstract public void clearModel();
	
	public AbstractModel()
    {
        propertyChangeSupport = new PropertyChangeSupport(this);
    }
	
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    public PropertyChangeListener[] getPropertyChangeListeners() {
    	return propertyChangeSupport.getPropertyChangeListeners();
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }
    
	public void addDataFileHandler(AbstractDataFileHandler dataFileHandler) {
		this.addedFileHandler.add(dataFileHandler);
		dataFileHandler.setModel(this);
	}
	
	public void removeDataFileHandler(AbstractDataFileHandler dataFileHandler, boolean saveBeforeDelete) throws InstanceNotFoundException, Exception {
		if (saveBeforeDelete) {
			dataFileHandler.saveAllDataFiles();
		}
		this.addedFileHandler.remove(dataFileHandler);
	}
	
	public void initialize(AbstractController controller) {
		this.controller = controller;
	}
	
	public void raiseError(String errorMessage) {
		this.controller.raiseError(errorMessage);
	}
	
	public int showDecisionWindow(String description, String[] options) {
		return this.controller.showDecisionWindow(description, options);
	}
}
