package de.iils.dc43.materialdatabase.abstractclasses;

import java.beans.PropertyChangeEvent;
import java.util.UUID;

public abstract class AbstractView {
	protected AbstractController controller;
	protected boolean startupFlag = false;
	
	public void setController(AbstractController controller) {
		this.controller = controller;
	}
	
	public void setStartupFlag(boolean value) {
		this.startupFlag = value;
	}

	public abstract void initialize();
	public abstract void show();
	public abstract UUID selectAndReturnMaterialIdFromGUI();
	public abstract void addTask(String taskName);
	public abstract void modelPropertyChange(PropertyChangeEvent evt);
	public abstract void raiseError(String description);
	public abstract int showDecisionWindow(String description, String[] options);
}
