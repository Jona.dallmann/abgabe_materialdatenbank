package de.iils.dc43.materialdatabase.abstractclasses;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.management.InstanceNotFoundException;

public abstract class AbstractController implements PropertyChangeListener{
    protected AbstractView registeredView;
    protected AbstractModel registeredModel;
    protected ArrayList<AbstractAction> registeredTasks = new ArrayList<>();
    
    public void addModel(AbstractModel model) {
        this.registeredModel = model;
        model.addPropertyChangeListener(this);
    }

    public void removeModel(AbstractModel model) {
        this.registeredModel = null;
        model.removePropertyChangeListener(this);
    }

    public void addView(AbstractView view) {
        this.registeredView = view;
    }

    public void removeView(AbstractView view) {
        this.registeredView= null;
    }
    
    public void addTask(AbstractAction task) {
    	this.registeredTasks.add(task);
    }
    
    public void removeTask(AbstractAction task) {
    	this.registeredTasks.remove(task);
    }
    
	public void initialize() {
		// this startup flag suppresses errors when there could not be
		// specific elements deleted in the view due to a reload process
		// of the model
		this.registeredView.setStartupFlag(true);
		this.registeredModel.initialize(this);
		for(AbstractAction task: this.registeredTasks) {
			this.registeredView.addTask(task.getName());
		}
		this.registeredView.setStartupFlag(false);
	}
    
    public String getParentDataFilePath(UUID id) {
    	return this.registeredModel.getParentDataFilePath(id);
    }
    
    public void moveElement(UUID elementId, UUID parentId) {
    	//changes the parent of an element and therefore moves it
    	this.registeredModel.moveElement(elementId, parentId);
    }
    
    public UUID[] getModelElementChildIds(UUID elementId) {
    	return this.registeredModel.getElementChildIds(elementId);
    }
    
    public boolean isElementChildOf(UUID elementId, UUID supposedParentElementId) {
    	return this.registeredModel.isElementChildOf(elementId, supposedParentElementId);
    }
    
    public void saveModel() throws InstanceNotFoundException, Exception {
    	this.registeredModel.saveModel();
    }
   
	// callable functions
	public abstract void addMaterial(UUID parentId);
	public abstract void addMaterialWithGeneralCategory(UUID parentId);
	public abstract void addFolder(UUID parentId);
	public abstract void addMaterialProperty(UUID parentId);
	public abstract void addMaterialPropertyCategory(UUID parentId);
	public abstract void runTask(String name);
	public abstract void raiseError(String description);
	public abstract int showDecisionWindow(String description, String[] options);
	public abstract void removeElement(UUID id);
	public abstract void clearModel();
	
	// function written for the dc43 interface to use
	public abstract String getCategoryNameOfMaterialProperty(UUID materialPropertyId) throws IllegalArgumentException;
	public abstract String getUnitOfMaterialProperty(UUID materialPropertyId) throws IllegalArgumentException;
	public abstract List<UUID> getMaterialPropertyIds(UUID materialId);
	public abstract String getFirstSubCategoryNameOfTheMaterial(UUID materialId);
	
	@Override
	public abstract void propertyChange(PropertyChangeEvent evt);
	
    public abstract void setModelElementProperty(UUID elementId, String propertyName, Object newValue);
    public abstract Object getModelElementProperty(UUID elementId, String propertyName);
    public abstract AbstractGraphElement getDatafileContainingElement(AbstractGraphElement element);
}