package de.iils.dc43.materialdatabase.abstractclasses;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.lang.model.element.UnknownElementException;
import javax.management.InstanceNotFoundException;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.jface.resource.DataFormatException;
import org.xml.sax.SAXException;

import de.iils.dc43.materialdatabase.implementations.modelelements.DataFile;
import de.iils.dc43.materialdatabase.implementations.modelelements.Folder;
import de.iils.dc43.materialdatabase.implementations.modelelements.Material;
import de.iils.dc43.materialdatabase.implementations.modelelements.MaterialProperty;
import de.iils.dc43.materialdatabase.implementations.modelelements.MaterialPropertyCategory;

import de.iils.dc43.materialdatabase.implementations.simplemodel.ElementDataExchangeClass;

public abstract class AbstractDataFileHandler {
	private List<Path> managedDataFilePaths = new ArrayList<>();
	private AbstractModel model;
	private boolean protectionStatus = false;

	// functions for specific implementation of the dataFileHandler for the specific
	// data format
	protected abstract List<ElementDataExchangeClass> fileToElementList(Path dataFilePath)
			throws DataFormatException, ParserConfigurationException, SAXException, IOException;

	public abstract void elementListToFile(Path dataFilePath, List<ElementDataExchangeClass> elements)
			throws InstanceNotFoundException, Exception;
	
	// function regarding the protection of the datafileHandler
	public void setProtectet() {
		this.protectionStatus = true;
	}
	
	public boolean isProtected() {
		return this.protectionStatus;
	}

	// functions for loading the data files
	public void addFilePath(Path filePath) {
		this.managedDataFilePaths.add(filePath);
	}

	public void addFilePath(String filePath) {
		Path path = Paths.get(filePath);
		this.addFilePath(path);
	}

	public void addFilePathAndLoadDatafile(String filePath) {
		this.addFilePathAndLoadDatafile(Paths.get(filePath));
	}

	public void addFilePathAndLoadDatafile(Path filePath) {
		this.addFilePath(filePath);
		this.loadDataFile(filePath);
	}

	public void removeFilePath(String filePath) {
		this.removeFilePath(Paths.get(filePath));
	}

	public void removeFilePath(Path path) {
		if (this.managedDataFilePaths.contains(path)) {
			this.managedDataFilePaths.remove(path);
		}
	}

	public List<Path> getDataFilePaths() {
		return managedDataFilePaths;
	}

	protected void raiseError(String errorMessage) {
		this.model.raiseError(errorMessage);
	}
	
	public void setModel(AbstractModel model) {
		this.model = model;
	}

	protected int showDecisionWindow(String description, String[] options) {
		// triggers the decision window in the view
		return this.model.showDecisionWindow(description, options);
	}

	public void initialize() {
		// loading all data files into the model
		for (Path dataFilePath : this.getDataFilePaths()) {
			this.loadDataFile(dataFilePath);
		}
	}
	
	public boolean hasElementChildInModel(UUID id) {
		UUID[] childIds = this.model.getElementChildIds(id);
		if(childIds.length >0) {
			return true;
		}else {
			return false;
		}
	}

	public void loadDataFile(Path dataFilePath) {
		List<ElementDataExchangeClass> dataFileElements = null;
		if (Files.exists(dataFilePath)) {
			// get all elements in the data file, sort them due to path length
			// and add them to the model. This is important so there are no elements
			// added to the graph with parents which haven't been created
			try {
				dataFileElements = this.fileToElementList(dataFilePath);

				// sort elements due to path length so the elements
				// are created in the
				Collections.sort(dataFileElements);
				
				// save elements uuids for later
				List<UUID> elementIds = new ArrayList<>();
				// add elements to model
				for (ElementDataExchangeClass element : dataFileElements) {
					UUID elementId = this.addElementToModel(element, dataFilePath);
					elementIds.add(elementId);
				}
				if(this.protectionStatus) {
					// set data file protected if the data file handler is protected
					DataFile correspondingDataFile = this.model.getDatafileContainingElement(elementIds.get(0));
					if(correspondingDataFile != null) {
						correspondingDataFile.setProtected();
					}
				}
				
			} catch (Exception e) {
				this.raiseError("Error Occured while loading '" + dataFilePath.toString() + "'\n" + e.getMessage());
				return;
			}
		} else {
			this.model.raiseError("File: '" + dataFilePath.toString() + "' does not exist.\nSkip loading...");
			return;
		}
		

	}

	private UUID addElementToModel(ElementDataExchangeClass element, Path dataFilePath) throws Exception {
		// this function takes a data element and places it at the tree

		// get the type of the element and its path
		String elementType = element.getType();
		List<String> path = element.getPathToParentElement();

		// get parent element in the graph
		UUID parentElementId = null;
		if (path.size() == 0) {
			parentElementId = this.model.getRootElementId();
		} else {
			parentElementId = this.model.getElementIdByPath(path);
		}

		// iterate through all data values and extract them and sort them
		// to the corresponding abstract graph element
		Map<String, Object> dataValuesMap = element.getAllDataValues();

		// placeholder for the new element
		AbstractGraphElement newElement = null;

		// preinitialize values
		String elementName = "";
		String elementNote = "";
		UUID elementId = null;
		String elementValueUnit = "";
		String elementlookUpTableUnit = "";
		Object elementValue = null;

		for (Entry<String, Object> dataValueEntry : dataValuesMap.entrySet()) {
			String dataValueName = dataValueEntry.getKey();
			Object dataValueObject = dataValueEntry.getValue();

			// check data values for their name and convert them accordingly
			if (dataValueName.equals("name") && dataValueObject instanceof String) {
				elementName = dataValueObject.toString();
			} else if (dataValueName.equals("note") && dataValueObject instanceof String) {
				elementNote = dataValueObject.toString();
			} else if (dataValueName.equals("id")) {
				if (dataValueObject instanceof UUID) {
					elementId = (UUID) dataValueObject;
				} else if (dataValueObject instanceof String) {
					try {
						elementId = UUID.fromString((String) dataValueObject);
					} catch (IllegalArgumentException ex) {
						// Simply catch and continue
					}
				}

			} else if (dataValueName.equals("unit") && dataValueObject instanceof String) {
				elementValueUnit = dataValueObject.toString();
			} else if (dataValueName.equals("lookUpTableUnit") && dataValueObject instanceof String) {
				elementlookUpTableUnit = dataValueObject.toString();
			} else if (dataValueName.equals("value") && dataValueObject instanceof Double
					|| dataValueObject instanceof Map) {
				elementValue = dataValueObject;
			} else {
				throw new UnknownElementException(null, "unknown element when transforming");
			}
		}

		// check for element type and set accordingly
		if (elementType.equals("dataFile")) {
			newElement = new DataFile(dataFilePath, elementName);
		} else if (elementType.equals("folder")) {
			newElement = new Folder(elementName);
		} else if (elementType.equals("category")) {
			newElement = new MaterialPropertyCategory(elementName);
		} else if (elementType.equals("material")) {
			// the case gets catched that the element has already
			// a id which helps tracking the element in the DC43 when
			// only the UUID is known. The below "if" ensures
			// that the UUID is the same, even when the Material
			// database was closed
			if (elementId != null) {
				newElement = new Material(elementName, elementId);
			} else {
				newElement = new Material(elementName);
			}

		} else if (elementType.equals("property")) {
			// preinitialize element reference
			MaterialProperty newPropertyElement;

			// set value depending on its type to the materialProperty
			if (elementValue instanceof Double) {
				double valueKonstant = Double.valueOf(elementValue.toString());
				if (elementId != null) {
					newPropertyElement = new MaterialProperty(elementName, valueKonstant, elementValueUnit, elementId);
				} else {
					newPropertyElement = new MaterialProperty(elementName, valueKonstant, elementValueUnit);
				}
				newPropertyElement.setLookUpTableUnit(elementlookUpTableUnit);
			} else if (elementValue instanceof Map) {
				Map<Double, Double> valueMap = (Map<Double, Double>) elementValue;
				if(elementId != null) {
					newPropertyElement = new MaterialProperty(elementName, valueMap, elementValueUnit, elementId);
				}else {
					newPropertyElement = new MaterialProperty(elementName, valueMap, elementValueUnit);
				}
				newPropertyElement.setLookUpTableUnit(elementlookUpTableUnit);
			} else {
				throw new Exception("unknown value element when transforming");
			}

			// set newpropertyElement to the newElement
			newElement = newPropertyElement;

		} else {
			throw new Exception(
					"unknown element type named '" + elementType + "' in ElementDataExchangeClass when transforming");
		}

		// add the missing properties like note etc to the element
		newElement.setNote(elementNote);

		// add this element to the model so it get included in the material database
		this.model.addElement(newElement, parentElementId);
		
		// return UUID of the created element
		return newElement.getId();
	}

	public void saveAllDataFiles() throws InstanceNotFoundException, Exception {
		// saves all the data files belonging to one AbstractDataFileHandler
		for (Path dataFilePath : this.getDataFilePaths()) {
			this.saveDataFile(dataFilePath);
		}
	}

	public void saveDataFile(Path dataFilePath) throws InstanceNotFoundException, Exception {
		// this is an extra function so data files can be saved individually
		// selecting the corresponding data file in the model and
		// getting the list of elements which should be saved and processing them
		UUID[] dataFileIds = this.model.getElementChildIds(this.model.getRootElementId());
		for (UUID dataFileId : dataFileIds) {
			Path path = (Path) this.model.getElementProperty(dataFileId, "FilePath");
			if (dataFilePath.equals(path)) {
				List<ElementDataExchangeClass> modelElements = this.createElementListFromModel(dataFileId);

				// save the elment list in the file (this is where the
				// specific data file handler docks onto)
				this.elementListToFile(dataFilePath, modelElements);
			}
		}
	}

	private List<ElementDataExchangeClass> createElementListFromModel(UUID dataFileId) {
		// creates a list of ElementDataExchangeClass from the given Datafile
		List<ElementDataExchangeClass> elementsToSave = new ArrayList<>();
		List<UUID> unprocessedElements = new ArrayList<>();
		unprocessedElements.add(dataFileId);
		while (unprocessedElements.size() != 0) {
			// get element id and add element children to
			UUID currentElementId = unprocessedElements.remove(0);
			UUID[] childElements = this.model.getElementChildIds(currentElementId);

			unprocessedElements.addAll(Arrays.asList(childElements));

			elementsToSave.add(this.createExchangeElementFromGraphElementById(currentElementId));
		}
		// sort elements to ensure that top level elements are first in the list
		Collections.sort(elementsToSave);
		return elementsToSave;
	}

	private ElementDataExchangeClass createExchangeElementFromGraphElementById(UUID graphElementId) {
		// gets a UUID from a graph element and returns the corresponding
		// ElementDataExchangeCalss element for it
		String type = this.model.getElementType(graphElementId);
		List<String> path = this.model.getPathByElementId(graphElementId);
		ElementDataExchangeClass dataExchangeElement = new ElementDataExchangeClass(type, path);

		// add properties to the data exchange element
		String name = this.model.getElementName(graphElementId);
		dataExchangeElement.addDataValue("name", name);

		String note = this.model.getElementNote(graphElementId);
		if (note != null) {
			dataExchangeElement.addDataValue("note", note);
		}

		dataExchangeElement.addDataValue("id", graphElementId);

		// add more specific properties if element is property
		if (type.equals("property")) {
			String unit = (String) this.model.getElementProperty(graphElementId, "Unit");
			dataExchangeElement.addDataValue("unit", unit);

			Object propertyValueObject = this.model.getElementProperty(graphElementId, "ValueObject");
			dataExchangeElement.addDataValue("value", propertyValueObject);

			Object propertyLutUnit = this.model.getElementProperty(graphElementId, "LookUpTableUnit");
			dataExchangeElement.addDataValue("lookUpTableUnit", propertyLutUnit);
		}
		return dataExchangeElement;
	}
}
