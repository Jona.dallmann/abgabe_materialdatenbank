package de.iils.dc43.materialdatabase.rules;

import de.*;
import de.iils.*;
import de.iils.dc43.*;
import de.iils.dc43.core.JavaRule;
import de.iils.dc43.core.graph.IDC43Graph;
import de.iils.dc43.materialdatabase.*;

import static tec.uom.se.quantity.Quantities.*;
import static de.iils.dc43.core.util.DC43Util.prettyPrint;

// !!!!for integration into dc43!!!!
// this is the execution of the getMaterialPropertiesFromDatabase() method
// which should be added in the rule as a slot when the attribute table
// is filled out 

@SuppressWarnings("all")
public class setProperties extends JavaRule {

	@Override
	public void execute() throws Exception {
		// get the dc43 graph
		IDC43Graph graph = getGraph();
		
		// get the material
		Dc43Material selectedMaterial = graph.firstInstance(Dc43Material.class);
		
		// execute the get material properties rule
		selectedMaterial.getMaterialPropertiesFromDatabase();
	}

}