package de.iils.dc43.materialdatabase.rules;

import de.*;
import de.iils.*;
import de.iils.dc43.*;
import de.iils.dc43.core.JavaRule;
import de.iils.dc43.core.graph.Annotation;
import de.iils.dc43.core.graph.IDC43Graph;
import de.iils.dc43.materialdatabase.Dc43GeneralProperty;
import de.iils.dc43.materialdatabase.Dc43Material;
import de.iils.dc43.materialdatabase.Dc43MaterialProperty;
import de.iils.dc43.materialdatabase.Dc43MechanicalProperty;
import de.iils.dc43.materialdatabase.Dc43Metal;
import de.iils.dc43.materialdatabase.Dc43OpticalProperty;
import de.iils.dc43.materialdatabase.Dc43Plastic;
import de.iils.dc43.materialdatabase.Dc43PredefinedMaterial;
import de.iils.dc43.materialdatabase.Dc43ThermalProperty;
import de.iils.dc43.materialdatabase.materialdatabasestarter.*;
import de.iils.dc43.materialdatabase.materialdatabasestarter.exceptions.NoMaterialSelectedException;
import de.iils.dc43.materialdatabase.materialdatabasestarter.exceptions.PropertyNotFoundException;
import de.iils.dc43.materialdatabase.materialdatabasestarter.exceptions.ValueNotFoundException;

import static tec.uom.se.quantity.Quantities.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import static de.iils.dc43.core.util.DC43Util.prettyPrint;

@SuppressWarnings("all")
public class rightClickSimulate extends JavaRule {

	@Override
	public void execute() throws Exception {
		// get the dc43 graph
		IDC43Graph graph = getGraph();
		
		// !!! for integration into dc43!!!
		// instead of "firstInstance(Dc43Material.class)" the class instance
		// on which has been right clicked has to be used here
		
		// get the material
		Dc43Material selectedMaterial = graph.firstInstance(Dc43Material.class);
		
		// get the material Database starter
		MaterialDataBaseStarter starter = MaterialDataBaseStarter.getInstance();
		
		// get selected material and its property Ids from the gui
		// this opens the gui
		List<UUID> materialAndItsPropertiesIdList = starter.selectAndGetMaterialAndProperties();
		
		// check if a material was selected and throw an exception if none was selected
		if(materialAndItsPropertiesIdList.size() == 0) {
			throw new NoMaterialSelectedException("No material selected in the gui");
		}
		
		// get the material id
		UUID materialId = materialAndItsPropertiesIdList.remove(0);
		
		// get the properties and put them with their names into a map
		Map<String,UUID> propertyNameToId = new HashMap<>();
		for(UUID materialPropertyId:materialAndItsPropertiesIdList) {
			String propertyName = starter.getNameOfModelElement(materialPropertyId);
			propertyNameToId.put(propertyName, materialPropertyId);
		}
		
		if(selectedMaterial instanceof Dc43Material){
			// set the id of the material
			selectedMaterial.setMaterialDatabaseId(materialId);
			
			// set the name of the material
			
			String materialName = starter.getNameOfModelElement(materialId);
			selectedMaterial.setName(materialName);
			
			// execute function to add properties this is always executed
			//selectedMaterial.getMaterialPropertiesFromDatabase(selectedMaterial.getMaterialDatabaseId());
		}
		
		if(selectedMaterial instanceof Dc43PredefinedMaterial) {
			// if the selected element is of type Dc43predefinedMaterial set the attributes predefined in this class
			Dc43PredefinedMaterial selectedPredefinedMaterial = (Dc43PredefinedMaterial) selectedMaterial;
			
			// set the density attribute
			UUID densityId = propertyNameToId.get("Density");
			Double densityValue = starter.getValueOfMaterialProperty(densityId);
			String densityUnit = starter.getUnitofMaterialProperty(densityId);
			selectedPredefinedMaterial.setDensity(getQuantity(densityValue.doubleValue(), densityUnit));
		}
		
		// based on the class/subclass of the material add properties
		if(selectedMaterial instanceof Dc43Metal) {
			// check if the material first sub category in the database (eg the top folder it is sorted in)
			// is metal
			String materialCategory = starter.getFirstSubCategoryOfTheMaterial(materialId);
			if(materialCategory.equals("Metal")) {
				Dc43Metal selectedMetalMaterial = (Dc43Metal) selectedMaterial;
				
				// set the predefined attributes in the Dc43Metal class
				// tensile Strength
				UUID tensileStrengthId = propertyNameToId.get("Tensile Strength");
				// check if the property exists
				this.propertyExistanceCheck(tensileStrengthId, "Tensile Strength", selectedMetalMaterial);
				Double tensileStrengthValue = starter.getValueOfMaterialProperty(tensileStrengthId);
				String tensileStrengthUnit = starter.getUnitofMaterialProperty(tensileStrengthId);
				selectedMetalMaterial.setTensileStrength(getQuantity(tensileStrengthValue.doubleValue(), tensileStrengthUnit));
				
				// modulus of elasticity
				UUID modulusOfElasticityId = propertyNameToId.get("Modulus of Elasticity");
				this.propertyExistanceCheck(modulusOfElasticityId, "Modulus of Elasticity", selectedMetalMaterial);
				Double modulusOfElasticityValue = starter.getValueOfMaterialProperty(modulusOfElasticityId);
				String modulusOfElasticityUnit = starter.getUnitofMaterialProperty(modulusOfElasticityId);
				selectedMetalMaterial.setModulusOfElasticity(getQuantity(modulusOfElasticityValue.doubleValue(), modulusOfElasticityUnit));

				// poissons ratio
				UUID poissonsRatioId = propertyNameToId.get("Poissons Ratio");
				this.propertyExistanceCheck(poissonsRatioId, "Poissons Ratio", selectedMetalMaterial);
				Double poissonsRatioValue = starter.getValueOfMaterialProperty(poissonsRatioId);
				// this is unitless therefore no unit is set
				selectedMetalMaterial.setPoissonsRatio(poissonsRatioValue);
				
				// shear modulus
				UUID shearModulusId = propertyNameToId.get("Shear Modulus");
				this.propertyExistanceCheck(shearModulusId, "Shear Modulus", selectedMetalMaterial);
				Double shearModulusValue = starter.getValueOfMaterialProperty(shearModulusId);
				String shearModulusUnit = starter.getUnitofMaterialProperty(shearModulusId);
				selectedMetalMaterial.setShearModulus(getQuantity(shearModulusValue.doubleValue(), shearModulusUnit));
				
				// electrical Resistivity
				UUID electricalResistivityId = propertyNameToId.get("Electrical Resistivity");
				this.propertyExistanceCheck(electricalResistivityId, "Electrical Resistivity", selectedMetalMaterial);
				Double electricalResistivityValue = starter.getValueOfMaterialProperty(electricalResistivityId);
				String electricalResistivityUnit = starter.getUnitofMaterialProperty(electricalResistivityId);
				selectedMetalMaterial.setElectricalResistivity(getQuantity(electricalResistivityValue.doubleValue(), electricalResistivityUnit));
				
				// specific heat capacity
				UUID specificHeatCapacityId = propertyNameToId.get("Specific Heat Capacity");
				this.propertyExistanceCheck(specificHeatCapacityId, "Specific Heat Capacity", selectedMetalMaterial);
				Double specificHeatCapacityValue = starter.getValueOfMaterialProperty(specificHeatCapacityId);
				String specificHeatCapacityUnit = starter.getUnitofMaterialProperty(specificHeatCapacityId);
				selectedMetalMaterial.setSpecificHeatCapacity(getQuantity(specificHeatCapacityValue.doubleValue(), specificHeatCapacityUnit));
	
				// thermal conductivity
				UUID thermalConductivityId = propertyNameToId.get("Thermal Conductivity");
				this.propertyExistanceCheck(thermalConductivityId, "Thermal Conductivity", selectedMetalMaterial);
				Double thermalConductivityValue = starter.getValueOfMaterialProperty(thermalConductivityId);
				String thermalConductivityUnit = starter.getUnitofMaterialProperty(thermalConductivityId);
				selectedMetalMaterial.setThermalConductivity(getQuantity(thermalConductivityValue.doubleValue(), thermalConductivityUnit));
	
			}
		}else if (selectedMaterial instanceof Dc43Plastic) {
			String materialCategory = starter.getFirstSubCategoryOfTheMaterial(materialId);
			if(materialCategory.equals("Plastic")) {
				Dc43Plastic selectedPlasticMaterial = (Dc43Plastic) selectedMaterial;
				// set the predefined attributes in the Dc43Plastic class
				// tensile Strength
				UUID tensileStrengthId = propertyNameToId.get("Tensile Strength");
				// check if the property exists
				this.propertyExistanceCheck(tensileStrengthId, "Tensile Strength", selectedPlasticMaterial);
				Double tensileStrengthValue = starter.getValueOfMaterialProperty(tensileStrengthId);
				String tensileStrengthUnit = starter.getUnitofMaterialProperty(tensileStrengthId);
				selectedPlasticMaterial.setTensileStrength(getQuantity(tensileStrengthValue.doubleValue(), tensileStrengthUnit));
				
				// flexural Strength
				UUID flexuralStrengthId = propertyNameToId.get("Flexural Strength");
				// check if the property exists
				this.propertyExistanceCheck(tensileStrengthId, "Flexural Strength", selectedPlasticMaterial);
				Double flexuralStrengthValue = starter.getValueOfMaterialProperty(flexuralStrengthId);
				String flexuralStrengthUnit = starter.getUnitofMaterialProperty(flexuralStrengthId);
				selectedPlasticMaterial.setFlexuralStrength(getQuantity(flexuralStrengthValue.doubleValue(), flexuralStrengthUnit));
				
				// flexural modulus
				UUID flexuralModulusId = propertyNameToId.get("Flexural Strength");
				// check if the property exists
				this.propertyExistanceCheck(flexuralModulusId, "Flexural Strength", selectedPlasticMaterial);
				Double flexuralModulusValue = starter.getValueOfMaterialProperty(flexuralModulusId);
				String flexuralModulusUnit = starter.getUnitofMaterialProperty(flexuralModulusId);
				selectedPlasticMaterial.setFlexuralModulus(getQuantity(flexuralModulusValue.doubleValue(), flexuralModulusUnit));
				
			}
		}
	}
	
	private void propertyExistanceCheck(Object objectToCheck, String objectName, Dc43PredefinedMaterial material) throws PropertyNotFoundException {
		if(objectToCheck == null) {
			throw new PropertyNotFoundException("Property '"+objectName+"' of material '"
								+ material.getName()+"' not found");
		}
	}
}