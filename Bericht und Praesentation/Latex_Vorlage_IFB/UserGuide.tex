\setcounter{page}{1}
\pagenumbering{arabic}  % Arabische Zahlen im Hauptteil bis zum Anhang


%********************************
\part{User Guide}
\label{chap:UserGuide}
\chapter{Dc43 Implementation}
The implementation and usage of the Material Database in the DC43 will be explained in the following.
\section{Class Diagram}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Dc43implementatio/classDiagrammDc43}
	\caption{class diagram}
	\label{fig:classdiagrammdc43}
\end{figure}
The class diagram \ref{fig:classdiagrammdc43} consists of several classes described in the following. In general it is recommended for unexperienced users to only use the GUI of the Material Database to select materials and not to perform any changes on the attributes of the objects, especially the \textit{materialDatabaseId}.
\subsection{Dc43Material}
This is the base class, which can be used to assign every material from the Material Database to it.
\subsubsection*{Attributes}
\begin{description}
	\item[name] \hfill \\ This is the name of the material, automatically set after using the Material Database GUI
	\item[materialDatabaseId]\hfill \\
	This is the unique id of the material and used by \textit{getMaterialPropertiesFromDatabase()} so this will be changed automatically by the Material Database GUI after using it
\end{description}

\subsubsection*{Methods}
\begin{description}
	\item[getPropertyValue(propertyName:String)]\hfill\\
	Takes the name of a property which is part of the \textit{dc43MaterialProperty}-link and returns its corresponding value
	\item[getPropertyUnit(propertyName:String)]\hfill\\
	Takes the name of a property which is part of the \textit{dc43MaterialProperty}-link and returns its corresponding unit
	\item[getMaterialPropertyUnits()]\hfill\\
	This method takes all properties of the material and adds the ones, which are not already hardcoded in the class, as a dynamical \textit{Dc43MaterialProperty}-class object to the \textit{Dc43Materials} \textit{dc43MaterialProperty}-link.
\end{description}

\subsection{Dc43PredefinedMaterial}
This class can be only used with materials which have a "General" property category and a property named "Density" in it, with a corresponding unit to $\frac{kg}{m^3}$. Otherwise the GUI will throw an exception after selecting the material.
\subsubsection*{Attributes}
\begin{description}
	\item[density] \hfill\\
	Density of the material, automatically set after using the Material Database GUI (if the described preconditions are met)
\end{description}

\subsection{Dc43PredefinedMetal/Dc43PredefinedPlastic/...}
This are the most specified material classes, child classes of \textit{Dc43PredefinedMaterial} and recommended to use, if possible. It is only possible to assign a material to them if all all of the attributes have a corresponding property with a compatible unit at the selected material. So the attribute-values can automatically be filled out by the Material Database. Otherwise the Material Database GUI will throw an exception. A list of the attributes, how they should be named and in which property category they should be stored, to ensure compatibility, is given below:

\begin{table}[H]
	\centering
	\begin{tabular}{||c c c||}
		\hline
		property name in Dc43 & name in database & category in database \\
		\hline\hline
		tensileStrength & Tensile Strength & Mechanical \\
		\hline
		modulusOfElasticity & Modulus of Elasticity & Mechanical \\
		\hline
		poissonsRatio & Poissons Ratio & Mechanical \\
		\hline
		shearModulus & Shear Modulus & Mechanical \\
		\hline
		electricalResistivity & Electrical Resistivity & Electrical \\
		\hline
		specificHeatCapacity & Specific Heat Capacity & Thermal \\
		\hline
		thermalConductivity & Thermal Conductivity & Thermal \\
		\hline
	\end{tabular}
\caption{Attribute conventions Dc43PredefindedMetal}
\end{table}

\begin{table}[H]
	\centering
	\begin{tabular}{||c c c||}
		\hline
		property name in Dc43 & name in database & category in database \\
		\hline\hline
		tensileStrength & Tensile Strength & Mechanical \\
		\hline
		flexuralStrength & Flexural Strength & Mechanical \\
		\hline
		flexuralModulus & Flexural Modulus & Mechanical \\
		\hline
	\end{tabular}
\caption{Attribute conventions Dc43PredefinedPlastic}
\end{table}

\textbf{!Using Dc43PredefinedMetal/-Plastic requires the material to be in a subfolder of the data file named Metal/Plastic!}

\subsection{Dc43MaterialProperty}
This is a class for dynamically accessing material properties, which are not hardcoded into the \textit{Dc43MaterialXXXX}-classes. During the execution of the design-rule, where the material is created, the properties get added as a instance of one of the child classes of \textit{Dc43MaterialProperty}. The specific child class depends on the category, the property is located in the material. This material properties get converted into \textit{Dc43MateriaProperty}-instances during the execution of the \textit{getMaterialPropertiesFromDatabase()}-method of \textit{Dc43Material} in the design rule. A table showing which \textit{MaterialPropertyCategory} is assigned to which \textit{Dc43MaterialProperty} child is given below:

\begin{table}[h]
	\centering
	\begin{tabular}{||c c||}
		\hline
		MaterialPropertyCategory name & child of Dc43MaterialProperty \\
		\hline \hline
		Mechanical & Dc43MechanicalProperty \\
		\hline
		General & Dc43GeneralProperty \\
		\hline
		Optical & Dc43OpticalProperty \\
		\hline
		Thermal & Dc43ThermalProperty \\
		\hline
		Electrical & Dc43ElectricalProperty \\
		\hline
		all other category names & Dc43UnclassifiedProperty \\
		\hline
	\end{tabular}
\caption{\textit{MaterialPropertyCategory} and corresponding property classes}
\label{table:materialpropertyclasses}
\end{table}

\subsubsection*{Attributes}
\begin{description}
	\item[materialDatabaseId] \hfill \\
	Unique id in the Material Database so the property can be tracked in the database
	\item[name] \hfill \\
	Name of the property in the Material Database
	\item[value]\hfill \\
	Value of the property in the Material Database
	\item[unit] \hfill \\
	Unit of the property in the Material Database
\end{description}

\textbf{!The Dc43MaterialProperty instances are dynamically created and do not have to be reloaded like the hardcoded attributes!}

\subsubsection{Example}
A property "General Property" is added to the "General"-category of the material "3.0205" in the "StandardDatafile.xml" (how to achieve this, is showed in the example in section \ref{ex:guiusage}):
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Dc43implementatio/example_property_craetion_1}
	\caption{new general property added}
	\label{fig:examplepropertycraetion1}
\end{figure}
This new created property has no corresponding attribute in the \textit{Dc43Metal} class, it could be assigned to. This results in the Material Database adding a new \textit{Dc43GeneralProperty}-instance (according to table \ref{table:materialpropertyclasses}) to the dc43MaterialProperty-link of the material after executing the design rules. The result graph is shown below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/Dc43implementatio/example_property_craetion_2}
	\caption{material with general property}
	\label{fig:examplepropertycraetion2}
\end{figure}

For more information about the behavior have a look at example \ref{ex:materialclasses}. An example, about how to access those instances in the graph, is given in example \ref{ex:dc43propertyclass}.


\section{Usage in graphical rules}
Have a look at Example \ref{ex:graphicalrule}.

\chapter{Dc43 Usage Examples}
\section{Different Material Classes}
\label{ex:materialclasses}
The following will be a demonstration on how a material will look in the design graph, if it is assigned to different  material classes with the Material Database GUI. The material choosen for this example is a copper alloy named "3.0205" from the "StandardDatafile.xml". It is located in "Metal"-"Aluminum alloy". A new property "Exmample" has been added to it, to demonstrate what happens to properties, which have no corresponding attribute in the classes of the DC43 material implementation.
\subsection{as Dc43Material}
\label{subsec:dc43material}
Is the element selected for a Dc43Material-class object the design graph will look like the following:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/Dc43implementatio/example_dc43material}
	\caption{material as Dc43Material}
	\label{fig:exampledc43material}
\end{figure}
\textit{name} and \textit{materialDatabaseID} are filled in an the rest of the properties are added as separate \textit{Dc43MaterialProperty}-instances.

\subsection{as Dc43PredefinedMaterial}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.65\linewidth]{images/Dc43implementatio/example_dc43predefinedMaterial}
	\caption{material as Dc43PredefinedMaterial}
	\label{fig:exampledc43predefinedmaterial}
\end{figure}
In addition to the graph of \ref{subsec:dc43material} the density is set as an attribute and therefore the number of \textit{Dc43MaterialProperty}-instances has decreased by one.

\subsection{as Dc43Metal}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{images/Dc43implementatio/example_dc43metal}
	\caption{material as Dc43Metal}
	\label{fig:exampledc43metal}
\end{figure}
All properties of the material are filled into their corresponding attributes. Only the "Exmample" property, which has no corresponding attribute in the \text{Dc43Metal}-class, is added as a \textit{Dc43MaterialProperty}-instance to the \textit{Dc43Metal} (red dot in figure \ref{fig:exampledc43metal}).

\subsection{as Dc43Plastic}
This does not work since the material is not in the "Plastic" subfolder of the "StandardDatabse.xml". If it would be moved to this folder, an exception would be thrown because the "3.0205" does not have the properties "Flexural Modulus" and "Flexural Strength".

\section{Graphical Rule Example}
\label{ex:graphicalrule}
The class diagram, before adding a material, looks like this:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\linewidth]{images/Dc43implementatio/example_graphical_usage_1}
	\caption{class diagram to add material to}
	\label{fig:examplegraphicalusage1}
\end{figure}
The part should now consist of a material. To achieve this simply the \textit{Dc43Metal}-class is added with a material link to the part (also \textit{Dc43PredefinedMaterial} or other material classes could be used, difference is explained in example \ref{ex:materialclasses}):
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{images/Dc43implementatio/example_graphical_usage_2}
	\caption{added material link}
	\label{fig:examplegraphicalusage2}
\end{figure}

Now the part can have a material. To specify the material a new graphical rule is needed, where a material instance is added to the part:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{images/Dc43implementatio/example_graphical_usage_3}
	\caption{added material to instance}
	\label{fig:examplegraphicalusage3}
\end{figure}

To select a material for the part simply right-click on the "material:Dc43Metal" instance and select DC43->Material Database to open the GUI of the Material Database:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{images/Dc43implementatio/example_graphical_usage_4}
	\caption{GUI open}
	\label{fig:examplegraphicalusage4}
\end{figure}
Select a material by choosing a material in the material tree (left side) of the GUI and clicking the button "select" in the bottom menu bar.\\
For a more detailed description on how to use the GUI please have a look at chapter \ref{ch:guiusage}.\\
After selecting a material (in this case, it has to be a metal, since we defined the part to have a metal material), the Material Database will automatically fill out the attributes of our \textit{Dc43Metal} and add a function execution at the end:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{images/Dc43implementatio/example_graphical_usage_5}
	\caption{filled out attributes}
	\label{fig:examplegraphicalusage5}
\end{figure}
The function execute at the end (\textit{getMaterialPropertiesFromDatabase()}) will automatically add the properties, which have no corresponding attribute in the \textit{Dc43Metal}-class as separate \textit{Dc43MaterialProperty} instance (further explanation/details in \ref{ex:materialclasses}).
After executing the design rules, the finished graph will look like this:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{images/Dc43implementatio/example_graphical_usage_6}
	\caption{final graph}
	\label{fig:examplegraphicalusage6}
\end{figure}
\newpage

\section{Accessing the Dc43Property class values}
\label{ex:dc43propertyclass}
Acessing this classes is possible with java rules. Using the values of the instances in math constraints is currently not possible but this will hopefully change in DC43next.

\chapter{Material Database GUI}
\label{ch:guiusage}
\textbf{!All changes in the GUI have to be saved, otherwise the changes will be lost!}
\\\\
For an example of how to use the GUI please have a look at example \ref{ex:guiusage}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_region}
	\caption[]{Material Database parts}
	\label{fig:databaseparts}
\end{figure}

The GUI of the Material Database consists of three different parts:
\begin{description}
	\item[1 - Material Tree]\hfill \\
	This displays the different data files with their included (sub-)folders and materials.
	\item[2 - General View]\hfill \\
	The properties, which exist for all the different elements in the Material Tree, are shown in here for
	the selected element of Material Tree.
	\item[3 - Properties View]\hfill\\
	This is only shown, when a material is selected in the Material Tree and contains the properties of the material, subclassed into different property classes.
\end{description}

\section{Material Tree}
\label{sec:materialtree}
\subsection{Elements}
The Material Tree contains several elements:
\begin{description}
	\item[Data Files]\hfill \\
	The topmost elements in the tree, representing the files, which are loaded into the Material Database.
	In \ref{fig:databaseparts} this are "UserDataFile.xml" and "StandardDataFile.xml". Those two are the ones, which are always includeds in the Material Database. The \textbf{"StandardDataFile.xml"} is protected and can not be changed in any way. It contains a variety of predefined materials to work with. \textbf{"UserDataFile.xml"} is not protected and the user can save custom materials here.
	\item[Materials]\hfill \\
	Those are the items on the lowest level of the Material Tree and represent the materials, stored in the data file.
	\item[Folders]\hfill \\
	Folders are on the levels between material and datafile. They are sorting the different materials into categories and subcategories. A folder can have sub-folders but no subsub-folders which is due to the limitation of the used MatMl format for presistent saving of the data.
\end{description}

\subsection{Right click menu}
\textbf{!This menu only opens by right-clicking on an element of the material tree!}\\\\
The Material Tree has a right click menu, which helps organizing the elements in the data files.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/database_tree_right_click}
	\caption[]{Material Tree right click menu}
	\label{fig:databasetreerightclick}
\end{figure}

Depending on the selected element, there is a different behavior of the Material Tree:
\begin{description}
	\item[remove]\hfill \\
	Simply removes the selected element from the data file. Removing a data file does not work, since it will disappear but be reloaded after restarting the Material Database.
	\item[new Folder]\hfill \\
	Adds a new sub-folder to the selected element, if the element is not a material or already a folder two levels under a data file ("Aluminium alloy" in Figure \ref{fig:databasetreerightclick}). In this case the folder will be added to the parent of the selected element. For example: adding a folder to "Aluminium Alloy" in Figure \ref{fig:databasetreerightclick} will add a folder to "Metal" instead. This also means adding a folder to a material, which is already in a folder two levels under a data file, will add the folder to the material parent folder instead. For example: adding a folder to "3.0205" will add a sub-folder to "Metal" instead.
	\item[new Material]\hfill\\
	Adds a new material to the selected folder/data-file. When a material is selected, the new material will be added to its parent folder. For example: adding a material to "3.0205" in figure \ref{fig:databasetreerightclick} will add a new material to "Aluminium alloy" instead. The new material is named "new Material" and has already a property category "General" where properties can be added to.
\end{description}

\section{General View}
\label{sec:generalview}
The General View contains all general information about the currently selected element in the Material tree.
\begin{description}
	\item[Name]\hfill \\
	Displays the name of the currently selected element. The name can be changed by simply clicking on it.
	\item[Datafile]\hfill \\
	Displays the path to the data file, the element is belonging to and is write protected, therefore can not be changed here.
	\item[Note]\hfill\\
	Displays the notes, made to the material/folder/data-file. This can be changed and supports multi-line notes.
\end{description}

\section{Properties View}
This is only displayed, when a material is selected in the material tree (like in figure \ref{fig:databaseparts}). It contains a Tab-Bar (\ref{propertiesTabBar}) displaying the different property categories of the material ("General" and "Mechanical" in figure \ref{fig:databaseparts}) with their belonging properties and a Menu-Bar (\ref{actionsMenuBar}) with different actions ("Tasks", "Save all",...).
\subsection{Property Category Tab-Bar}
\label{propertiesTabBar}
Selecting a property category displays the different properties, belonging to the category, in a table. Each column is self-explaining except "LookUpTableUnit", which defines the unit of the look up table, if a look up table for the value is used. This is currently not supported in combination with the DC43 (for more information have a look at the programmers guide \ref{chap:ProgrammersGuide}).
It is possible to change the values for each of the different columns, of a material property, by simply double clicking on the entry. There are some restrictions to be aware of:
\begin{description}
	\item [Value] \hfill\\ Does accept natural or real numbers. A look up table can be inserted in the following way: "\{x\_1=y\_1,x\_2=y\_2,...\}" and the GUI will help with an example, if the format is wrong.
	\item [Unit/LookUpTableUnit]\hfill \\ Only accepts units in the format "unit1\^{}(exponent1)*unit2\^{}(exponent2) so no fracture sign "/" can be used. For example: The unit $\frac{mm\cdot K}{g\cdot Ohm}$ has to be written like this: "mm*K*g\^{}(-1)*Ohm\^{}(-1)". If the property is unit-less, simply leave the unit column empty.
\end{description}
\textbf{!The unit-parser in the Dc43 is case sensitive, so before adding a unit, better have a look in the Dc43, if the unit is upper or lower case!}
\\ \\
The Property Category Tab-Bar has also a right click menu to organize the properties and their categories.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/database_properties_right_click.PNG}
	\caption[]{Properties View right click menu}
	\label{fig:propertyviewrightclick}
\end{figure}
The menu items are self-explaining but there are two \textbf{tweaks to the behavior}:
\begin{itemize}
	\item \textbf{rename Category} opens a editor with the category name in it, at the end of the current property table.
	\item \textbf{remove Category} removes the category including all the properties belonging to it. When the only category of an material is removed, it can not be used anymore and a new material has to be created or the material database has to be restarted and the material without a category will get a "General" category again.
\end{itemize}

The right-click menu will have the additional entry "remove Property" when the cursor is hovering over a property during opening the menu.\\ \\
\textbf{!Due to the MatMl format, all categories with no materials in it will be deleted after closing the Material Database!}

\subsection{Actions Menu-Bar}
\label{actionsMenuBar}
The Actions Menu-Bar contains several buttons to control the Material Database.
\begin{itemize}
	\item \textbf{Tasks}\\
	This opens a menu with custom tasks which can be executed on the Material Database like, for example, reading in a PDF. The tasks have to be added manually to the database and are automatically displayed in this menu. For more Information have a look at \ref{sec:actions}.
	\item \textbf{Save all}\\
	Saves all current changes, made in the GUI, to the corresponding data file.
	\item \textbf{Select}\\
	Fetches the currently selected element in the Material Tree and parses it to Dc43. A saving dialogue is opened so saving changes, made by the user, can be done before closing the window.
	\item \textbf{Close}\\
	self-explaining
\end{itemize}


\chapter{GUI Example}
\label{ex:guiusage}
\section{create a new material}
A new material should be added to the Material Database, the properties are given below (Link: \url{https://matmatch.com/materials/alky3070-din-1787-grade-2-0060}):
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{images/database_add_material_example_2.PNG}
	\caption[]{MatWeb material name}
	\label{fig:databaseaddmaterialexample2}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/database_add_material_example.PNG}
	\caption[]{MatWeb material properties}
	\label{fig:databaseaddmaterialexample}
\end{figure}

It is decided to add the material into the sub-folder "Copper alloy" in the folder "Metal".

The Material Database GUI looks like this:
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_1}
	\caption[]{GUI at beginning}
	\label{fig:databaseexamplegui1}
\end{figure}

First the folder and sub-folder need to be created. Right-click on "UserDataFile.xml" and select "new Folder" to add a top level folder to the UserDataFile. Then left-click on the newly created folder named "new Folder" and select its "Name"-field in the "General"-view to change its name to "Metal". The result will look like this:
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_2}
	\caption{Metal folder created}
	\label{fig:databaseexamplegui2}
\end{figure}

Right-click on the "Metal" folder and select "new Folder" to create the sub-folder for the material. Rename it, with the same procedure as the previous one, to "Copper alloy". The result will look like this:
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_3}
	\caption[]{Copper alloy folder added}
	\label{fig:databaseexamplegui3}
\end{figure}
Now right-click on "Copper alloy"-folder in the material tree and select "new Material", to create the material. Rename it to "2.0060". The result will look like this in the GUI:
\begin{figure}[H]
	\centering
	
	
	.
	\includegraphics[width=1\linewidth]{images/Database_example_gui_4}
	\caption{material is created}
	\label{fig:databaseexamplegui4}
\end{figure}

Now the material Properties and their categories need to be added to the material.
First the categories will be added. The material has the categories "General" and "Mechanical" in Figure \ref{fig:databaseaddmaterialexample}. Luckily we have already a property category "General" and therefore only the category "Mechanical" needs to be added.
To do this, right-click on the Table below the "General"-Tab in the Properties View and select "add Category".
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_5}
	\caption{}
	\label{fig:databaseexamplegui5}
\end{figure}

Now select the newly created category named "new Category" in the Tab-Bar beside "General". Right-click on the Table below the Tab-bar and select "rename Category".
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_6}
	\label{fig:databaseexamplegui6}
\end{figure}

This will open the editor for the name of the Category. Type in "Mechanical" and press [enter] to set the new name.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_7}
	\label{fig:databaseexamplegui7}
\end{figure}

\textbf{!When there are a lot of properties in the material category, the editor for the category name will be at the end of the list of properties!}
\\
\\
Now the properties need to be added which will be first done for the "Mechanical" category. First "Elastic Modulus" will be added to the category. To do this right-click on the table and select "add Property". Then double click on the "new Property" name column to open the name editor and set the name to "Elastic Modulus".
\begin{figure}[H]
	\centering
	\includegraphics[width=0.99\linewidth]{images/Database_example_gui_8}
	\label{fig:databaseexamplegui8}
\end{figure}
The same is done for the Value and setting it to "95", the Unit to "GPa" and the Note to "@ 23\textdegree C".
\\ \\
\textbf{HINT: The columns can be switched by pressing [TAB]}\\ \\
\textbf{HINT: A number with exponent with base 10 can be inserted like the following: $2.45*10^{-12}$ -> 2.45E-12}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_9}
	\label{fig:databaseexamplegui9}
\end{figure}
The same way it is done for "Poissons Ratio" (values from Figure \ref{fig:databaseaddmaterialexample2}) except that it is unit-less and therefore the Unit column is left empty.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_10}
	\label{fig:databaseexamplegui10}
\end{figure}
Now select the category "General" to add the Density to the material. This is equivalent to the previous materials except for the Unit value. As described in \ref{propertiesTabBar} there is a special format needed for the unit. So the given unit $\frac{g}{cm^3}$ has to be inserted into the Material Database like this: "g*cm\^{}(-3)"
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/Database_example_gui_11}
	\label{fig:databaseexamplegui11}
\end{figure}

Type in a note (if needed) and the material is ready to be used in the DC43 after pressing "Save all" in the menu bar below the properties table.