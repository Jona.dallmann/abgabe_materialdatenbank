\setcounter{page}{1}
\pagenumbering{arabic}  % Arabische Zahlen im Hauptteil bis zum Anhang


%********************************
\part{Programmers Guide}
\label{chap:ProgrammersGuide}
\chapter{General}
\label{ch:general}
\textbf{!It is strongly recommended to read the User Guide in Part \ref{chap:UserGuide}, before reading this Programmers Guide!} \\ \\
This chapter will give a brief inside in the Material Database program structure, how it works and how it is implemented. Therefore sections are taken out of the class diagram. The complete class diagram can be found in the appendix at the end of the handbook: \ref{chap:Appendix}.

\section{Main Structure}
\label{sec:mainstructure}
The main structure of the Material Database is implemented as MVC pattern and shown in the class diagram below:
\begin{figure}[H]
	\centering
	\addtolength{\leftskip} {-2.3cm}
	\addtolength{\rightskip}{-2cm}
	\includegraphics[width=0.8\linewidth]{"images/dc42ProgrammersGuide/mvc class diagram/modelGoonTest"}
	\caption{MVC structure class diagram}
	\label{fig:modelgoontest}
\end{figure}
The model, view and controller are created as abstract classes with a base functionality, so they can be easily exchanged (more information in chapter \ref{sec:changemodel} ff). They  have the base functionality like registering and un-registering the different components implemented which makes exchanging the different classexs a lot easier. The \textit{SimpleXXX}-classes are the specific implementations of the model, view and controller.

For the propagation of changes between model and view, the property change listener interface of java is used. So a change in the model will be propagated to the \textit{AbstractController}, which is calling the \textit{modelPropertyChange()}-method of the \textit{AbstractView}, where the change will be processed. (The implementation, which is used as a guide can be found in \url{https://www.oracle.com/technical-resources/articles/javase/mvc.html})

\subsection{Model}
\label{sec:model}
The underlying model of the Material Database is the following:
A graph/tree is used to store the materials and their properties. The topmost element of the tree is the \textit{RootElement} with its only purpose to connect the underlying \textit{DataFiles}, so a search algorithm can be performed on the graph. This \textit{RootElement} can only exist once, so it is implemented with a singleton pattern. The \textit{DataFiles} represent physical files, which are managed by the Material Database. The children of this \textit{DataFiles} can either be \textit{Materials} or \textit{Folders}. \textit{Folders} can have up to two sub-folders as children (constraint due to the used MatMl format for the \textit{DataFiles}), or an unlimited number of \textit{Materials}. \textit{Materials} can have \textit{MaterialPropertyCategories} as children and \textit{MaterialPropertyCategories} have \textit{MaterialProperties} as children. The above table visualizes the constraints of the elements by showing the graph elements  on the left and the number of child elements of a specific type it can have in the specific column:
\begin{table}[H]
	\centering
	\addtolength{\leftskip} {-2.3cm}
	\addtolength{\rightskip}{-2cm}
	\begin{tabular}{||c c c c c c c||}
		\hline
		& RootElement & DataFile & Folder & Material & MaterialProperty-&MaterialProperty \\&&&&&Category&\\
		\hline\hline
		RootElement & 0 & $\infty$ & 0 & 0 & 0 & 0 \\
		\hline
		DataFile & 0 & 0 & $\infty$ & $\infty$ & 0 & 0 \\
		\hline
		Folder & 0 & 0 & 2 & $\infty$ & 0 & 0 \\
		\hline
		Material & 0 & 0 & 0 & 0 & $\infty$ & 0 \\
		\hline
		MaterialProperty-&0&0&0&0&0&$\infty$\\Category&&&&&&\\
		\hline
		MaterialProperty & 0 & 0 & 0 & 0 & 0 & 0 \\
		\hline
	\end{tabular}
\end{table}
Managing this constraints is done by the \textit{addElement(AbstractGraphElement element, AbstractGraphElement parent)}-method in the \textit{SimpleModel}.

The implementation of the different tree/graph elements is visualized in the class diagram below.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/dc42ProgrammersGuide/graph_elements_classes/graph_elements_classes}
	\caption{graph element classes}
	\label{fig:graphelementsclasses}
\end{figure}
The most important attribute, all those classes have in common, is the id. This is a java UUID, which is unique for each element and helps to identify the element in the Material Database. The second most important attribute is the parent element id, which is the id of the parent element in the tree/graph. For a \textit{MaterialProperty} this would be the id of the \textit{MaterialPropertyCategory}, the property belongs to.

\subsubsection{SimpleModel}
\label{sub:simplemodel}
The \textit{SimpleModel} has a special structure, which will be explained in the following. An overview is given in the class diagram below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\linewidth]{images/dc42ProgrammersGuide/simpe_model_classes/simple_model_classes}
	\caption{simple model class structure}
	\label{fig:simplemodelclasses}
\end{figure}
The \textit{SimpleModel} is a class which administrates the model and ensures that the model constraints are followed. It is using a map based graph to store the material in a graph structure. It can be easily replaced by another graph, as long as this graph inherits from \textit{UndirectedWeightedSimpleGraph}. To find elements on the graph, a \textit{BreadthFirstSearch}-algorithm is used but this can also be easily replaced by another algorithm, as long as it is a child of \textit{PathSearchAlgorithm}.

\subsection{DataFiles/DataFileHandler}
\label{sub:datafilehandler}
The persistence layer of the Material Database is implemented via files called \textit{DataFiles}. Those files can be given in any format, as long as a \textit{DataFileHandler} exists to load and save those files. This \textit{DataFileHandlers} simply add an abstraction layer to the process of loading and saving the file. Which breaks implementing a \textit{DataFileHandler} for a specific file format down to converting the file to \textit{ElementDataExchangeClasse}-objects and passing them to the \textit{DataFileHandler}. Further details about this are given in section \ref{sub:fromabstractdatafilehandler}.

 The \textit{DataFileHandlers} will be added to the model at the startup of the Material Database and load the content of their assigned \textit{DataFiles} into the model. Currently there are several \textit{DataFileHandler} existing:
\begin{description}
	\item[MatMlFileHandler] \hfill\\
	This is the \textit{DataFileHandler} used as the standard-\textit{DataFileHandler} whish uses the MatMl-standard for storing the materials. This is also used by the "StandardDatafile.xml" and supports look up tables for the values.
	\item[SimpleFileHandler]\hfill \\
	This is a simple, robust \textit{DataFileHandler}, but it has self-invented format and therefore can not be read by other programs. It is supporting look up tables.
	\item[AnsysXmlFileHandler] \hfill\\
	This is the attempt to create a file handler to read in ANSYS Xml material files, which use a form of MatMl format to store the materials. This needs still some work and should not be used.
\end{description}
There are also two \textit{DataFiles} which are delivered with the Material Database. More information about them are given in \ref{sec:generalview}.

\subsection{View/SimpleView}
The view is responsible for the visualization and uses Java-SWT for this. A class diagram is given below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{"images/dc42ProgrammersGuide/view classes/view classes"}
	\caption{view implementation}
	\label{fig:view-classes}
\end{figure}
Because the GUI is able to change the model, it is somehow also an controller, which passes commands through to the controller. To do this, the implementation of the \textit{Listener} and \textit{SelectionListener} is needed in the \textit{SimpleView}. The three additional classes \textit{MaterialTreeItem}, \textit{MaterialTabItem} and \textit{MaterialTableItem} are just wrappers for corresponding classes (as shown in \ref{fig:view-classes}), so the id of the graph element, which is represented by the corresponding class instance, can be stored in the class. The most important method of \textit{SimpleView} is \textit{createContents()}, which creates the elements which are visible in the GUI.

\subsection{Controller/SimpleController}
The controller is the main class to control the model. All changes to the model should be done via implemented methods of the \textit{SimpleController}.

\subsection{Dc43 implementation/MaterialDataBaseStarter}
\label{sub:dc43implementation}
The Dc43 implementation of the Material Database is done with a class called \textit{MaterialDatabaseStarter}. A singleton pattern is used, so the same instance is accessed every time and the Material Database can not be executed as two instances parallel, so \textit{DataFiles} can not be corrupted during simultaneous access.
An overview over this class and its connections is given below:
\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{images/dc42ProgrammersGuide/materialDatabaseStarterclass/materialDatabaseStarter}
	\caption{material database starter implementation}
	\label{fig:materialdatabasestarter}
\end{figure}
The \textit{MaterialDatabaseStarter} contains the model, view and control, configures them and runs the view, to start the Material Database and return the id of the selected \textit{Material}. To do this, the method \textit{MaterialDatabaseStarter.selectAndGetMaterialAndProperties()} is used, which performs all these steps. The code to perform this is given below:
\begin{Verbatim}[tabsize = 3]
// get the material Database starter
MaterialDataBaseStarter starter = MaterialDataBaseStarter.getInstance();

// get selected material and its property Ids from the gui
// this opens the gui
List<UUID> materialAndItsPropertiesIdList =
					starter.selectAndGetMaterialAndProperties();
\end{Verbatim}
In addition to this, the controller is used to retrieve information about the selected material. 

\chapter{How to...}
\label{ch:howto}
\section{...create a custom DataFileHandler...}
\label{sec:customdatafilehandler}
\textbf{!When creating an custom \textit{DataFileHandler} it is important that the id of every graph element can be saved. This is needed so the DC43 library can track the \textit{GraphElements} after a restart of the Material Database. If the elements do not have an id (which is a java UUID) when they are loaded by the \textit{DataFileHandler}, the id will be created for them and passed to the \textit{DataFileHandler} during saving!}\\ \\
The classes to create \textit{DataFileHandlers} from are given below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{"images/dc42ProgrammersGuide/filehandlerclass diagram/filehandlerclassdiagram"}
	\caption{data file handler class diagram}
	\label{fig:filehandlerclassdiagram}
\end{figure}
When a new \textit{DatFileHandler} is created, it should be a child class of \textit{AbstractDataFileHandler} or \textit{AbstractXmlFileHandler}. In contrast to the \textit{AbstractDataFileHandler} the \textit{AbstractXmlFileHandler} offers a extended functionality for xml files, like the option to choose an .xsd-file, to check the format of the .xml-files, which are added to the \textit{DataFileHandler}.

\subsection{...from AbstractDataFileHandler}
\label{sub:fromabstractdatafilehandler}
The \textit{AbstractDataFileHandler} does most of the conversion work so generally only two methods need to be implemented in the child datafile handler:
\begin{description}
	\item[fileToElementList(Path dataFilePath)]\hfill \\
	In this method, the file, located in the given path, should be opened and transformed to a list of \textit{ElementDataExchangeClass} objects which shoiuld be returned. Those objects are then taken and transformed to elements of the material database.
	\item[elementListToFile(Path dataFilePath, List<ElementDataExchangeClass> elements)]\hfill
	The path to the \textit{DataFile} and its elements (as a list of ElementDataExchangeClass-elemnts) are given and should be transformed to the desired target file/format.
\end{description}

\subsubsection{Usage of the ElementDataExchangeClass}
The \textit{ElementDataExchangeClass} is a class to exchange data between the model and the \textit{DataFileHandler} in an abstract way. The structure of the class is visualized below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{"images/dc42ProgrammersGuide/element exchange/element exchange"}
	\caption{element exchange class diagram}
	\label{fig:element-exchange}
\end{figure}

The most important thing of this class are:
\begin{description}
	\item[type]\hfill\\ the type of the element which is exchanged with this class
	\item[pathToParentElement]\hfill\\ the path to the parent element of the exchanged element (example given in source code).
	\item[dataValuesMap]\hfill\\ the values stored in this are dynamic elements which are not the same for every model element type
\end{description}
A table showing which type of element is needs which values in the \textit{ElementDataExchangeClass}  is given below:
\begin{table}[H]
	\centering
	\addtolength{\leftskip} {-1cm}
	\addtolength{\rightskip}{-2cm}
	\begin{tabular}{||c c c c||}
		\hline
		Element type & pathToParentElement & type & dataValue "Name":String \\
		\hline\hline
		property & has to be given & "property" & "" if not given\\
		\hline
		folder & has to be given & "folder" & "" if not given \\
		\hline
		category & has to be given & "category" & "" if not given \\
		\hline
		material & has to be given & "material" & "" if not given \\
		\hline
		dataFile & not needed & "dataFile" & "" if not given\\
		\hline
	\end{tabular}
	\newline
	\vspace{0.5em}
	\newline
	\begin{tabular}{||c c c||}
		\hline
		Element type & dataValue "note":String & dataValue "id":UUID\\
		\hline\hline
		property &  "" if not given & new created, when not given\\
		\hline
		folder & "" if not given & new created, when not given\\
		\hline
		category & "" if not given & new created, when not given \\
		\hline
		material & "" if not given & new created, when not given \\
		\hline
		dataFile & "" if not given & new created, when not given\\
		\hline
	\end{tabular}
	\newline
	\vspace{0.5em}
	\newline
	\begin{tabular}{||c c c c||}
		\hline
		Element type & dataValue "unit":String & dataValue  & dataValue "value" \\&&"lookupTableUnit":String& \\
		\hline\hline
		property & "" if not given & "" if not given & Double or\\&&&Map<Double,Double>\\&&&for look up table \\
		\hline
		folder & not needed & not needed & not needed \\
		\hline
		category & not needed & not needed & not needed \\
		\hline
		material & not needed & not needed & not needed \\
		\hline
		dataFile & not needed & not needed & not needed \\
		\hline
	\end{tabular}
\caption{ElementDataExchangeClass entries part II}
\end{table}
The property "Flexural Strength" of the picture below will be shown as an \textit{ElementDataExchangeClass}-instance to give an example:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{"images/dc42ProgrammersGuide/element exchange/element exchange example"}
	\caption{example property in Material Database}
	\label{fig:element-exchange-example}
\end{figure}

The corresponding \textit{ElementDataExchangeClass} will look like this:
\begin{table}[H]
	\centering
	\addtolength{\leftskip} {-2cm}
	\addtolength{\rightskip}{-2cm}
	\begin{tabular}{||c c c||}
		\hline
		attribute & format & value \\
		\hline \hline
		type & String & "property" \\
		\hline
		pathToParentElement & List<String> & ["StandardDataFile.xml",\\&&"Plastic", "PEI", "Mechanical"] \\
		\hline
		dataValues & Map<String,Object> & \{"note"=(String)"@ 23 $\degree C$",\\&& "unit"=(String)"Pa",\\&& "name"=(String)"Flexural Strength",\\&& "id"=(UUID)"baf4412f-...",\\&& "value"=(Double)"1.65E8"\} \\
		\hline
	\end{tabular}
\caption{property element as \text{DataElementExchangeClass}}
\end{table}
For further details refer to the source code \textit{AbstractDataFileHandler}, \textit{addElementToModel()}-method.

\subsection{...from AbstractXmlDataFileHandler}
This datafile handler extends the functionality of the \textit{AbstractFileHandler} for a better handling of .xml-files. Usage is the same and explained in \ref{sub:fromabstractdatafilehandler}.\\The extensions are:
\begin{description}
	\item[.xsd file support]\hfill \\
	It is possible to add an .xsd file to this datafile handler to check the format of the input file. Usage explained in \ref{sec:abstracrxmlfileadd}.
	\item[validateSubtreeNodeAgainstXsd(Node subtreeRootNode)]\hfill \\
	With this function it is possible to validate a subtree of the .xml file against the given xsd. Simply pass the root node of this subtree to the function. The function will raise an error, when the subtree does not fit to the scheme. This is useful when an xml file includes material data but this is included in other, not needed data.
	\item[getNewDocument()]\hfill \\
	Returns a new document where the material data can be written into
	\item[getXmlRootNode(Path dataFilePath)] \hfill \\
	Returns the root node of the .xml-file corresponding to dataFilePath, so material data can be extracted from or added to.
	\item[getOnlyChildNodes(Node node)] \hfill \\
	Iterates through the children of an xml node (not its sub children) and returns a list of the children, which are of type ELEMENT\_NODE.
	\item[getAllChildNodesWithName(Node parentNode, String childNodeName)]\hfill \\
	Takes parentNode, iterates through all its child nodes (not sub child nodes), takes the nodes whose node name equals childNodeName and returns them as list.
	\item[getFirstChildNodeWithName(Node parentNode, String childNodeName)]\hfill \\
	Takes parentNode and iterates through all its child nodes until a node is found, whose name equals childNodeName and returns this node. If no node with the given childNodeName is found, null is returned. 
	\item[saveXmlFile(Document document, Path dataFilePath)] \hfill \\
	Saves the given .xml document in the given path.
\end{description}

\section{...add a new child-class of Dc43PredefinedMaterial}
\textbf{!This is the same also for a child of Dc43Material!}\\\\
First add the class in the class-diagram with some attributes it should have:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/dc42ProgrammersGuide/addClassToDc43Diagram/ho_to_add_class_1}
	\caption{added class with attributes}
	\label{fig:hotoaddclass1}
\end{figure}

\textbf{This needs completion after the integration into DC43next is done!}

\section{...add a new DataFile...}
The adding of a new datafile is done in the  getInstance()-method of the \textit{MaterialDatabaseStarter}-class. The procedure is different based on how and where the \textit{DataFile} should be added. If simply a new datafile should be added to the \textit{userDataFileMatMlHandler}, it is recommended to simply copy the "StandardDataFile.xml" and add it to the datafile handler. After opening the GUI some errors will appear but clicking "save all" in the GUI will resolve this and the \textit{DataFile} is ready to be used.
\\\\
\textbf{!The format of the added file should be readable by the \textit{DataFileHandler}. The handler does not overwrite the content of a file, which is in an unreadable format!}
\subsection{...to an existing DataFile handler}
\label{sec:addtoexistingdatafile}
Simply add the file with \textit{addFilePath(path/toFile)} to the datafile handler and the database will show the datafile at the next startup. The code below will add a new datafile to the instance \textit{userDataFileHandler} of \textit{MatMlFileHandler}
\begin{Verbatim}
userDataFileHandler.addFilePath("\\path\\to\\file");
\end{Verbatim}
Example: MaterialDatabaseStarter.java line 57f

\subsection{...with a new DataFile handler}
\label{sec:withnewdatafilehandler}
Create the \textit{DataFileHandler} and add the \textit{DataFile} as explained in \ref{sec:addtoexistingdatafile}. Now decide if the \textit{DataFileHandler} should be write protected or not and add it to the model. A code example is given below:
\begin{Verbatim}
// create the datafile handler for a generic DataFileHandler-class
ChosenFileHandlerClass myFileHandler = new ChosenFileHandlerClass();

// if this is called, the filehandler will be protected, if it should not be
// protected, simply dont use the code line below
myFileHandler.setProtected();

// here the model is created etc. which is not shown in this
// code example

// add a file to the dataFileHandler
myFileHandler.addFilePath("\\path\\to\\data\\file");

// add the created file handler to the model, after the model has been created
model.addDataFileHandler(myFileHandler);
\end{Verbatim}
Example: MaterialDatabaseStarter.java line 42-64

\subsection{...with a new child of AbstractXmlFileHandler}
\label{sec:abstracrxmlfileadd}
The basic procedure is the same as in \ref{sec:withnewdatafilehandler} but there is an extra option which could be used. The \textit{AbstractXmlFileHandler} has the option to add a .xsd to file to check, if the .xml fulfills the structural requirements. The adapted code from \ref{sec:addtoexistingdatafile} is given below:
\begin{Verbatim}
// create the datafile handler for a generic XmlFileHandlerChild-class
XmlFileHandlerChildClass myFileHandler = new XmlFileHandlerChildClass();

// if this is called, the filehandler will be protected, if it should not be
// protected, simply dont use the code line below
myFileHandler.setProtected();

// here the model is created etc. which is not shown

// add xsd file to the filehandler
myFileHandler.setXsdFile("\\path\\to\\xsd\\file")

// add a file to the dataFilehandler
myFileHandler.addFilePath("\\path\\to\\data\\file");

// add the created file handler to the model,
// after the model has been created
model.addDataFileHandler(myFileHandler);
\end{Verbatim}
Example: MaterialDatabaseStarter.java line 42-64

\section{...add a new task to the GUI}
\label{sec:actions}
The GUI supports the execution of tasks which are implemented via custom java code. To add such an task simply create a child of the \textit{AbstractAction}-class and implement the desired functionality in the \textit{performTask()}-method. Examples are given in de.iils.dc43.materialdatabase.implementations.actions.
To add the created class to the task, create a new object of the task in \textit{MaterialDatabaseStarter} and add it to the controller with \textit{addTask(task)}. For example have a look at \textit{MaterialDatabaseStarter} lines 76-80:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{"images/dc42ProgrammersGuide/add tesk/add task"}
	\caption{add task code snipppet}
	\label{fig:add-task}
\end{figure}
This tasks can preform actions on the model and therefore the controller is passed as an argument to the task, when creating a new task. Also this task should never perform any operations directly on the model or the view, which will break the MVC-pattern.

\section{...change the model}
\label{sec:changemodel}
There are two different ways to change the model. The first way is to change just the underlying model, without the restrictions (change \textit{modelGraph} in \textit{SimpleModel}), the second is changing model with the restrictions (exchange \textit{SimpleModel}). It is recommended to only change the underlying model and not change the model with the restrictions. To change the model without the restrictions a class derived from \textit{UndirectedWeightedSimpleGraph} is needed.

\section{...change the view}
To change the view simply create a new child-class of \textit{AbstractView} and add it instead of the \textit{SimpeView} as \textit{materialDatabaseView} to the \textit{MaterialDatabaseStarter}. The most important method which needs implementation, is the \textit{modelPropertyChange()}-method, which propagates the change from the model to the view. To have a overview over the events, which can happen, have a look at the implementation of this function in the \textit{SimpleView} class.

\chapter{To do}
This chapter lists some useful features, which did not make it into the Material Database but would make working with the Material Database a lot easier and improve usability.

\section{add datafile-Button in GUI}
\label{sec:adddatafile}
Currently it is not possible to add a new datafile via the GUI to the Material Database. In the GUI (\textit{SimpleView.java}) there is a section commented out which will add a "add DataFile"-Button to the GUI (line 320ff) but the functionality is not implemented yet. Adding the functionality will work this way:
\subsection{Changes in the GUI}
First the "add DataFile"-button, mentioned above, should be commented in again. After this it should be considered, which \textit{DataFileHandler} the newly created \textit{DataFiles} should use. I would recommend a drop down menu, like for the tasks ( see section \ref{actionsMenuBar}), as explained in \ref{sec:actions}, in which the \textit{DataFileHandler}, the new \textit{DataFile} will be created for, can be chosen (the new \textit{DataFile} will be created in an corresponding format). So for every new added \textit{DataFileHandler}, a new entry should appear in this drop down menue, to create a new \textit{DataFile} for this handler. In addition to this a path must be defined, where the new \textit{DataFile} should be stored. I would recommend to store it automatically in the "dataFiles"-folder of the Material Database source code.
\subsection{Changes in the Controller}
Add a method in the \textit{AbstractController} and the \textit{SimpleController}, its name could be "addDatafile", which adds a new \textit{DataFile} to the model (the "root"-element of the model will be the parent of the newly created \textit{DataFile}) and the chosen \textit{DataFileHandler} (changes will be automatically propagated to the view, due to the \textit{PropertyChangeListener}). It is recommended to create a \textit{createNewDataFile}-function for the \textit{DataFileHandler}, which copies a template for an empty \textit{DataFile} to the chosen location and renames it correspondingly. After this the method \textit{loadDataFile()} of the corresponding \textit{DataFileHandler} should be executed for the added \textit{DataFile}, to load it into the view.\\
Now a mechanism is needed, which stores/saves the location and corresponding \textit{DataFileHandler} of the newly created \textit{DataFile}, otherwise the \textit{DataFile} wount be included after a restart of the Material Database. This can be done in two ways: first, an .ini file could be created for the Material Database, which will be loaded during the startup and where the location of the \textit{DataFiles} and their corresponding \textit{DataFilehandler} are saved. As alternative the "StandardDataFile.xml" could contain a comment section at the beginning, which includes the relevant data. It is recommended to implement the persitent layer for this as suggested in first way. In addition to this also the "UserDataFile" could be implemented in this .ini file, but the "Standard" \textit{DataFile} should stay hardcoded in the \textit{MaterialDatabaseStarter} so it is ensured that at least this file is always loaded.

\section{removing DataFile}
This is more an optional feature but when adding a \textit{DataFile} is supported, also its removal should be. So before implementing this, the adding feature in \ref{sec:adddatafile} should be implemented.\\
The removing of \textit{DataFiles} can be currently done by right-clicking on the \textit{DataFile} in the Material Tree in the GUI (explanation in section \ref{sec:materialtree}) and selecting "remove". Due to the fact that the \textit{DataFiles}, which should be added to the Material Database, are hardcoded, the \textit{DataFile} is loaded again, after restarting the Material Database. So the following could be done, to implement this feature:

\subsection{changes}
The \textit{AbstractDataFileHandler} has a \textit{removeFilePath()}-method, which only removes the file from the list of paths, which are administered by the \textit{DataFileHandler}. But the file needs also to be removed from the .ini, if the .ini is implemented like it is suggested in section \ref{sec:adddatafile}. In addition to this, it has to be decided, if the file should also be removed from the file system or not.\\
It is suggested to add a "removeDataFile()"-method to \textit{SimpleController} and \textit{AbstractController} which will deal with removing the \textit{DataFile} from the .ini and probably delete it from the file system (depends on the wished behavior). This function could be called from the \textit{SimpleControllers} \textit{removeElement()}-method, which will check, if the given element id corresponds to a \textit{DataFile} and then calls \textit{removeDataFile()}, after \textit{this.registeredModel.removeElement()} as shown below:
\begin{Verbatim}[tabsize = 4]
@Override
public void removeElement(UUID id) {
	// remove element with given id from the model
	// (also removes it in the view due to the property listener)
	this.registeredModel.removeElement(id);
	
	// check if the element to remove is a dataFile
	AbstractGraphElement element = this.registeredModel.getElementById(id);
	if(element.getType().equals("dataFile")){
		this.removeDataFile((DataFile) element);
	}
}
\end{Verbatim}

\subsection{moving elements}
It should be possible to move elements from one \textit{DataFile} into another. This would make it easier to adapt existing materials to the own needs. To make this possible, also a duplicate feature is needed. The workflow would be like the following: right-click on an element in the Material Tree (\ref{sec:materialtree}) and select duplicate for duplication or drag and drop the material/folder/etc from one position in the Material Tree to another. The implementation of those features is explained below:
\subsection{duplicate feature}
\label{subsub:duplicatefeature}
\subsubsection{changes in GUI}
A "duplicate"-entry is needed in the right-click menu of the Material Tree (Tree is explained in \ref{sec:materialtree}). To add this, add a menu item to the Material Tree in \textit{SimpleView.java} line 223 ff where it is also done for the other menu items. After this the functionality for the newly created menu item is needed, which can be added in the \textit{widgedSelected()}-method of \textit{SimpleView.java}. This cane be done by creating a new entry in line 1247 ff, where the text of the newly created menu item should be used by a if condition, to trigger the duplication command in the \textit{SimpleController}.

\subsubsection{changes in Controller}
Add a the two \textit{duplicateElement()}-methods (which takes the element id as argument or the element id and the new parent id) to the \textit{SimpleController} and \textit{AbstractController}. This should only call the \textit{duplicateElement}-method of the model.

\subsubsection{changes in the Model}
 Add a \textit{duplicateElement}-method in the \textit{SimpleModel} and \textit{AbstractModel} which should take the element id and the parent id of the element as arguments. This functions should get the element corresponding to the given id, check the type of the element and create a new element with the same type name etc with its parent being the element behind the parent id. The different elements like \textit{Folder}, \textit{FataFile}, and material have to be treated individually. Especially the \textit{DataFile} where a new file needs to be created for the duplication and it also needs to be added to the corresponding \textit{DataFileHandler}. Please check if the \textit{DataFile} is protected, before adding the elements to the tree. This can be done by calling \textit{addDataFileContainingElement()} for the elements id and then calling DataFileElement.isprotected(). Also a call of \textit{firePropertyChange()} is needed which will inform the view about the creation of the new element: \textit{firePropertyChange(createElementPartenId, null, newCreatedElementId)}.
 
 \subsection{moving feature}
 It is recommended to implement the duplicate feature (section \ref{subsub:duplicatefeature}) is already implemented. For this feature only a few changes are needed:
 \subsubsection{change in GUI}
 I would recommend to implement the moving in the Material Tree with drag and drop. An example for this is given in \url{http://www.java2s.com/Tutorial/Java/0280__SWT/Dragleafitemsinatree.htm}. After dragging and dropping the material/folder/etc the \textit{moveElement()}-method of the controller should be called.
 
 \subsubsection{changes in the Model}
 Implement the \textit{moveElement()}-method of the \textit{SimpleModel}, which already exists as a dummy.
 Call the \textit{duplicateElement()} method of the model to duplicate the element and then the \textit{removeElement()}-method, to remove the original element. The code example is given below:
 \begin{Verbatim}[tabsize=4]
@Override
public void moveElement(UUID elementId, UUID newParentId) {
	// check if the moving is done on a protected datafile
	AbstractGraphElement correspondingDataFile = 
						this.getDataFileContainingElement(elementId);
	// do the duplication and remove the element if the duplication
	// succeeded and the datafile containing the element is
	// not protected otherwise the element will just be duplicated
	// to the new position
	if(this.duplicateElement(elementId, newParentId)
		&& !correspondingDataFile.isProtected()){
		this.removeElement(elementId);
	}
}
 \end{Verbatim}

\section{supporting "/"-sign in units}
Currently the unit-column of the property in the Material Database GUI only supports units in a special format (have a look at section \ref{propertiesTabBar}). This should be changed to make entering units a little bit easier and can be achieved in the following way:
\subsection{changes in the propertyElement}
In the \textit{PropertyElement} in \textit{setUnit()} there is a part in where the unit is not set, if it contains a "$/$"-sign. This part should be removed.

\subsection{changes in the MatMlFileHandler}
This \textit{FileHandler} has a method \textit{convertUnitToUnitNode()} which converts the unit-string of the material property to unit nodes, whose can be saved in the MatMl-format. The structure of the MatMl format is given in the "matmlDefinition.xsd"-file in the source code. An example of how the unit $ \frac{W}{m\cdot K}$ will look like in the MatMl-\textit{DataFile}, is given below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\linewidth]{images/dc42ProgrammersGuide/unitsave/unit_save}
	\caption{unit in MatMl Style}
	\label{fig:unitsave}
\end{figure}
Now the functions needs to be adapted so it supports the "/" sign.

\section{Look up table support}
The Material Database does currently not support look-up-tables, or at least supports them just partly, which means it is possible to use them as value in the GUI (section \ref{propertiesTabBar}).
\subsection{changes in the Dc43 class diagram}
A new function has to be implemented to get the value of the look up table and the unit.
Both can be accessed with the following methods in the \textit{MaterialProperty}-class:
\begin{description}
	\item[getLookUpTableUnit()] \hfill \\
	returns the look up table unit as string
	\item[getValueAtPoint()] \hfill \\
	returns the value of the look up table at a specific point
\end{description}
Now the Dc43 class diagram must be adjusted, so the look up table unit and the look up table value can be recieved from the material database.\\ \\
TIPP: It can be checked, if the \textit{MaterialProperty} has a look up table or a fixed value, by simple asking for the look up table unit. If it does not exist, there is no look up table used for this property.

\chapter{Expansions}
\section{Search field for the GUI}
The GUI does currently not support the search for materials/folders and it would be nice, if such a feature could be implemented. In the \textit{SimpleView} lines 161ff is the creation of a search field commented out. It creates a search field above the Material Tree as shown below:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\linewidth]{images/dc42ProgrammersGuide/searchField/searchField}
	\caption{search field}
	\label{fig:searchfield}
\end{figure}
The typed in values are already red out in this field so the only thing missing is the selection of the correct folders/materials. This should be done by making the other elements invincible, which do not correspond to the typed in name.\\
It is suggested to make a separate class for the Material Tree and implementing this functionality there. The position where the code should be implemented is marked with a FIXME in the commented out code.

\section{Search for property values in the GUI}
Currently it is not possible to search for a material with specific values. This should be changed and therefore a search-action should be created so the materials in the database can be filtered for some specific values like density>10$\frac{g}{cm^3}$. The basic implementation in the GUI can be done with an action as explained in section \ref{sec:actions}. To filter the different properties a function could be used like the following example (not safe to use, just example):
\begin{Verbatim}[tabsize = 4]
public List<AbstractGraphElement> filterMaterialsByCategory
(String propertyName, double maxValue, double minValue){
	// search in the model for all elements of type property
	Set<AbstractGraphElement> allVertices = this.modelGraph.getVertices();
	List<AbstractGraphElement> foundMaterials = new ArrayList<>();
	for (AbstractGraphElement element : allVertices) {
		if (element.getType().equals("property")
		&& element.getName().equals(propertyName)) {
			// get the material of the property which is
			// two nodes above the property
			UUID propertyCategoryId = element.getParentId();
			UUID materialId = this.getElementParentId(propertyCategoryId);
			foundMaterials.add(this.getElementById(materialId));
		}
	}
	return foundMaterials;
}
\end{Verbatim}
So this returned material list just needs to be processed in the \textit{SimpleView} for a good presentation.