\babel@toc {english}{}
\contentsline {part}{\numberline {I}User Guide}{1}{part.1}%
\contentsline {chapter}{\numberline {1}Dc43 Implementation}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Class Diagram}{2}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Dc43Material}{3}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Dc43PredefinedMaterial}{3}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}Dc43PredefinedMetal/Dc43PredefinedPlastic/...}{4}{subsection.1.1.3}%
\contentsline {subsection}{\numberline {1.1.4}Dc43MaterialProperty}{4}{subsection.1.1.4}%
\contentsline {subsubsection}{\numberline {1.1.4.1}Example}{5}{subsubsection.1.1.4.1}%
\contentsline {section}{\numberline {1.2}Usage in graphical rules}{6}{section.1.2}%
\contentsline {chapter}{\numberline {2}Dc43 Usage Examples}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Different Material Classes}{7}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}as Dc43Material}{7}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}as Dc43PredefinedMaterial}{8}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}as Dc43Metal}{8}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}as Dc43Plastic}{8}{subsection.2.1.4}%
\contentsline {section}{\numberline {2.2}Graphical Rule Example}{9}{section.2.2}%
\contentsline {section}{\numberline {2.3}Accessing the Dc43Property class values}{12}{section.2.3}%
\contentsline {chapter}{\numberline {3}Material Database GUI}{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}Material Tree}{14}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Elements}{14}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Right click menu}{14}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}General View}{16}{section.3.2}%
\contentsline {section}{\numberline {3.3}Properties View}{16}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Property Category Tab-Bar}{16}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Actions Menu-Bar}{18}{subsection.3.3.2}%
\contentsline {chapter}{\numberline {4}GUI Example}{19}{chapter.4}%
\contentsline {section}{\numberline {4.1}create a new material}{19}{section.4.1}%
\contentsline {part}{\numberline {II}Programmers Guide}{1}{part.2}%
\contentsline {chapter}{\numberline {5}General}{2}{chapter.5}%
\contentsline {section}{\numberline {5.1}Main Structure}{2}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Model}{3}{subsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.1.1}SimpleModel}{4}{subsubsection.5.1.1.1}%
\contentsline {subsection}{\numberline {5.1.2}DataFiles/DataFileHandler}{5}{subsection.5.1.2}%
\contentsline {subsection}{\numberline {5.1.3}View/SimpleView}{6}{subsection.5.1.3}%
\contentsline {subsection}{\numberline {5.1.4}Controller/SimpleController}{7}{subsection.5.1.4}%
\contentsline {subsection}{\numberline {5.1.5}Dc43 implementation/MaterialDataBaseStarter}{7}{subsection.5.1.5}%
\contentsline {chapter}{\numberline {6}How to...}{9}{chapter.6}%
\contentsline {section}{\numberline {6.1}...create a custom DataFileHandler...}{9}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}...from AbstractDataFileHandler}{10}{subsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.1.1}Usage of the ElementDataExchangeClass}{10}{subsubsection.6.1.1.1}%
\contentsline {subsection}{\numberline {6.1.2}...from AbstractXmlDataFileHandler}{12}{subsection.6.1.2}%
\contentsline {section}{\numberline {6.2}...add a new child-class of Dc43PredefinedMaterial}{13}{section.6.2}%
\contentsline {section}{\numberline {6.3}...add a new DataFile...}{14}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}...to an existing DataFile handler}{14}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}...with a new DataFile handler}{15}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}...with a new child of AbstractXmlFileHandler}{15}{subsection.6.3.3}%
\contentsline {section}{\numberline {6.4}...add a new task to the GUI}{16}{section.6.4}%
\contentsline {section}{\numberline {6.5}...change the model}{17}{section.6.5}%
\contentsline {section}{\numberline {6.6}...change the view}{17}{section.6.6}%
\contentsline {chapter}{\numberline {7}To do}{18}{chapter.7}%
\contentsline {section}{\numberline {7.1}add datafile-Button in GUI}{18}{section.7.1}%
\contentsline {subsection}{\numberline {7.1.1}Changes in the GUI}{18}{subsection.7.1.1}%
\contentsline {subsection}{\numberline {7.1.2}Changes in the Controller}{18}{subsection.7.1.2}%
\contentsline {section}{\numberline {7.2}removing DataFile}{19}{section.7.2}%
\contentsline {subsection}{\numberline {7.2.1}changes}{19}{subsection.7.2.1}%
\contentsline {subsection}{\numberline {7.2.2}moving elements}{20}{subsection.7.2.2}%
\contentsline {subsection}{\numberline {7.2.3}duplicate feature}{20}{subsection.7.2.3}%
\contentsline {subsubsection}{\numberline {7.2.3.1}changes in GUI}{20}{subsubsection.7.2.3.1}%
\contentsline {subsubsection}{\numberline {7.2.3.2}changes in Controller}{21}{subsubsection.7.2.3.2}%
\contentsline {subsubsection}{\numberline {7.2.3.3}changes in the Model}{21}{subsubsection.7.2.3.3}%
\contentsline {subsection}{\numberline {7.2.4}moving feature}{21}{subsection.7.2.4}%
\contentsline {subsubsection}{\numberline {7.2.4.1}change in GUI}{21}{subsubsection.7.2.4.1}%
\contentsline {subsubsection}{\numberline {7.2.4.2}changes in the Model}{21}{subsubsection.7.2.4.2}%
\contentsline {section}{\numberline {7.3}supporting "/"-sign in units}{22}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}changes in the propertyElement}{22}{subsection.7.3.1}%
\contentsline {subsection}{\numberline {7.3.2}changes in the MatMlFileHandler}{22}{subsection.7.3.2}%
\contentsline {section}{\numberline {7.4}Look up table support}{23}{section.7.4}%
\contentsline {subsection}{\numberline {7.4.1}changes in the Dc43 class diagram}{23}{subsection.7.4.1}%
\contentsline {chapter}{\numberline {8}Expansions}{24}{chapter.8}%
\contentsline {section}{\numberline {8.1}Search field for the GUI}{24}{section.8.1}%
\contentsline {section}{\numberline {8.2}Search for property values in the GUI}{24}{section.8.2}%
\contentsline {chapter}{\numberline {A}Appendix}{26}{appendix.A}%
